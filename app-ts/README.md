# tg-cms-backend

### 注意事項

.env 環境檔放在根目錄
F5 啟動偵錯

### 安裝 vscode extensions

安裝 .vscode/extensions.json 列的套件
存檔時會自動排版、修正錯誤(如果可以)

### 資料庫

mysql
redis

### model generate

db schema 有修改時手動執行 npm run model

### swagger

更新 Controller 後動新啟動專案會自動 express routes 跟 swagger.json
(或手動執行 npm run swagger)
[Validator](https://github.com/OAI/OpenAPI-Specification/blob/main/versions/3.1.0.md#properties) 寫在 model(body) 上或 controller 參數上
swagger url: /api

### view model validation

參考 https://tsoa-community.github.io/docs/annotations.html#list-of-supported-keywords-with-arguments 直接寫在 interface 上即可

### 編譯部署執行

npm run build
將以下檔案上傳到 server

-   .env
-   package.json
-   package-lock.json
-   swagger.json (如果需要 swagger)
-   tsoa.json
    在 server package.json 那層目錄執行 npm install
    在 server dist 目錄下執行 pm2 start index.js

### 其他

不要 export default
不要 export default
不要 export default
