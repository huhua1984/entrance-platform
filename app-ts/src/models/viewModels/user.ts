/**
 * login user request body
 * @example {
 * "username": "demo_user1",
 * "password": "zxc12345",
 * "code": "718007a2d2da44e0a10933d7c807fc4d"
 * }
 */
export interface IUserLonginReqBody {
    /**
     * 帳號
     * @required
     * @maxLength 255
     */
    username: string;

    /**
     * 密碼
     * @required
     */
    password: string;

    /**
     * 推廣碼(代理UUID)
     * @required
     */
    code: string;
}
