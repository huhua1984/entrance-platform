import { Request } from 'express';
import { TranslateOptions } from 'i18n';
export interface Ii18nRequest extends Request {
    t: (phraseOrOptions: string | TranslateOptions, ...replace: string[]) => string;
}

export interface IAuthParameter {
    agentId: number,
    channelUrl: string,
    recordUrl: string,
    md5Key: string,
    desKey: string
}
