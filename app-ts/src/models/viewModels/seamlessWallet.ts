/**
 * seamless wallet request query
 * @example agent=10001&timestamp=1488803043759 &param=4Oq38C3kRzEU9%2Be2pqdUNwa7nIbBcWGpFRQjxYboxJ37cEUpZ0P3wND7jBmzQ7Do&key=378c50b aaf22320332ee09e704ad8ad3
 */
export interface ISeamlessWalletRequest {
    /**
     * 代理编号(平台提供)
     * @required
     */
    agent: string;

    /**
     * 时间戳(Unix 时间戳带上毫秒),获取当前时间
     * @required
     */
    timestamp: string;

    /**
     * 参数加密字符串
     * @required
     */
    param: string;

    /**
     * Md5 校验字符串
     * @required
     */
    key: string;
}
