import { Controller, Example, Get, Queries, Route, Tags } from 'tsoa';
import { ISeamlessWalletRequest } from '../models/viewModels/seamlessWallet';

import { logger } from '../config/logger';
import { SeamlessWalletResponse } from '../libs/vp/seamlessWalletStatus';
import { seamlessService } from '../services/seamlessWalletService';
@Route('seamlessWallet')
@Tags('Seamless Wallet')
export class SeamlessWalletController extends Controller {
    /**
     * @summary 單一錢包使用接口
     * @param param
     */
    @Get('vp')
    @Example<string>('"{"m":"channelHandle","s":1001,"d":{"code":0,"account":"demo_user1","money":0}}"')
    async vp(
        @Queries() param: ISeamlessWalletRequest): Promise<SeamlessWalletResponse> {
        const seamlessWalletResponse = await seamlessService.vp(param);
        if (seamlessWalletResponse.s === 1000) {
            logger.warn(seamlessWalletResponse);
        }
        return seamlessWalletResponse;
    }

}
