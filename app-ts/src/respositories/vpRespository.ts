import { HogoBrPlay } from '../models/auto/hogo_br_play/models';
import { VpOrderAttributes } from '../models/auto/hogo_br_play/vpOrder';

class VpRespository {
    async getOrder(orderId: string, channelId: string) {
        return await HogoBrPlay.VpOrder.findOne({
            where: {
                orderId,
                channelId
            },
            raw: true
        });
    }
    async insertOrder(order: Partial<VpOrderAttributes>) {
        return await HogoBrPlay.VpOrder.create(order);
    }
    async updateOrder(status: number, orderId: string, channelId: string) {
        return await HogoBrPlay.VpOrder.update(
            {
                status
            },
            {
                where: { orderId, channelId },
            }
        );
    }
}

export const vpRespository = new VpRespository();
