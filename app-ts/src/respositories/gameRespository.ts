import { HogoBrSale } from '../models/auto/hogo_br_sale/models';

class GameRespository {
    async getOneIntergationByGameCodeGateway(gameCode: string, channelId: string, slug: string) {
        return await HogoBrSale.GameIntegration.findOne({
            include: [{
                required: true,
                model: HogoBrSale.Game,
                attributes: ['id', 'gameCode', 'gameSerial', 'maintainStatus'],
                as: 'game',
                where: {
                    status: 1
                }
            },
            {
                model: HogoBrSale.Gateway,
                attributes: ['slug', 'name', 'id', 'serial'],
                as: 'gateway',
                where: {
                    slug
                }
            }],
            where: {
                channelId,
                gameCode,
                status: 1
            }
        });
    }
}

export const gameRespository = new GameRespository();
