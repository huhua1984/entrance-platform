import { Transaction } from 'sequelize';
import { v4 as uuidv4 } from 'uuid';
import { HogoBrSale } from '../models/auto/hogo_br_sale/models';
import { WalletLedgerAttributes } from '../models/auto/hogo_br_sale/walletLedger';

class WalletRespository {
    async getUserBalance(userId: string, symbol: string, t?: Transaction) {
        return await HogoBrSale.Wallet.findOne({
            attributes: ['balance', 'serial', 'id'],
            where: {
                userId,
                symbol,
                position: 'TOKEN'
            },
            transaction: t
        });
    }
    async create(userId: string, symbol: string) {
        return await HogoBrSale.Wallet.create({
            id: uuidv4(),
            userId,
            symbol,
            position: 'TOKEN'
        });
    }
    async insertLedger(ledger: Partial<WalletLedgerAttributes>, t?: Transaction) {
        return await HogoBrSale.WalletLedger.create({
            id: uuidv4(),
            ...ledger
        }, { transaction: t });
    }
}

export const walletRespository = new WalletRespository();
