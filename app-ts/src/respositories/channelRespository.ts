import { HogoBrSale } from '../models/auto/hogo_br_sale/models';

class ChannelRespository {
    async getOneByAuthParamAgentId(agentId: number, gatewaySlug: string) {
        return await HogoBrSale.Channel.findOne({
            attributes: ['id', 'authParameter', 'symbol', 'language', 'statusMaintain'],
            where: {
                ['authParameter.agentId']: agentId
            },
            subQuery: false,
            include: [{
                attributes: ['id'],
                model: HogoBrSale.GatewayIntegration,
                as: 'gatewayIntegrations',
                where: { status: 1 },
                include: [{
                    attributes: ['id', 'slug', 'serial'],
                    model: HogoBrSale.Gateway,
                    as: 'gateway',
                    where: { status: 1, slug: gatewaySlug }
                }]
            }],
        });
    }
}

export const channelRespository = new ChannelRespository();
