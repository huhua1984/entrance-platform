import { HogoBrSale } from '../models/auto/hogo_br_sale/models';

class UserRespository {
    async getOneByUsernameAndChanneId(username: string, channelId: string, symbol: string) {
        return await HogoBrSale.User.findOne({
            attributes: ['id', 'status'],
            where: {
                username,
                channelId
            },
            include: {
                model: HogoBrSale.Wallet,
                as: 'wallets',
                where: {
                    symbol,
                    position: 'TOKEN'
                }
            }
        });
    }
}

export const userRespository = new UserRespository();
