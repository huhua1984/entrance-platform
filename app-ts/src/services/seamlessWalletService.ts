import { BigNumber } from 'bignumber.js';
import queryString from 'querystring';
import { Transaction } from 'sequelize';
import { logger } from '../config/logger';
import { util } from '../libs/util';
import { SeamlessWalletResponse, seamlessWalletStatus } from '../libs/vp/seamlessWalletStatus';
import { sequelizeOption } from '../models/auto/hogo_br_sale/models';
import { IAuthParameter } from '../models/viewModels/general';
import { ISeamlessWalletRequest } from '../models/viewModels/seamlessWallet';
import { channelRespository } from '../respositories/channelRespository';
import { gameRespository } from '../respositories/gameRespository';
import { userRespository } from '../respositories/userRespository';
import { vpRespository } from '../respositories/vpRespository';
import { walletRespository } from '../respositories/walletRespository';

class SeamlessService {
    async vp(query: ISeamlessWalletRequest): Promise<SeamlessWalletResponse> {
        const { agent, timestamp, param, key } = query;

        const channel = await channelRespository.getOneByAuthParamAgentId(Number(agent), 'vp');
        if (!channel) {
            return new SeamlessWalletResponse(1000, seamlessWalletStatus.ERR_AGENT_NONEXISTENT);
        }
        if (channel.statusMaintain === 1 || channel.gatewayIntegrations.length === 0 || !channel.gatewayIntegrations[0].gateway) {
            return new SeamlessWalletResponse(1000, seamlessWalletStatus.ERR_MAINTENANCE);
        }

        const authParameter: IAuthParameter = (<IAuthParameter>channel.authParameter);
        const keyOrg = agent + timestamp + authParameter.md5Key;
        const serverKey = util.md5Encode(keyOrg);
        if (serverKey !== key) {
            return new SeamlessWalletResponse(1000, seamlessWalletStatus.ERR_MD5);
        }
        const decodeParams = queryString.parse(util.desDecode(authParameter.desKey, param));
        if (!decodeParams) {
            return new SeamlessWalletResponse(1000, seamlessWalletStatus.ERR_DES_DECODE);
        }
        const { account, orderId, money, gameId, kindId, currency, s } = decodeParams as { account: string, orderId: string, money: string, gameId: string, kindId: string, currency: string, number: string, s: string };
        const sNumber = s ? Number(s) : 1000;
        switch (sNumber) {
            case 1001: {
                if (!account) {
                    return new SeamlessWalletResponse(sNumber, seamlessWalletStatus.ERR_DATA);
                }
                const username = account;
                const user = await userRespository.getOneByUsernameAndChanneId(username, channel.id, currency);
                if (!user) {
                    return new SeamlessWalletResponse(sNumber, seamlessWalletStatus.ERR_ACCOUNT_NONEXISTENT);
                }
                if (user.status !== 1) {
                    return new SeamlessWalletResponse(sNumber, seamlessWalletStatus.ERR_MAINTENANCE);
                }
                const userWallet = user.wallets[0];
                if (!userWallet) {
                    await walletRespository.create(user.id, currency);
                }
                return new SeamlessWalletResponse(sNumber, {
                    code: seamlessWalletStatus.SUCCESS.code,
                    account,
                    money: userWallet ? new BigNumber(userWallet.balance).dividedBy(1000000).toNumber() : 0
                });
            }
            case 1002:
            case 1003: {
                if (isNaN(Number(money)) || !account || !orderId) {
                    return new SeamlessWalletResponse(sNumber, seamlessWalletStatus.ERR_DATA);
                }
                const gameIntegration = await gameRespository.getOneIntergationByGameCodeGateway(kindId, channel.id, 'vp');
                if (!gameIntegration || gameIntegration.maintainStatus === 1 || gameIntegration.game.maintainStatus === 1) {
                    return new SeamlessWalletResponse(sNumber, seamlessWalletStatus.ERR_MAINTENANCE);
                }

                const { allBet, cellScore, revenue, profit, totalwithdraw, gameNo } = decodeParams as { allBet: string, cellScore: string, revenue: string, profit: string, totalwithdraw: string, gameNo: string };
                const username = account;
                const user = await userRespository.getOneByUsernameAndChanneId(username, channel.id, currency);
                if (!user) {
                    return new SeamlessWalletResponse(sNumber, seamlessWalletStatus.ERR_ACCOUNT_NONEXISTENT);
                }
                if (user.status !== 1) {
                    return new SeamlessWalletResponse(sNumber, seamlessWalletStatus.ERR_MAINTENANCE);
                }
                const t = await sequelizeOption.transaction({
                    isolationLevel: Transaction.ISOLATION_LEVELS.SERIALIZABLE
                });
                try {
                    const userWallet = user.wallets[0];
                    if (!userWallet) {
                        await t.commit();
                        return new SeamlessWalletResponse(sNumber, seamlessWalletStatus.ERR_ACCOUNT_NONEXISTENT);
                    }
                    const originMoney = userWallet.balance;
                    let balance: number;
                    let type: number;
                    const updateBalance = new BigNumber(money).multipliedBy(1000000).toNumber();
                    if (sNumber === 1002) {
                        balance = new BigNumber(originMoney).minus(updateBalance).toNumber();
                        type = 1;
                    }
                    else if (sNumber === 1003) {
                        balance = new BigNumber(originMoney).plus(updateBalance).toNumber();
                        type = 0;
                    }
                    if (balance < 0) {
                        await t.rollback();
                        return new SeamlessWalletResponse(sNumber, seamlessWalletStatus.ERR_BALANCE_INSUFFICIENT);
                    }
                    const order = await vpRespository.getOrder(orderId, channel.id);
                    if (order) {
                        await t.rollback();
                        return new SeamlessWalletResponse(sNumber, seamlessWalletStatus.ERR_ORDERID);
                    }
                    const updatedWallet = await userWallet.update({ balance }, {
                        transaction: t,
                    });
                    await vpRespository.insertOrder({
                        channelId: channel.id,
                        walletId: updatedWallet.id,
                        walletSerial: updatedWallet.serial,
                        orderId: orderId,
                        username: account,
                        originMoney,
                        money: updateBalance,
                        status: updatedWallet ? 1 : 0,
                        type: type,
                        gameSerial: gameIntegration.game.gameSerial,
                        gatewaySerial: gameIntegration.gateway.serial,
                        kindId: kindId,
                        allBet: Number(allBet) || 0,
                        cellScore: Number(cellScore) || 0,
                        revenue: Number(revenue) || 0,
                        profit: Number(profit) || 0,
                        gameUserNo: gameNo,
                        totalWithdraw: Number(totalwithdraw) || 0,
                        currency
                    });
                    await walletRespository.insertLedger({
                        walletId: updatedWallet.id,
                        walletSerial: updatedWallet.serial,
                        gatewayId: gameIntegration.gateway.id,
                        gatewaySerial: gameIntegration.gateway.serial,
                        orderId: orderId,
                        gain: type === 0 ? updateBalance : 0,
                        loss: type === 1 ? updateBalance : 0,
                        balanceBefore: originMoney,
                        balanceRemain: balance,
                        type: `vp_seamless_wallet_${sNumber}`,
                        typeId: sNumber
                    }, t);
                    if (!updatedWallet) {
                        await t.rollback();
                        return new SeamlessWalletResponse(sNumber, seamlessWalletStatus.ERR_ACCOUNT_PROCESSING);
                    }
                    await t.commit();
                    return new SeamlessWalletResponse(sNumber, {
                        code: seamlessWalletStatus.SUCCESS.code,
                        account,
                        money: new BigNumber(balance).dividedBy(1000000).toNumber()
                    });
                }
                catch (error) {
                    await t.rollback();
                    logger.error(`单一钱包上下分失败 s:${sNumber}`);
                    logger.error(error);
                    return new SeamlessWalletResponse(sNumber, seamlessWalletStatus.ERR_UNKNOWN_ERROR);
                }
            }
            case 1004: {
                if (!orderId) {
                    return new SeamlessWalletResponse(sNumber, seamlessWalletStatus.ERR_DATA);
                }
                /**
                 * 状态(1:成功, 2:失败, 3:处理中, 4:查无订单, 5:其他错误)
                 * 当状态为2或4时，会进行重新派奬, 成功后修正订单为已处理
                 */
                const order = await vpRespository.getOrder(orderId, channel.id);
                const status = order ? order.status : 4;
                return new SeamlessWalletResponse(sNumber, {
                    code: seamlessWalletStatus.SUCCESS.code,
                    status
                });
            }
            case 1005: {
                if (isNaN(Number(money)) || !account || !orderId) {
                    return new SeamlessWalletResponse(sNumber, seamlessWalletStatus.ERR_DATA);
                }
                const username = account;
                const user = await userRespository.getOneByUsernameAndChanneId(username, channel.id, currency);
                if (!user) {
                    return new SeamlessWalletResponse(sNumber, seamlessWalletStatus.ERR_ACCOUNT_NONEXISTENT);
                }
                if (user.status !== 1) {
                    return new SeamlessWalletResponse(sNumber, seamlessWalletStatus.ERR_MAINTENANCE);
                }
                const userWallet = await user.wallets[0];
                if (!userWallet) {
                    return new SeamlessWalletResponse(sNumber, seamlessWalletStatus.ERR_ACCOUNT_NONEXISTENT);
                }
                let status = 0;
                const order = await vpRespository.getOrder(orderId, channel.id);
                const updateBalance = new BigNumber(money).multipliedBy(1000000);
                if (order) {
                    status = 1;
                    if (order.status === 1) {
                        const t = await sequelizeOption.transaction({
                            isolationLevel: Transaction.ISOLATION_LEVELS.SERIALIZABLE
                        });
                        try {
                            const balance = new BigNumber(userWallet.balance).plus(updateBalance).toNumber();
                            const updatedWallet = await userWallet.update({ balance }, {
                                transaction: t
                            });
                            await walletRespository.insertLedger({
                                walletId: updatedWallet.id,
                                walletSerial: updatedWallet.serial,
                                gatewayId: channel.gatewayIntegrations[0].gateway.id,
                                gatewaySerial: channel.gatewayIntegrations[0].gateway.serial,
                                orderId: orderId,
                                gain: updateBalance.toNumber(),
                                loss: 0,
                                balanceBefore: userWallet.balance,
                                balanceRemain: balance,
                                type: `vp_seamless_wallet_${sNumber}`,
                                typeId: sNumber
                            }, t);
                            status = 2;
                            if (updatedWallet) {
                                await vpRespository.updateOrder(2, orderId, channel.id);
                                status = 1;
                            }
                            await t.commit();

                        }
                        catch (error) {
                            await t.rollback();
                            logger.error(`单一钱包取消下注失败 s:${sNumber}`);
                            logger.error(error);
                            return new SeamlessWalletResponse(sNumber, seamlessWalletStatus.ERR_UNKNOWN_ERROR);
                        }
                    }
                }
                else {
                    const { allBet, cellScore, revenue, profit, totalwithdraw, gameNo } = decodeParams as { allBet: string, cellScore: string, revenue: string, profit: string, totalwithdraw: string, gameNo: string };
                    const gameIntegration = await gameRespository.getOneIntergationByGameCodeGateway(kindId, channel.id, 'vp');
                    if (!gameIntegration) {
                        return new SeamlessWalletResponse(sNumber, seamlessWalletStatus.ERR_MAINTENANCE);
                    }
                    await vpRespository.insertOrder({
                        walletId: userWallet.id,
                        channelId: channel.id,
                        orderId: orderId,
                        username: account,
                        originMoney: userWallet.balance,
                        money: updateBalance.toNumber(),
                        status: 2,
                        type: 1,
                        gameSerial: gameIntegration.game.gameSerial,
                        gatewaySerial: gameIntegration.gateway.serial,
                        kindId: kindId,
                        allBet: Number(allBet) || 0,
                        cellScore: Number(cellScore) || 0,
                        revenue: Number(revenue) || 0,
                        profit: Number(profit) || 0,
                        gameUserNo: gameNo,
                        totalWithdraw: Number(totalwithdraw) || 0,
                        currency
                    });
                    status = 1;
                }
                return new SeamlessWalletResponse(sNumber, {
                    code: seamlessWalletStatus.SUCCESS.code,
                    status
                });
            }
            default:
                return new SeamlessWalletResponse(sNumber, seamlessWalletStatus.ERR_UNKNOWN_REQUEST);
        }
    }
}
export const seamlessService = new SeamlessService();
