import * as bcrypt from 'bcrypt';
import passport from 'passport';
import * as passportJWT from 'passport-jwt';
import { ExtractJwt, Strategy as JwtStrategy } from 'passport-jwt';
import { Strategy as LocalStrategy } from 'passport-local';
import { envConfig } from '../config/envConfig';
import { HogoBrSale } from '../models/auto/hogo_br_sale/models';
import { Ii18nRequest } from '../models/viewModels/general';

const authenticateUser = new passport.Passport();

// Setup passport strategy
authenticateUser.use(
    new LocalStrategy(
        {
            usernameField: 'username',
            passwordField: 'password',
            passReqToCallback: true,
        },
        async (req: Ii18nRequest, username: string, password: string, done: any) => {
            try {
                const channel = await HogoBrSale.Channel.findOne({
                    attributes: ['cooperationFunc', 'status', 'authParameter', 'language', 'statusMaintain'],
                    where: { id: req.body.code },
                    raw: true,
                });

                if (!channel) {
                    return done(null, false, { message: req.t('5_errorMsg_8') });
                }
                if (channel.status === 0) {
                    return done(null, false, { message: req.t('0_channelDisabled') });
                }
                if (channel.statusMaintain === 1) {
                    return done(null, false, { message: req.t('4_UnderMain') });
                }

                const user = await HogoBrSale.User.findOne({ where: { username, channelId: req.body.code } });

                if (!user) {
                    return done(null, false, { message: req.t('1_accountPasswordErrorMsg') });
                }
                if (user.status === 0) {
                    return done(null, false, { message: req.t('0_accountSuspended') });
                }

                const hashPass = /^\$2y\$/.test(user.password) ? '$2b$' + user.password.slice(4) : user.password;

                if (!bcrypt.compareSync(password, hashPass)) {
                    return done(null, false, { message: req.t('1_accountPasswordErrorMsg') });
                }

                await user.update({ loginAt: new Date() });

                return done(null, user);
            }
            catch (error) {
                return done(error);
            }
        }
    )
);

// Serialize and deserialize user
authenticateUser.serializeUser((user: any, cb: any) => {
    cb(null, { serial: user.serial });
});

authenticateUser.deserializeUser((user: any, cb: any) => {
    HogoBrSale.User.findByPk(user.serial, { include: [{ association: 'channel' }] }).then((_user: any) => {
        return cb(null, _user);
    });
});

const jwtOptions: passportJWT.StrategyOptions = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: envConfig.jwt.secret
};

authenticateUser.use(new JwtStrategy(jwtOptions, function (jwtPayload: any, next: any) {
    HogoBrSale.User.findByPk(jwtPayload.id, {
        include: [{ association: 'channel' }],
    }).then((user: any) => {
        if (!user) {
            return next(null, false);
        }
        return next(null, user);
    });
})
);

export { authenticateUser };
