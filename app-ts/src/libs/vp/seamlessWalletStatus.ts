/* eslint-disable @typescript-eslint/naming-convention */
type SeamlessWalletStatusType =
    | 'SUCCESS'
    | 'ERR_BALANCE_INSUFFICIENT'
    | 'ERR_ACCOUNT_NONEXISTENT'
    | 'ERR_MAINTENANCE'
    | 'ERR_TOKEN'
    | 'ERR_DATA'
    | 'ERR_DES_DECODE'
    | 'ERR_MD5'
    | 'ERR_UNKNOWN_REQUEST'
    | 'ERR_ORDERID'
    | 'ERR_AGENT_NONEXISTENT'
    | 'ERR_ACCOUNT_PROCESSING'
    | 'ERR_UNKNOWN_ERROR';

type SeamlessWalletStatusEntry = {
    code: number;
    name: SeamlessWalletStatusType;
    message: string;
};

const seamlessWalletStatus: Record<SeamlessWalletStatusType, SeamlessWalletStatusEntry> = {
    SUCCESS: {
        code: 0,
        name: 'SUCCESS',
        message: '成功',
    },
    ERR_BALANCE_INSUFFICIENT: {
        code: 1,
        name: 'ERR_BALANCE_INSUFFICIENT',
        message: '余额不足',
    },
    ERR_ACCOUNT_NONEXISTENT: {
        code: 2,
        name: 'ERR_ACCOUNT_NONEXISTENT',
        message: '会员帐号不存在',
    },
    ERR_MAINTENANCE: {
        code: 3,
        name: 'ERR_MAINTENANCE',
        message: '维护中',
    },
    ERR_TOKEN: {
        code: 4,
        name: 'ERR_TOKEN',
        message: 'token验证错误',
    },
    ERR_DATA: {
        code: 5,
        name: 'ERR_DATA',
        message: '数据格式错误',
    },
    ERR_DES_DECODE: {
        code: 6,
        name: 'ERR_DES_DECODE',
        message: '解密错误',
    },
    ERR_MD5: {
        code: 7,
        name: 'ERR_MD5',
        message: 'MD5错误',
    },
    ERR_UNKNOWN_REQUEST: {
        code: 8,
        name: 'ERR_UNKNOWN_REQUEST',
        message: '未知的操作子类型',
    },
    ERR_ORDERID: {
        code: 9,
        name: 'ERR_ORDERID',
        message: '订单编号重复',
    },
    ERR_AGENT_NONEXISTENT: {
        code: 10,
        name: 'ERR_AGENT_NONEXISTENT',
        message: '代理不存在',
    },
    ERR_ACCOUNT_PROCESSING: {
        code: 11,
        name: 'ERR_ACCOUNT_PROCESSING',
        message: '帳號正在上下分',
    },
    ERR_UNKNOWN_ERROR: {
        code: 999,
        name: 'ERR_UNKNOWN_ERROR',
        message: '未知的錯誤',
    },
};

type IDataResult = {
    code: number,
    status?: number,
    account?: string,
    money?: number,
    name?: string,
    mesaage?: string
};

class SeamlessWalletResponse {
    s: number;
    m: string = '/channelHandle';
    d: IDataResult;

    constructor(s: number = 1000, d: IDataResult) {
        this.s = s;
        this.d = d;
    }
}

export { SeamlessWalletResponse, SeamlessWalletStatusEntry, SeamlessWalletStatusType, seamlessWalletStatus };
