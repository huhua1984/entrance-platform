import { apiResponses, ResponseCode } from '../models/enums/ApiResponse';

export class ServiceResponse<T> {
    code: string;
    message: string;
    body: T;
    codeNO: number;

    constructor(code: ResponseCode = ResponseCode.OK, body: T = null, message = '') {
        const apiRes = apiResponses[code];
        this.body = body;
        this.code = apiRes.code;
        this.codeNO = apiRes.codeNO;
        this.message = message || apiRes.message;
    }
}
