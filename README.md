# Game Launcher 


# INFO

> WIP
* 2023/11/24: 此API第一階段功能只串接player(user), channel, 所以admin, agents部分可以暫且忽略 schema也以hogo為主
* 2023/12/05: 切分兩個服務 外部port: `9528`, 內部port: `9527`

## Prerequisite

* install npm & node
* mysql & redis server

## Initialize
```bash
cd app
cp dev.env .env
npm install
npm run start
```


## Swagger
http://localhost:9528/api/docs

## 命名規範
* API路徑及名稱遵循Restful，名稱以`小駝峰`
    * Example: 
        * 獲得資料     GET      /data
        * 新增資料     POST     /data
        * 刪除資料     DELETE   /data/1
        * 詳細資料     GET      /data/1/dailyDetail  
* 檔案與資料夾命名規則採`小駝峰` 
    * Example: `services/userService`
* API Response `res.apiRes(status, body)`
    * `status`定義[apiRes](./app/libs/middlewares/apiResHandler.js), [statusCode](./app//ibs/statusCode.js)
    * `body`回應主體
        * 若非分頁形式回應參數可命名為`data`或者根據API內容命名e.g. `games`
        * 若已分頁形式回應參數為`rows`
        * 若是錯誤訊息或是給使用者要知道的內容回傳`message`


## ENV 設定
* `NODE_ENV`
  * cookie安全層級設定到https
  * 限制發送簡訊API 間隔60秒 每天上限十封
  * 套用helmet.js - 常見的headers安全保護 [詳情](https://helmetjs.github.io/)
  * Express.js 內建production設定 - css cache, less error messages [詳情](https://expressjs.com/en/advanced/best-practice-performance.html#set-node_env-to-production)
* `PUBLIC_PORT` 對外服務port 
* `INTERNAL_PORT` 對內服務port

## Docker build & run
```bash
cd app
cp dev.env .env 
docker compose up -d
```

## Usage

1. chrome -> http://localhost:9527/signup?code={代理uuid} (進到測試註冊頁面)
1. 使用前端註冊
1. chrome -> http://localhost:9527/signin?code={代理uuid} (進到測試登入頁面)
2. 使用前端登入 redirect 遊戲登入

## sequelize-auto
  * 根據database輔助產生sequelize model
  * 例:
  ```bash
    npx sequelize-auto -o "./db/models/hogo_br_play" -d hogo_br_play -h 192.168.32.32 -u root -x 123456 -e mysql -p 13306
  ```

##  game_integrations seeder

```bash
  NODE_ENV=hogo_br_sale npx sequelize-cli db:seed:all
  NODE_ENV=hogo_br_sale npx sequelize-cli db:seed:undo:all 
```