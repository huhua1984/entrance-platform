const vpLogRespository = require('../../repositories/vpLog');
const { gameProviderAdapter } = require('../../libs/gameProviders/gameProviderAdaptor');
const logger = require('../logger');
const db = require('../../db/models');
const utils = require('../../libs/utils');
const { VpProvider } = require('../gameProviders/providers/vp');
const Op = require('sequelize').Op;
const channels = db.channels;
const _ = require('lodash');

class VpGameRecordPuller {
  #cron;
  constructor () {
    const cron = require('node-cron');
    this.#cron = cron.schedule('*/3 * * * *', async () => {
      try {
        const endTime = new Date().getTime() - 60 * 1000;
        const startTime = endTime - 4 * 60 * 1000;
        await this.pull(startTime, endTime);
        logger.info('vp gameRecord pull service job executed successfully');
      } catch (error) {
        if (error === VpProvider.ERR_RES_RECORD_NOT_EXISTS || error === VpProvider.ERR_RES_TOO_MANY_REQUESTS) {
          logger.info(`vp gameRecord pull service job executed, ${error.message}`);
          return;
        }
        logger.warn(`vp gameRecord pull service job, ${error.message}`);
      }
    });
    if (!process.env.VP_RECORD_PULL_RECORD_ENABLE) {
      this.stop();
    } else {
      logger.info('vp gameRecord pull service start');
    }
  }

  stop () {
    this.#cron.stop();
  }

  start () {
    this.#cron.start();
  }

  async pull (startTime, endTime) {
    const channelRows = await channels.findAll({
      attribues: ['id'],
      where: {
        authParameter: { [Op.not]: null }
      },
      raw: true
    });
    const removeByUniqueAgent = _.uniqBy(channelRows, (row) => {
      return row.authParameter.agentId;
    });
    for (let index = 0; index < removeByUniqueAgent.length; index++) {
      await utils.delay(2000);
      try {
        await this.pullByChannel(startTime, endTime, removeByUniqueAgent[index].id);
      } catch (error) {
        logger.warn(`參數: ${JSON.stringify(removeByUniqueAgent[index])} 拉單失敗`);
      }
    }
  }

  async pullByChannel (startTime, endTime, channelId) {
    const vpRecords = await gameProviderAdapter.getGameRecord('vp', {
      channelId,
      startTime,
      endTime
    });
    return await vpLogRespository.bulkUpsert(vpRecords, channelId);
  }
}

module.exports = new VpGameRecordPuller();
