const { body } = require('express-validator');

module.exports = {
  insert: [
    body('userId').exists().withMessage('please input user_id'),
    body('position').exists().withMessage('please input position'),
    body('symbol').exists().withMessage('please input symbol')
  ]
};
