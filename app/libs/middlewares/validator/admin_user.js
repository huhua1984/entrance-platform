const { param, body } = require('express-validator');

module.exports = {
  updateChannelPassword: [param('channel_id').exists().withMessage('请输入代理ID'), body('password').exists().isLength({ min: 8 }).withMessage('请输入密码')]
};
