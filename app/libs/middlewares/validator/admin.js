const { check } = require('express-validator');

module.exports = {
  signIn: [check('account').exists().withMessage('请输入帐号'), check('password').exists().withMessage('请输入密码')],
  agentCreate: [
    check('uid').exists().withMessage('请输入父代理'),
    check('name').exists().withMessage('请输入代理名字'),
    check('account').exists().withMessage('请输入帐号'),
    check('password').exists().withMessage('请输入密码'),
    check('promoCode').exists().withMessage('请输入推廣碼'),
    check('promotionUrl').exists().withMessage('请输入推廣網址'),
    check('brand').exists().withMessage('请输入品牌'),
    // check('desKey').exists().withMessage('请输入desKey'),
    // check('md5Key').exists().withMessage('请输入md5key'),
    check('isPromotionDomain').exists().isIn(['1', '2']).withMessage('請選擇自有網址'),
    check('status').exists().isIn(['1', '2']).withMessage('请選擇狀態')
  ],
  agentUpdate: [
    check('uid').exists().withMessage('请输入父代理'),
    check('name').exists().withMessage('请输入代理名字'),
    check('promotionUrl').exists().withMessage('请输入推廣網址'),
    check('brand').exists().withMessage('请输入品牌'),
    // check('desKey').exists().withMessage('请输入desKey'),
    // check('md5Key').exists().withMessage('请输入md5key'),
    check('isPromotionDomain').exists().isIn(['1', '2']).withMessage('請選擇自有網址'),
    check('status').exists().isIn(['1', '2']).withMessage('请選擇狀態')
  ],
  gameProviderAgentCreate: [
    check('agentId').exists().withMessage('请選擇代理'),
    check('gameProviderId').exists().withMessage('请選擇遊戲廠商'),
    check('setInfo').exists().withMessage('请输入系統設置'),
    check('status').exists().isIn(['1', '2']).withMessage('请選擇狀態')
  ],
  gameProviderAgentUpdate: [
    check('agentId').exists().withMessage('请選擇代理'),
    check('gameProviderId').exists().withMessage('请選擇遊戲廠商'),
    check('setInfo').exists().withMessage('请输入系統設置'),
    check('status').exists().isIn(['1', '2']).withMessage('请選擇狀態')
  ],
  gameProviderCreate: [
    check('code').exists().withMessage('请入名稱'),
    check('name').exists().withMessage('请輸入代號'),
    check('channelUrl').exists().isURL().withMessage('请输入channel Url'),
    check('recordUrl').exists().isURL().withMessage('请输入record Url'),
    check('logo').exists().withMessage('请输入logo 路徑'),
    check('status').exists().isIn(['1', '2']).withMessage('请選擇狀態')
  ],
  gameProviderUpdate: [
    check('code').exists().withMessage('请入名稱'),
    check('channelUrl').exists().isURL().withMessage('请输入channel Url'),
    check('recordUrl').exists().isURL().withMessage('请输入record Url'),
    check('logo').exists().withMessage('请输入logo 路徑'),
    check('status').exists().isIn(['1', '2']).withMessage('请選擇狀態')
  ]
};
