const { check, body } = require('express-validator');
const { validatorMsg } = require('../../utils');
const CryptoJS = require('crypto-js');

const checkNewPasswordValidator = function (confirmPassword, { req }) {
  return confirmPassword === req.body.password;
};

const updateNewPasswordValidator = function (confirmNewPassword, { req }) {
  return confirmNewPassword === req.body.newPassword;
};

const paddingJpPhoneNumber = (phoneNumber) => {
  // 检查是否是手机号码且不以81开头
  if (/^[2-9]\d{9}$/.test(phoneNumber) && !/^81/.test(phoneNumber)) {
    // 在手机号码前添加81
    phoneNumber = '81' + phoneNumber;
  }
  return String(phoneNumber);
};

const decryptPassword = (password) => {
  const bytes = CryptoJS.AES.decrypt(password, 'JPCafe');
  const originalPassword = bytes.toString(CryptoJS.enc.Utf8);
  return originalPassword;
};

module.exports = {
  signIn: [body('username').exists().isLength({ min: 8 }).withMessage(validatorMsg('0_usernameErrorMsg', 8)),
    body('password').exists().customSanitizer(decryptPassword).withMessage(validatorMsg('0_passwordTip')),
    check('code').exists().withMessage(validatorMsg('0_codeTip'))],
  phoneSignIn: [check('phoneNumber').exists().isLength({ min: 6 }).customSanitizer(paddingJpPhoneNumber).withMessage(validatorMsg('0_phoneTip')),
    check('verifyCode').exists().withMessage(validatorMsg('0_passwordTip')),
    check('code').exists().isLength({ min: 6 }).withMessage(validatorMsg('0_codeTip'))],
  register: [check('username').exists().isLength({ min: 8 }).withMessage(validatorMsg('0_usernameErrorMsg', 8)),
    body('password').exists().customSanitizer(decryptPassword).isLength({ min: 8 }).withMessage(validatorMsg('0_passwordErrorMsg', 8)),
    check('confirmPassword').exists().customSanitizer(decryptPassword).custom(checkNewPasswordValidator).withMessage(validatorMsg('1_checkPwd_noMatch')),
    check('phoneNumber').exists().isLength({ min: 6 }).customSanitizer(paddingJpPhoneNumber).withMessage(validatorMsg('0_phoneTip')),
    check('verifyCode').exists().isLength({ min: 6 }).withMessage(validatorMsg('0_verifyCodeTip')),
    check('code').exists().withMessage(validatorMsg('0_codeTip'))],
  sendVerification: [body('phoneNumber').exists().isLength({ min: 6 }).customSanitizer(paddingJpPhoneNumber).withMessage(validatorMsg('0_phoneTip')),
    check('code').exists().withMessage(validatorMsg('0_codeTip'))],
  updatePassword: [body('password').exists().customSanitizer(decryptPassword).withMessage(validatorMsg('0_passwordTip')),
    body('newPassword').exists().customSanitizer(decryptPassword).isLength({ min: 8 }).withMessage(validatorMsg('0_passwordErrorMsg', 8)),
    body('confirmNewPassword').exists().customSanitizer(decryptPassword).custom(updateNewPasswordValidator).withMessage(validatorMsg('1_checkPwd_noMatch'))]
};
