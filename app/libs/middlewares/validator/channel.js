const { param } = require('express-validator');
const { validatorMsg } = require('../../utils');

module.exports = {
  checkId: [param('id').exists().withMessage(validatorMsg('0_codeTip'))]
};
