const logger = require('../logger');
const statusCode = require('../statusCode');

/**
 * @typedef {('ok'|'unknown'|'invalidArgument'|'deadlineExceeded'|'notFound'|'alreadyExists'|'permissionDenied'|'resourceExhausted'|'unimplemented'|'internal'|'unavailable'|'unauthenticated')} Status
 */

/**
 * Express middleware for handling API responses.
 * @function
 * @param {Object} req - Express request object.
 * @param {Object} res - Express response object.
 * @param {Function} next - Express next function.
 * @returns {void}
 * @throws {Error} Will throw an error if an invalid status code is provided.
 * @example
 * // Usage example:
 * app.use(apiResHandler);
 * app.get('/example', (req, res, next) => {
 *   try {
 *     const status = 'ok';
 *     const body = { example: 'body' };
 *     res.apiRes(status, body);
 *   } catch (error) {
 *     next(error);
 *   }
 * });
 */
const apiResHandler = (req, res, next) => {
  /**
   * Generate and send an API response based on the provided body and status.
   * @function
   * @param {Status} status - The status code (key) to determine the response details.
   * @param {Object} [body={}] - The body to be included in the response.
   * @returns {void}
   * @throws {Error} Will throw an HTTP error if an invalid status code is provided.
   * @example
   * const status = 'ok';
   * const body = { example: 'body' };
   * res.apiRes(status, body);
   */
  res.apiRes = (status, body) => {
    if (!statusCode[status]) {
      logger.warn(`status: ${status} is not found in statusCodes of apiResHandler`);
    }
    const _status = statusCode[status] ? status : 'internal';

    /**
     * @typedef {Object} StatusCode
     * @property {string} code - The status code.
     * @property {number} codeNO - The numeric status code.
     * @property {string} message - The status message.
     * @property {number} httpStatus - The HTTP status code.
     */

    /**
     * @type {StatusCode}
    */
    const { codeNO, code, httpStatus } = statusCode[_status];

    const responseObj = {
      codeNO,
      code,
      message: body?.message || req.t(`0_${_status}`),
      ...(body ? { body } : {})
    };
    return res.status(httpStatus).json(responseObj);
  };
  next();
};

module.exports = apiResHandler;
