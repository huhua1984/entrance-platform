const rateLimit = require('express-rate-limit');
const { RedisStore } = require('rate-limit-redis');
const redisClient = require('../../config/redis');

const minuteLimiter = rateLimit({
  windowMs: 60 * 1000, // 一分鐘的時間窗口
  max: 1, // 每分鐘最多1次請求
  handler: (req, res, next, options) => res.apiRes('resourceExhausted', { message: req.t('0_minuteLimit') }),
  keyGenerator: (req, res) => req.body.phoneNumber || req.ip,
  store: new RedisStore({
    prefix: 'minuteLimiter:',
    sendCommand: (...args) => redisClient.call(...args)
  })
});

const dayLimiter = rateLimit({
  windowMs: 24 * 60 * 60 * 1000, // 一天的時間窗口
  max: 10, // 每天最多10次請求
  handler: (req, res, next, options) => res.apiRes('resourceExhausted', { message: req.t('0_dayLimit') }),
  keyGenerator: (req, res) => req.body.phoneNumber || req.ip,
  store: new RedisStore({
    prefix: 'dayLimiter:',
    sendCommand: (...args) => redisClient.call(...args)
  })
});

module.exports = { minuteLimiter, dayLimiter };
