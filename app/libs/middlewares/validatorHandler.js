const validationResult = require('express-validator').validationResult;

// middleware -> check()、body()、cookie() , check if for all
// official sample : https://github.com/validatorjs/validator.js#validators

const validatorHandler = function (req, res, next) {
  const result = validationResult(req);
  if (!result.isEmpty()) {
    logger.warn(`${req.originalUrl}  参数验证错误: ${JSON.stringify(result.array())}}`);
    return res.apiRes('invalidArgument', { errors: result.array(), message: result.errors[0].msg });
  }
  next();
};

module.exports = validatorHandler;
