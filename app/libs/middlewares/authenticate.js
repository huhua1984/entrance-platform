const passport = require('passport');

const isAuthenticated = (req, res, next) => {
  if (!req.isAuthenticated()) {
    return res.apiRes('unauthenticated');
  }
  next();
};

module.exports = process.env.PASSPORT_STRATEGY === 'jwt' ? passport.authenticate('jwt', { session: false }) : isAuthenticated;
