const logger = require('../logger');
const { isProd } = require('../utils');

module.exports = function errorHandler (err, req, res, next) {
  // set locals, only providing error in development
  const body = {};
  body.message = err.Message || err.message;
  if (!isProd()) {
    logger.error(err);
    body.stack = err.stack;
  }
  // response the error
  if (Number(err.status) === 404) {
    logger.warn(`${req.method} ${req.originalUrl} is not found`);
    res.apiRes('notFound', { message: `${req.method} ${req.originalUrl} is not found` });
    return;
  }
  res.apiRes('unknown', body);
};
