const path = require('path');
const fs = require('fs');
const crypto = require('crypto');
const RedisStore = require('connect-redis').default;
const client = require('../config/redis');

exports.randomKey = function (len) {
  const buf = [];
  const chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
  const charlen = chars.length;

  for (let i = 0; i < len; ++i) {
    buf.push(chars[getRandomInt(0, charlen - 1)]);
  }

  return buf.join('');
};

function getRandomInt (min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

/**
 * Recursively scans a directory and generates an array of route objects based on the file structure.
 *
 * @param {string} routeFile - The path to the root directory to scan for routes.
 * @returns {Array<{ group: string, target: string }>} An array of route objects, each containing a group and target.
 */
exports.scanRoutes = (routeFile) => {
  const getGroup = (prefix, filePath) => {
    prefix = (prefix ?? '');
    if (prefix[0] !== '/') prefix = '/' + prefix;
    let result = [];
    fs.readdirSync(filePath, { withFileTypes: true }).forEach(file => {
      if (file.isDirectory()) {
        const route = getGroup(`${filePath}/${file.name}`, `${prefix}${file.name}`);
        result = result.concat(route);
      } else {
        const group = `${prefix}${(file.name.substring(0, file.name.lastIndexOf('.')))}`;
        const routeName = file.name.substring(0, file.name.lastIndexOf('.'));
        const target = path.join(filePath, routeName);
        result.push({ group: group === '/general' ? '/' : group, target });
      }
    });
    return result;
  };
  const groupRoutes = getGroup('', routeFile);
  return groupRoutes;
};

/**
 * Configures session middleware and Passport authentication for a specified domain.
 *
 * @param {Object} app - Express application instance.
 * @param {string} domain - The domain for which session and Passport should be configured ('player', 'agent', or 'admin').
 * @throws {Error} Throws an error if an invalid domain is provided.
 * @example
 * // Usage example:
 * const express = require('express');
 * const app = express();
 * app.use(cookieParser());
 * handleSession(app, 'agent');
 * handleSession(app, 'player');
 * handleSession(app, 'admin');
 */
exports.handleSession = (app, domain) => {
  const session = require('express-session');

  const authPaths = {
    player: ['/api/player', '/index'],
    agent: ['/api/agent', '/agent'],
    admin: ['/api/admin', '/admin']
  };

  const sessionConfig = {
    store: new RedisStore({ client }),
    name: domain,
    secret: domain, // Use the domain as the session secret
    cookie: { secure: this.isProd(), maxAge: 86400000 },
    resave: false,
    saveUninitialized: false,
    proxy: true
  };

  const passportMiddleware = require(`../config/passport/${domain}`);

  const paths = authPaths[domain];

  if (!paths || !passportMiddleware) {
    throw new Error(`Invalid domain: ${domain}`);
  }

  app.use(paths, session(sessionConfig));
  app.use(paths, passportMiddleware.initialize());
  app.use(paths, passportMiddleware.session());
};

exports.getIp = function (req) {
  let ip = req.headers['x-real-ip'] || req.headers['x-forwarded-for'] || req.socket.remoteAddress || '';
  const ips = ip.split(',');
  if (ips.length > 0) {
    if (ips !== '::1') {
      ip = ips[0];
    } else { ip = '127.0.0.1'; }
  }
  ip = ip === '::1' ? '127.0.0.1' : ip;
  return ip.replace(/::ffff:/, '');
};

// AES解密
exports.desDecode = function (desKey, data) {
  const cipherChunks = [];
  const decipher = crypto.createDecipheriv('aes-128-ecb', desKey, '');
  decipher.setAutoPadding(true);
  cipherChunks.push(decipher.update(data, 'base64', 'utf8'));
  cipherChunks.push(decipher.final('utf8'));
  return cipherChunks.join('');
};

// AES加密
exports.desEncode = function desEncode (desKey, data) {
  const cipherChunks = [];
  const cipher = crypto.createCipheriv('aes-128-ecb', desKey, '');
  cipher.setAutoPadding(true);
  cipherChunks.push(cipher.update(data, 'utf8', 'base64'));
  cipherChunks.push(cipher.final('base64'));

  return cipherChunks.join('');
};

exports.md5Encode = function md5Encode (data) {
  return crypto.createHash('md5').update(data).digest('hex');
};

exports.validatorMsg = (i18nKey, ...params) => {
  return (value, { req, location, path }) => {
    return req.t(i18nKey, ...params);
  };
};

exports.isProd = () => {
  return process.env.NODE_ENV === 'production';
};

exports.delay = (ms) => {
  return new Promise(resolve => setTimeout(resolve, ms));
};
