const fs = require('fs');
const path = require('path');
const logger = require('../logger');

class SendAdapter {
  #vendors;

  constructor () {
    (async () => {
      await this.getModule();
    })();
  }

  async getModule () {
    try {
      const vendorsPath = path.resolve(__dirname, 'vendors');
      const modules = new Map();
      if (!fs.existsSync(vendorsPath)) {
        return modules;
      }
      fs.readdirSync(vendorsPath, { withFileTypes: true }).forEach(file => {
        const filePath = path.resolve(vendorsPath, file.name);
        const module = require(filePath);
        const gameParameter = module.VendorName;
        modules.set(gameParameter, module);
      });
      this.#vendors = modules;
    } catch (error) {
      logger.error('无法载入系统发送解析模组:', error);
      return {};
    }
  }

  async sendMsg (method = 'sms', param = {}) {
    if (!this.#vendors.has(method)) {
      return { code: 1, message: 'not this method' };
    }
    const module = this.#vendors.get(method);
    return await module.sendMsg(param);
  }

  async getBalance (method = 'sms') {
    const module = this.#vendors.get(method);
    return await module.getBalance();
  }
}

module.exports = new SendAdapter();
