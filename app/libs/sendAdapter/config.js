require('dotenv').config();

module.exports = {
  sms: {
    baseUrl: process.env.SMS_BASE_URL,
    appId: process.env.SMS_APP_ID,
    apiKey: process.env.SMS_API_KEY,
    apiPwd: process.env.SMS_API_PASSWORD
  }
};
