const axios = require('axios');
const { sms } = require('../config');
const moment = require('moment');
const crypto = require('crypto');
const logger = require('../../logger');
const { isProd } = require('../../utils');

class SmsHandle {
  VendorName = 'sms';

  /** sendMsg
     * @param {object} param {}
     * @param {string} param.target '886900000000,886912345678'
     * @param {string} param.content 'hallo world'
     * @param {string} param.appId 'xLi0DCsa'
     */
  async sendMsg (param) {
    try {
      const target = param.target;
      const requestData = {
        appId: param.appId || sms.appId,
        numbers: param.target,
        content: isProd() ? param.content : param.content + `\nsend time: ${moment().format()}`,
        senderId: process.env.SMS_SENDER_ID
      };
      if (!target || !param.content) {
        return { status: -1, reason: 'target or content is null' };
      }
      const timestamp = parseInt((+moment()) / 1000);
      const sign = crypto.createHash('md5').update(`${sms.apiKey}${sms.apiPwd}${timestamp}`).digest('hex');
      const url = `${sms.baseUrl}/sendSms`;
      const res = await axios.post(url, requestData, {
        headers: {
          'Content-Type': 'application/json;charset=UTF-8',
          Sign: sign,
          Timestamp: String(timestamp),
          'Api-Key': sms.apiKey
        }
      });

      const status = Number(res.data.status);
      const reason = res.data.reason;
      const success = res.data.success;
      const fail = res.data.fail;
      const rows = res.data.array;
      logger.info(`sms is sent, target: ${target}, content: ${requestData.content}, reason: ${reason}, fail: ${fail}, success: ${success}`);
      return { status, reason, success, fail, rows };
    } catch (e) {
      logger.error(e);
      return { code: -999, reason: 'system error' };
    }
  }

  /** getBalance
     */
  async getBalance () {
    try {
      const timestamp = parseInt((+moment()) / 1000);
      const sign = crypto.createHash('md5').update(`${sms.apiKey}${sms.apiPwd}${timestamp}`).digest('hex');

      const url = `${sms.baseUrl}/getBalance`;
      const res = await axios.get(url, {
        headers: {
          'Content-Type': 'application/json;charset=UTF-8',
          Sign: sign,
          Timestamp: timestamp + '',
          'Api-Key': sms.apiKey
        }
      });
      const code = res.data.status;
      const reason = res.data.reason;
      const balance = res.data.balance;
      const gift = res.data.gift;
      const credit = res.data.credit;
      return { code, reason, balance, gift, credit };
    } catch (e) {
      logger.error(e);
      return { code: -999, reason: 'system error' };
    }
  }

  /** getSentRcd
     */
  async getSentRcd () {
    try {
      const timestamp = parseInt((+moment()) / 1000);
      const sign = crypto.createHash('md5').update(`${sms.apiKey}${sms.apiPwd}${timestamp}`).digest('hex');
      const url = `${sms.baseUrl}/getSentRcd?appId=${sms.appId}`;
      const res = await axios.get(url, {
        headers: {
          'Content-Type': 'application/json;charset=UTF-8',
          Sign: sign,
          Timestamp: timestamp + '',
          'Api-Key': sms.apiKey
        }
      });
      const code = res.data.status;
      const reason = res.data.reason;
      const balance = res.data.balance;
      const gift = res.data.gift;
      const credit = res.data.credit;
      return { code, reason, balance, gift, credit };
    } catch (e) {
      logger.error(e);
      return { code: -999, reason: 'system error' };
    }
  }
}

module.exports = new SmsHandle();
