const fs = require('fs');
const path = require('path');
const logger = require('../logger');

class GameProviderAdapter {
  static ERROR_GAME_NOT_FOUND = new Error('Game not found');
  static ERROR_METHOD_NOT_SUPPORTED = new Error('Method not supported');

  #providers;

  constructor () {
    (async () => {
      await this.getModule();
    })();
  }

  async getModule () {
    try {
      const providersPath = path.resolve(__dirname, 'providers');
      const modules = new Map();
      if (!fs.existsSync(providersPath)) {
        return modules;
      }
      fs.readdirSync(providersPath, { withFileTypes: true }).forEach(file => {
        const filePath = path.resolve(providersPath, file.name);
        const gameProviderName = file.name.replace('.js', '');
        const module = require(filePath)[`${gameProviderName}Provider`];
        modules.set(gameProviderName, module);
      });
      this.#providers = modules;
    } catch (error) {
      error.message = `'GameProviderAdapter getModule error: ${error.message}`;
      logger.error(error);
      return {};
    }
  }

  #handleProviderNotFound (game) {
    if (!this.#providers.has(game)) {
      throw GameProviderAdapter.ERROR_GAME_NOT_FOUND;
    }
  }

  async getLoginUrl (game = 'vp', param = {}) {
    this.#handleProviderNotFound(game);
    const module = this.#providers.get(game);
    if (typeof module.getLoginUrl !== 'function') {
      throw GameProviderAdapter.ERROR_METHOD_NOT_SUPPORTED;
    }
    return await module.getLoginUrl(param);
  }

  async getGameRecord (game = 'vp', param = {}) {
    this.#handleProviderNotFound(game);
    const module = this.#providers.get(game);
    if (typeof module.getGameRecord !== 'function') {
      throw GameProviderAdapter.ERROR_METHOD_NOT_SUPPORTED;
    }
    return await module.getGameRecord(param);
  }

  async getBalance (game = 'vp', param = {}) {
    this.#handleProviderNotFound(game);
    const module = this.#providers.get(game);
    if (typeof module.getBalance !== 'function') {
      throw GameProviderAdapter.ERROR_METHOD_NOT_SUPPORTED;
    }
    return await module.getBalance(param);
  }

  async seamlessWallet (game = 'vp', param = {}) {
    this.#handleProviderNotFound(game);
    const module = this.#providers.get(game);
    if (typeof module.seamlessWallet !== 'function') {
      throw GameProviderAdapter.ERROR_METHOD_NOT_SUPPORTED;
    }
    return await module.seamlessWallet(param);
  }
}

module.exports = { gameProviderAdapter: new GameProviderAdapter(), GameProviderAdapter };
