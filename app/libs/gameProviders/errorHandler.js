const { VpProvider } = require('./providers/vp');
module.exports.vpErrorHandler = async (err, req, res, next) => {
  logger.error(err);
  switch (err) {
    case VpProvider.ERR_CHANNEL_NOT_FOUND:
      return res.apiRes('invalidArgument', { message: req.t('5_failedGetAgentMsg') });
    case VpProvider.ERR_AUTH_PARAMETER_IS_NOT_SET:
      return res.apiRes('invalidArgument', { message: `channel.auth_parameter ${req.t('20_notSet')}` });
    case VpProvider.ERR_AUTH_PARAMETER_FORMAT_INCORRECT:
      return res.apiRes('invalidArgument', { message: `channel.auth_parameter ${req.t('77_fillParametersError')}` });
    case VpProvider.ERR_RES_NOT_IN_WHITELIST:
      return res.apiRes('unauthenticated', { message: `${req.t('0_gameChannel')}: ${req.t('0_channelWhiteList')}` });
    case VpProvider.ERR_RES_INVALID_PARAMS:
      return res.apiRes('invalidArgument', { message: `${req.t('0_gameChannel')}: ${req.t('0_verifica0_validationFieldMissingionMissing')}` });
    case VpProvider.ERR_RES_FAILED:
      return res.apiRes('unknown', { message: `${req.t('0_gameChannel')}: ${req.t('0_plzContactCustomerService')}` });
    default:
      next(err);
  }
};
