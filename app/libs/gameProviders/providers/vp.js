const db = require('../../../db/models');
const channels = db.channels;
// const walletRepository = require('../../../repositories/wallet');
const channelRepository = require('../../../repositories/channel');
const utils = require('../../utils');
const axios = require('axios');
const moment = require('moment');
const queryString = require('querystring');

class VpProvider {
  name = 'vp';
  static ERR_CHANNEL_NOT_FOUND = new Error('Channel not found');
  static ERR_AUTH_PARAMETER_IS_NOT_SET = new Error('Auth parameter is not set');
  static ERR_AUTH_PARAMETER_FORMAT_INCORRECT = new Error('Auth parameter format is not correct');
  static ERR_RES_NOT_IN_WHITELIST = new Error('Not in whitelist');
  static ERR_RES_RECORD_NOT_EXISTS = new Error('Record not exists');
  static ERR_RES_TOO_MANY_REQUESTS = new Error('Too many requests');
  static ERR_RES_INVALID_PARAMS = new Error('Invalid parameters');
  static ERR_RES_FAILED = new Error('Requset failed');
  static ERR_MD5 = new Error('MD5 key error');
  static ERR_DES_DECODE = new Error('Decryption error');
  static ERR_FORMAT = new Error('Data format error');
  #ignoreResWarnCodes = [16, 43];

  async #getChannelParamter (channelId) {
    const channel = await channels.findOne({
      attributes: ['authParameter', 'symbol', 'language', 'promotionUrl', 'promotionUrlType'],
      where: {
        id: channelId
      },
      raw: true
    });
    if (!channel) {
      throw VpProvider.ERR_CHANNEL_NOT_FOUND;
    }
    if (!channel.authParameter) {
      throw VpProvider.ERR_AUTH_PARAMETER_IS_NOT_SET;
    }
    const { channelUrl, agentId, md5Key, desKey } = channel.authParameter;
    if (!agentId && !md5Key && !desKey && !channelUrl) {
      logger.warn(`channel_id: ${channelId}, 未设置正确的参数格式, 例: {"desKey": "44BB0C62445FFCDD", "md5Key": "7C44054F1958981F", "agentId": 1, "recordUrl": "http://192.168.32.33:2290/getRecordHandle", "channelUrl": "http://192.168.32.33:2090/channelHandle"}`);
      throw VpProvider.ERR_AUTH_PARAMETER_FORMAT_INCORRECT;
    }
    return channel;
  }

  async #reqObjParser (param) {
    const channel = await this.#getChannelParamter(param.channelId);
    const timestamp = new Date().getTime();
    let params, url;
    switch (param.type) {
      // 登入平台
      case 0: {
        url = channel.authParameter.channelUrl;
        const currency = channel.symbol || 'JPY';
        const kindId = param.kindId || 0;
        const paramOrg = 's=' + param.type + '&account=' + param.username + '&orderid=' + String(channel.authParameter.agentId) + moment(timestamp).format('YYYYMMDDhhmmss') + '001' + param.username + '&ip=' + param.ip + '&lineCode=' + process.env.SERVICE_NAME + '&tiny=' + 0 + `&currency=${currency}&money=0&KindID=${kindId}`;
        params = {
          timestamp,
          agent: channel.authParameter.agentId,
          param: utils.desEncode(channel.authParameter.desKey, paramOrg),
          key: utils.md5Encode(String(channel.authParameter.agentId) + timestamp + channel.authParameter.md5Key)
        };
        break;
      }
      // 查詢餘額
      case 1: {
        url = channel.authParameter.channelUrl;
        const currency = channel.symbol || 'JPY';
        const paramOrg = `s=${param.type}&account=${param.username}&currency=${currency}`;
        params = {
          timestamp,
          agent: channel.authParameter.agentId,
          param: utils.desEncode(channel.authParameter.desKey, paramOrg),
          key: utils.md5Encode(String(channel.authParameter.agentId) + timestamp + channel.authParameter.md5Key)
        };
        break;
      }
      // 查询游戏注单
      case 9:
        url = channel.authParameter.recordUrl;
        params = {
          timestamp,
          agent: channel.authParameter.agentId,
          param: utils.desEncode(channel.authParameter.desKey, `s=${param.type}&startTime=${param.startTime}&endTime=${param.endTime}`),
          key: utils.md5Encode(String(channel.authParameter.agentId) + timestamp + channel.authParameter.md5Key)
        };
        break;
    }
    return { url, params, channel };
  }

  #gameRecordOrganization (data) {
    const newArr = [];
    try {
      const list = data.d.list;
      switch (parseInt(data.s)) {
        case 109:
          for (let i = 0; i < data.d.count; i++) {
            const tmp = {};
            Object.keys(list).forEach(key => {
              tmp[key] = list[key][i];
            });
            newArr.push(tmp);
          }
          break;
        default:
          break;
      }
      return newArr;
    } catch (error) {
      logger.warn('拉单资料整理异常');
      return newArr;
    }
  }

  #resCheck (result) {
    if (result.status !== 200 || !result.data.d || result.data.d.code !== 0) {
      // 符合預期response code忽略warn log
      if (!this.#ignoreResWarnCodes.includes(result.data.d.code)) {
        logger.warn(`${result.request?._header} failed, response: ${JSON.stringify(result.data)}`);
      }
      switch (result.data.d.code) {
        // 渠道白名单错误
        case 5:
          throw VpProvider.ERR_RES_NOT_IN_WHITELIST;
        // 验证字段丢失
        case 6:
          throw VpProvider.ERR_RES_INVALID_PARAMS;
        // 数据不存在（当前没有注单）
        case 16:
          throw VpProvider.ERR_RES_RECORD_NOT_EXISTS;
        // 拉单过于频繁(两次拉单时间间隔必须大于 1 秒)
        case 43:
          throw VpProvider.ERR_RES_TOO_MANY_REQUESTS;
        // 其他错误
        default:
          throw VpProvider.ERR_RES_FAILED;
      }
    }
  }

  async getLoginUrl (param) {
    param.type = 0;
    const { url, params, channel } = await this.#reqObjParser(param);

    const result = await axios.get(url, { params, timeout: 300000 });
    this.#resCheck(result);

    const lang = channel.authParameter?.lang || channel.language;
    // 替换lang query parameter 使用代理设定的语系
    if (lang) {
      result.data.d.url = result.data.d.url.replace(/(lang=).*?(&|$)/, '$1' + lang.replace('_', '-') + '$2');
    }
    return result.data.d.url + (channel.promotionUrl && channel.promotionUrlType === 1 ? `&jumpType=3&backUrl=${channel.promotionUrl}` : '');
  }

  async getGameRecord (param) {
    param.type = 9;
    const { url, params } = await this.#reqObjParser(param);
    const result = await axios.get(url, { params });
    this.#resCheck(result);
    return this.#gameRecordOrganization(result.data);
  }

  async getBalance (param) {
    param.type = 1;
    const { url, params } = await this.#reqObjParser(param);
    const result = await axios.get(url, { params });
    this.#resCheck(result);
    return result.data.d.money;
  }

  async seamlessWallet (param) {
    const res = { s: 1000, m: '/channelHandle', d: {} };
    if (typeof param.agent !== 'string') throw VpProvider.ERR_FORMAT;
    if (typeof param.timestamp !== 'string') throw VpProvider.ERR_FORMAT;
    if (typeof param.param !== 'string') throw VpProvider.ERR_FORMAT;
    if (typeof param.key !== 'string') throw VpProvider.ERR_FORMAT;

    const channel = await channelRepository.getChannelByAuthParamAgentId(param.agent);
    if (!channel) {
      throw VpProvider.ERR_CHANNEL_NOT_FOUND;
    }
    const { channelUrl, agentId, md5Key, desKey } = channel.authParameter;
    if (!agentId && !md5Key && !desKey && !channelUrl) {
      logger.warn(`channel_id: ${channel.id}, 未设置正确的auth_parameter格式, 例: {"desKey": "44BB0C62445FFCDD", "md5Key": "7C44054F1958981F", "agentId": 1, "recordUrl": "http://192.168.32.33:2290/getRecordHandle", "channelUrl": "http://192.168.32.33:2090/channelHandle"}`);
      throw VpProvider.ERR_AUTH_PARAMETER_FORMAT_INCORRECT;
    }

    const keyOrg = param.agent + param.timestamp + md5Key;
    const serverKey = utils.md5Encode(keyOrg);
    const decodeParams = queryString.parse(utils.desDecode(desKey, param.param));
    if (serverKey !== param.key) throw VpProvider.ERR_MD5;
    if (!decodeParams) throw VpProvider.ERR_DES_DECODE;
    // const { account, orderId, money, data, gameId, kindId, currency } = decodeParams;
    // const s = parseInt(decodeParams.s);
    // switch (s) {
    //   case 1001: {
    //     if (!account || account === undefined) throw VpProvider.ERR_FORMAT;
    //     const username = account.replace('', '');
    //     const accountBalance = await walletRepository.getAccountBalance({ serverCode, account, agent, currency });
    //     if (!accountBalance) await walletRepository.insertAccount({ serverCode, agent, account, money: 1000000, currency });
    //     res.d.code = statusCode.SUCCESS.code;
    //     res.d.account = account;
    //     res.d.money = !accountBalance ? 1000000 : accountBalance.money;
    // utils.resEnd(res, res);
    //     break;
    //   }
    // }
    return res;
  }
}
module.exports = { vpProvider: new VpProvider(), VpProvider };
