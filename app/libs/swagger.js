// const statusCode = require('./statusCode');
// const apiResExamples = {};
// for (const [key, value] of Object.entries(statusCode)) {
//   const { code, codeNO, message } = value;
//   apiResExamples[key] = { value: { code, codeNO, message, body: { example: `your body structure, ${codeNO}`  } };
// }

exports.swaggerDefinition = {
  openapi: '3.0.0',
  info: {
    title: process.env.SERVICE_NAME,
    description: `${process.env.SERVICE_NAME} Information`
  },
  servers: [
    {
      url: 'http://192.168.32.32:9528',
      description: 'Development server'
    },
    {
      url: 'http://192.168.32.36:9528',
      description: 'System integration testing server'
    }
  ],
  tags: [
    {
      name: 'general',
      description: '通用模組'
    },
    {
      name: 'player',
      description: '玩家服務模組'
    },
    {
      name: 'channel',
      description: '代理服務模組'
    }
  ],
  components: {
    schemas: {
      ApiResponse: {
        type: 'object',
        properties: {
          code: { type: 'string' },
          codeNO: { type: 'integer' },
          message: { type: 'string' },
          body: { type: 'any', description: 'your response body structure' }
        },
        example: {
          code: 'OK',
          codeNO: 0,
          message: '操作成功'
        }
      }
    }
  }
};
