const randomIndex = (max) => {
  max = Number(max);
  return Math.floor(Math.random() * max);
};

const getRandomOneOfString = (str) => {
  return str.charAt(randomIndex(str.length));
};

const shuffle = (arr) => {
  if (!Array.isArray(arr)) return [];
  arr.forEach((element, index) => {
    const nextIndex = randomIndex(arr.length);
    [arr[index], arr[nextIndex]] = [arr[nextIndex], arr[index]];
  });
  return arr;
};

const charset = {
  upperCase: 'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
  lowerCase: 'abcdefghijklmnopqrstuvwxyz',
  numbers: '0123456789',
  symbols: '!@#$%^&*()+_-=}{[]|:;"/?.><,`~'
};

const similarCharacters = /[ilLI1|`:;oO0]/g;

const noSimilarCharset = Object.keys(charset).reduce((result, key) => {
  result[key] = charset[key].replace(similarCharacters, '');
  return result;
}, {});

const option = {
  upperCase: true,
  lowerCase: true,
  numbers: true,
  symbols: true,
  similarCharacters: false
};

exports.generatePassword = (minLength, maxLength) => {
  minLength = minLength || 8;
  maxLength = minLength || maxLength;
  const length = minLength + randomIndex(maxLength - minLength);
  const useCharset = option.similarCharacters ? charset : noSimilarCharset;
  const allConcat = Object.keys(useCharset).reduce((result, key) => result + useCharset[key], '');
  const pwdArray = Object.keys(useCharset)
    .filter(key => option[key])
    .map(key => getRandomOneOfString(useCharset[key]));
  for (let i = length - pwdArray.length; i > 0; i--) {
    pwdArray.push(getRandomOneOfString(allConcat));
  }
  return shuffle(pwdArray).join('');
};
