/**
 * @typedef {import('ioredis').Redis} Redis
 */

const logger = require('../libs/logger');
const { isProd } = require('../libs/utils');

class VerificationRespository {
  /**
   * Create a Verification instance.
   * @constructor
   * @param {Redis} client - The Redis client for data storage.
   * @param {SendAdapter} sender - The SendAdapter for sending messages.
   */
  constructor (client, sender, users) {
    this.client = client;
    this.sender = sender;
    this.users = users;
  }

  async sendVerification (phoneNumber, channelId, i18n) {
    const phoneNumberKey = `verifyCode:${phoneNumber}-${channelId}`;
    const code = Math.floor(100000 + Math.random() * 900000).toString();
    if (isProd()) {
      const content = i18n('0_phoneVerificationMsg', code);
      const { status } = await this.sender.sendMsg('sms', { target: phoneNumber, content });
      if (status !== 0) {
        logger.warn(`发送手机简讯失败, status: ${status}, 请确认状态码 https://www.onbuka.cn/sms-api1/#s1`);
        switch (status) {
          case -9:
            return { message: i18n('0_phoneFormatErrorMsg'), code };
          default:
            return { message: `${i18n('0_sendVerification')}${i18n('0_fail')}`, code };
        }
      }
    }
    await this.client.set(phoneNumberKey, code, 'EX', 600);
    return { message: '', code };
  }

  async verifyPhoneSMSCode (params) {
    const { phoneNumber, channelId, verifyCode } = params;
    const phoneNumberKey = `verifyCode:${phoneNumber}-${channelId}`;
    const smsCode = await this.client.get(phoneNumberKey);
    if (!smsCode || (Number(smsCode) !== Number(verifyCode))) {
      return false;
    }
    return true;
  }

  async sendForgetPassowrd (username, phoneNumber, newPassowrd, i18n) {
    const content = i18n('0_forgetPasswordMsg', username, newPassowrd);
    if (isProd()) {
      const { status } = await this.sender.sendMsg('sms', { target: phoneNumber, content });
      if (status !== 0) {
        logger.warn(`发送手机简讯失败, status: ${status}, 请确认状态码 https://www.onbuka.cn/sms-api1/#s1`);
        switch (status) {
          case -9:
            return { message: i18n('0_phoneFormatErrorMsg') };
          default:
            return { message: `${i18n('0_sendVerification')}${i18n('0_fail')}` };
        }
      }
    }
    return { message: '' };
  }
}

module.exports = new VerificationRespository(require('../config/redis'), require('../libs/sendAdapter/sendAdapter'));
