const db = require('../db/models');
const channels = db.channels;

class ChannelRespository {
  async getChannelByAuthParamAgentId (agentId) {
    return await channels.findOne({
      attributes: ['id', 'authParameter', 'symbol', 'language'],
      where: {
        'authParameter.agentId': agentId
      },
      raw: true
    });
  }
}
module.exports = new ChannelRespository();
