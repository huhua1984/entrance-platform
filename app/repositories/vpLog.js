const db = require('../db/models');
const games = db.games;
const vpLog = db.vpLog;

const _ = require('lodash');

class VpLogRespository {
  async bulkUpsert (record, channelId) {
    const gameInfo = await games.findAll({ include: [{ association: 'gateway', where: { slug: ['vp', 'nc'] } }], raw: true });
    const gameInfoByMap = _.keyBy(gameInfo, 'gameCode');
    const vpLogs = record.map(row => {
      const underScoreIndex = row.Accounts.indexOf('_');
      const username = underScoreIndex !== -1 ? row.Accounts.substring(underScoreIndex + 1) : row.Accounts;
      const gameId = gameInfoByMap[row.KindID]?.id || '';
      // TODO wallet
      return {
        walletId: '',
        username,
        language: row.Language,
        roomCode: row.ServerID,
        channelId,
        gameId,
        recordDetails: row,
        cellScore: row.CellScore,
        allBet: row.AllBet,
        profit: row.Profit,
        revenue: row.Revenue,
        gameUserNo: row.GameID,
        gameStartTime: row.GameStartTime,
        gameEndTime: row.GameEndTime,
        currency: row.Currency
      };
    });
    return await vpLog.bulkCreate(vpLogs, {
      updateOnDuplicate: ['gameUserNO', 'gameEndTime']
    });
  }
}
module.exports = new VpLogRespository();
