const db = require('../db/models');
const wallets = db.wallets;

class WalletRespository {
  async getAccountBalance (userId, currency) {
    return await wallets.findOne({
      where: {
        userId,
        currency
      },
      raw: true
    });
  }
}
module.exports = new WalletRespository();
