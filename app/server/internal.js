const createError = require('http-errors');
const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const helmet = require('helmet');

const morganMiddleware = require('../libs/middlewares/morgan');
const { scanRoutes, isProd } = require('../libs/utils');
const listEndpoints = require('express-list-endpoints');
const logger = require('../libs/logger');
global.logger = logger;
const errorHandler = require('../libs/middlewares/errorHandler');
const apiResHandler = require('../libs/middlewares/apiResHandler');

const app = express();

const serverType = path.parse(__filename).name;

/**
 * Middlewares, Passport, Session
 */

if (isProd()) {
  app.use(helmet());
}
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(morganMiddleware);

/**
 * CORS setting
 */
const cors = require('cors');
app.use(cors());

/**
 * I18N setting
 */
const { I18n } = require('i18n');
const pathHelper = require('path');
const directory = pathHelper.join(pathHelper.resolve(), '..', 'locales');
const fs = require('fs');
const locales = [];
const files = fs.readdirSync(directory);

const jsonFiles = files.filter(file => path.extname(file) === '.json');
locales.push(...jsonFiles.map(file => path.parse(file).name));

if (locales.length === 0) {
  logger.error(`未找到语言文件，请检查 ${directory} 内的档案`);
  process.exit();
}

const i18n = new I18n();
const defaultLocale = process.env.DEFAULT_LOCALE || 'zh-cn';
i18n.configure({
  locales,
  directory,
  defaultLocale,
  retryInDefaultLocale: false,
  header: '',
  cookie: 'lang',
  autoReload: true,
  syncFiles: false,
  updateFiles: false,
  queryParameter: 'lang',
  api: {
    __: 't',
    __n: 'tn'
  },
  missingKeyFn: function (locale, value) {
    if (value === '') {
      return undefined;
    }
    logger.warn(`缺少翻译: 语系 = ${locale}, key = ${value}`);
    return value;
  }
});

app.use(i18n.init);

// api res format
app.use(apiResHandler);

// back-end setting
app.set('trust proxy', 1);

// 除了page & genearl之外 會以檔案名稱作group routes
const routes = scanRoutes(path.join(__dirname, '..', 'routes', serverType));
if (routes.length > 0) {
  for (const route of routes) {
    try {
      if (route.group === '/page') {
        continue;
      }
      app.use('/api' + route.group, require(route.target));
    } catch (error) {
      logger.error(`${serverType} router use failed。 group: ${route.group} 。 file: ${route.target} 。 error: ${error.message}`);
    }
  }
}

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(errorHandler);

// list all endpoints
logger.info(`${serverType} list all routes:`);
listEndpoints(app).forEach(el => {
  logger.info(`${serverType} route: ${el.path}, methods: ${el.methods.join(', ')}, middlewares: ${el.middlewares.join(', ')}`);
});

module.exports = app;
