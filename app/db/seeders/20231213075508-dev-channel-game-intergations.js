'use strict';
const { v4: uuidv4 } = require('uuid');

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  up: async (queryInterface, Sequelize) => {
    // Assuming you have instances of games and channels created before running this seed

    // Fetch game and channel IDs
    const games = await queryInterface.sequelize.query('SELECT `id` as `game_id`,  `gateway_id`,  `game_name`,  `game_nick_name`,  `game_code`,  `status`,  `game_type`,  `sort_id`,  `is_hot`,  `hot_order`,  `flag`,  `group`,  `remark`,  `game_img`,  `lang` FROM games where `deleted_at` is null ');
    const channels = await queryInterface.sequelize.query('SELECT id FROM channels');

    const gameIntegrationsData = [];

    // Create seed data for game_integrations based on existing games and channels
    games[0].forEach((game) => {
      channels[0].forEach((channel) => {
        const gameIntegration = { id: uuidv4(), ...game, channel_id: channel.id };
        gameIntegrationsData.push(gameIntegration);
      });
    });

    // Insert seed data into game_integrations table
    return queryInterface.bulkInsert('game_integrations', gameIntegrationsData, {});
  },

  down: async (queryInterface, Sequelize) => {
    // Remove all seeded data from game_integrations table
    return queryInterface.bulkDelete('game_integrations', null, {});
  }
};
