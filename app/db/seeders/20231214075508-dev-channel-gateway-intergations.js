'use strict';
const { v4: uuidv4 } = require('uuid');

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  up: async (queryInterface, Sequelize) => {
    // Assuming you have instances of gateways and channels created before running this seed

    // Fetch gateway and channel IDs
    const gateways = await queryInterface.sequelize.query('SELECT `id` as `gateway_id`,  `status` FROM gateways where `deleted_at` is null ');
    const channels = await queryInterface.sequelize.query('SELECT id FROM channels');

    const gatewayIntegrationsData = [];

    // Create seed data for gateway_integrations based on existing gateways and channels
    gateways[0].forEach((gateway) => {
      channels[0].forEach((channel) => {
        const gatewayIntegration = { id: uuidv4(), ...gateway, channel_id: channel.id };
        gatewayIntegrationsData.push(gatewayIntegration);
      });
    });

    // Insert seed data into gateway_integrations table
    return queryInterface.bulkInsert('gateway_integrations', gatewayIntegrationsData, {});
  },

  down: async (queryInterface, Sequelize) => {
    // Remove all seeded data from gateway_integrations table
    return queryInterface.bulkDelete('gateway_integrations', null, {});
  }
};
