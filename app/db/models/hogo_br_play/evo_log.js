module.exports = function (sequelize, DataTypes) {
  return sequelize.define('evo_log', {
    id: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    wallet_id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      comment: '钱包ID'
    },
    GameID: {
      type: DataTypes.STRING(80),
      allowNull: false,
      comment: '游戏编码'
    },
    RecordID: {
      type: DataTypes.STRING(50),
      allowNull: false,
      comment: '游戏回合ID'
    },
    BankID: {
      type: DataTypes.STRING(50),
      allowNull: false,
      comment: '交易ID'
    },
    RecordDetails: {
      type: DataTypes.TEXT,
      allowNull: true,
      comment: '明细'
    },
    Bet: {
      type: DataTypes.DECIMAL(30, 2),
      allowNull: false,
      defaultValue: 0.00,
      comment: '押注金额'
    },
    Win: {
      type: DataTypes.DECIMAL(30, 2),
      allowNull: false,
      defaultValue: 0.00,
      comment: '游戏赢分'
    },
    JPBet: {
      type: DataTypes.DECIMAL(30, 2),
      allowNull: false,
      defaultValue: 0.00,
      comment: '彩金押注金额'
    },
    JPPrize: {
      type: DataTypes.DECIMAL(30, 2),
      allowNull: false,
      defaultValue: 0.00,
      comment: '彩金赢分'
    },
    NetWin: {
      type: DataTypes.DECIMAL(30, 2),
      allowNull: false,
      defaultValue: 0.00,
      comment: '总输赢'
    },
    RequireAmt: {
      type: DataTypes.DECIMAL(30, 2),
      allowNull: false,
      defaultValue: 0.00,
      comment: '实际押注金额(仅推币机与捕鱼机)'
    },
    GameDate: {
      type: DataTypes.DATE,
      allowNull: false
    },
    CreateDate: {
      type: DataTypes.DATE,
      allowNull: false
    },
    before_result: {
      type: DataTypes.TEXT,
      allowNull: true,
      comment: '提交前'
    },
    result_return: {
      type: DataTypes.TEXT,
      allowNull: true,
      comment: '提交后'
    },
    refund_return: {
      type: DataTypes.TEXT,
      allowNull: true,
      comment: '退款提交'
    },
    status: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1,
      comment: '状态 1提交 2 完成 3撤回'
    }
  }, {
    sequelize,
    tableName: 'evo_log',
    timestamps: true,
    indexes: [
      {
        name: 'index_id_creatime',
        using: 'BTREE',
        fields: [
          { name: 'id' },
          { name: 'created_at' }
        ]
      }
    ]
  });
};
