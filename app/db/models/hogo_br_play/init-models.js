const DataTypes = require('sequelize').DataTypes;
const _cq9Log = require('./cq9_log');
const _evoLog = require('./evo_log');
const _fcLog = require('./fc_log');
const _pgLog = require('./pg_log');
const _ppLog = require('./pp_log');
const _vpLog = require('./vp_log');
const _vpOrder = require('./vp_order');

function initModels (sequelize) {
  const cq9Log = _cq9Log(sequelize, DataTypes);
  const evoLog = _evoLog(sequelize, DataTypes);
  const fcLog = _fcLog(sequelize, DataTypes);
  const pgLog = _pgLog(sequelize, DataTypes);
  const ppLog = _ppLog(sequelize, DataTypes);
  const vpLog = _vpLog(sequelize, DataTypes);
  const vpOrder = _vpOrder(sequelize, DataTypes);

  return {
    cq9Log,
    evoLog,
    fcLog,
    pgLog,
    ppLog,
    vpLog,
    vpOrder
  };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;
