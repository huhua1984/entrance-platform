module.exports = function (sequelize, DataTypes) {
  return sequelize.define('pg_log', {
    id: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    wallet_id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      comment: '钱包ID'
    },
    game_id: {
      type: DataTypes.STRING(80),
      allowNull: false,
      comment: '游戏编码'
    },
    transaction_id: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    bet_amount: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
      comment: 'bet金额'
    },
    win_amount: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
      comment: 'win金额'
    },
    transfer_amount: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
      comment: '调整金额'
    },
    bet_details: {
      type: DataTypes.TEXT,
      allowNull: false,
      comment: '数据'
    },
    before_result: {
      type: DataTypes.TEXT,
      allowNull: false,
      comment: '提交前'
    },
    result_return: {
      type: DataTypes.TEXT,
      allowNull: false,
      comment: '提交后'
    },
    status: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1,
      comment: '状态 1提交 2 完成 3撤回'
    }
  }, {
    sequelize,
    tableName: 'pg_log',
    timestamps: true,
    indexes: [
      {
        name: 'index_id_creatime',
        using: 'BTREE',
        fields: [
          { name: 'id' },
          { name: 'created_at' }
        ]
      },
      {
        name: 'index_transactionId',
        using: 'BTREE',
        fields: [
          { name: 'transaction_id' }
        ]
      }
    ]
  });
};
