const Sequelize = require('sequelize');
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('playlist_bet_temp', {
    id: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    game_id: {
      type: DataTypes.CHAR(36),
      allowNull: false
    },
    user_id: {
      type: DataTypes.CHAR(36),
      allowNull: false
    },
    wallet_id: {
      type: DataTypes.CHAR(36),
      allowNull: false
    },
    gateway_id: {
      type: DataTypes.CHAR(36),
      allowNull: false
    },
    channel_id: {
      type: DataTypes.CHAR(36),
      allowNull: false
    },
    consume: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0
    },
    reward: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0
    },
    game_code: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    time: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: Sequelize.Sequelize.literal('CURRENT_TIMESTAMP')
    },
    oid: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    type: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    state: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'playlist_bet_temp',
    timestamps: false,
    indexes: [
      {
        name: 'index_id_createdate',
        using: 'BTREE',
        fields: [
          { name: 'id' },
          { name: 'time' }
        ]
      },
      {
        name: 'index_time',
        using: 'BTREE',
        fields: [
          { name: 'time' }
        ]
      }
    ]
  });
};
