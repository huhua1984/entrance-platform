module.exports = function (sequelize, DataTypes) {
  return sequelize.define('vp_order', {
    id: {
      autoIncrement: true,
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true
    },
    channel_id: {
      type: DataTypes.STRING(50),
      allowNull: false,
      comment: '代理ID'
    },
    order_id: {
      type: DataTypes.STRING(190),
      allowNull: false,
      comment: '订单编号'
    },
    username: {
      type: DataTypes.STRING(255),
      allowNull: false,
      comment: '玩家名'
    },
    origin_money: {
      type: DataTypes.DOUBLE,
      allowNull: false,
      comment: '帐变前玩家原始钱包金额'
    },
    money: {
      type: DataTypes.DOUBLE,
      allowNull: false,
      comment: '订单金额变化数量'
    },
    status: {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: '订单状态1是成功 2是失败'
    },
    type: {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: '订单类型0玩家上分1玩家下分2代理充值'
    },
    kind_id: {
      type: DataTypes.STRING(20),
      allowNull: true,
      comment: '遊戲ID'
    },
    game_id: {
      type: DataTypes.STRING(200),
      allowNull: true,
      defaultValue: '0',
      comment: '遊戲局號'
    },
    game_user_no: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    currency: {
      type: DataTypes.STRING(50),
      allowNull: false,
      comment: '币别'
    },
    all_bet: {
      type: DataTypes.DOUBLE,
      allowNull: false,
      defaultValue: 0,
      comment: '總投注'
    },
    cell_score: {
      type: DataTypes.DOUBLE,
      allowNull: false,
      defaultValue: 0,
      comment: '有效投注'
    },
    revenue: {
      type: DataTypes.DOUBLE,
      allowNull: false,
      defaultValue: 0,
      comment: '抽水'
    },
    profit: {
      type: DataTypes.DOUBLE,
      allowNull: false,
      defaultValue: 0,
      comment: '盈利'
    },
    total_withdraw: {
      type: DataTypes.DOUBLE,
      allowNull: false,
      defaultValue: 0,
      comment: '攜入金額'
    }
  }, {
    sequelize,
    tableName: 'vp_order',
    timestamps: true,
    underscored: true,
    indexes: [
      {
        name: 'PRIMARY',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'id' }
        ]
      },
      {
        name: 'uni_idx_channelId_orderId',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'channel_id' },
          { name: 'order_id' }
        ]
      },
      {
        name: 'idx_username_createdAt',
        using: 'BTREE',
        fields: [
          { name: 'username' },
          { name: 'created_at' }
        ]
      }
    ]
  });
};
