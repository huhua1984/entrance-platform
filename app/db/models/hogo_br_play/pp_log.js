const Sequelize = require('sequelize');
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('pp_log', {
    id: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    wallet_id: {
      type: DataTypes.CHAR(36),
      allowNull: false
    },
    game_id: {
      type: DataTypes.STRING(80),
      allowNull: false
    },
    round_id: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    provider_id: {
      type: DataTypes.STRING(80),
      allowNull: false
    },
    bet_hash: {
      type: DataTypes.STRING(80),
      allowNull: false
    },
    bet_amount: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0
    },
    bet_reference: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    bet_timestamp: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: Sequelize.Sequelize.literal('CURRENT_TIMESTAMP')
    },
    bet_round_details: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    bet_return: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    status: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1
    },
    refund_return: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    result_hash: {
      type: DataTypes.STRING(80),
      allowNull: false
    },
    result_amount: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0
    },
    result_reference: {
      type: DataTypes.STRING(50),
      allowNull: false
    },
    result_timestamp: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: Sequelize.Sequelize.literal('CURRENT_TIMESTAMP')
    },
    result_round_details: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    result_return: {
      type: DataTypes.TEXT,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'pp_log',
    timestamps: true,
    indexes: [
      {
        name: 'index_id_creatime',
        using: 'BTREE',
        fields: [
          { name: 'id' },
          { name: 'created_at' }
        ]
      }
    ]
  });
};
