const Sequelize = require('sequelize');
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('playlist_1001', {
    id: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    game_id: {
      type: DataTypes.CHAR(36),
      allowNull: false
    },
    user_id: {
      type: DataTypes.CHAR(36),
      allowNull: false
    },
    wallet_id: {
      type: DataTypes.CHAR(36),
      allowNull: false
    },
    gateway_id: {
      type: DataTypes.CHAR(36),
      allowNull: false
    },
    channel_id: {
      type: DataTypes.CHAR(36),
      allowNull: false
    },
    consume: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0
    },
    reward: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0
    },
    game_code: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    time: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: Sequelize.Sequelize.literal('CURRENT_TIMESTAMP')
    },
    oid: {
      type: DataTypes.STRING(100),
      allowNull: false
    },
    cmoney: {
      type: DataTypes.BIGINT,
      allowNull: false
    },
    type: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    record_date: {
      type: DataTypes.DATE,
      allowNull: true
    },
    batch: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    state: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    radio: {
      type: DataTypes.DECIMAL(3, 2),
      allowNull: false
    },
    wtime: {
      type: DataTypes.DATE,
      allowNull: true
    },
    smoney: {
      type: DataTypes.BIGINT,
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'playlist_1001',
    hasTrigger: true,
    timestamps: false,
    indexes: [
      {
        name: 'index_oid',
        using: 'BTREE',
        fields: [
          { name: 'oid' }
        ]
      },
      {
        name: 'index_userId',
        using: 'BTREE',
        fields: [
          { name: 'user_id' }
        ]
      },
      {
        name: 'index_channelId',
        using: 'BTREE',
        fields: [
          { name: 'channel_id' }
        ]
      },
      {
        name: 'index_gameCode',
        using: 'BTREE',
        fields: [
          { name: 'game_code' }
        ]
      },
      {
        name: 'index_id_createdate',
        using: 'BTREE',
        fields: [
          { name: 'id' },
          { name: 'time' }
        ]
      }
    ]
  });
};
