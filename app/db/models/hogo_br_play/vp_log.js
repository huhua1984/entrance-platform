module.exports = function (sequelize, DataTypes) {
  return sequelize.define('vp_log', {
    id: {
      autoIncrement: true,
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    walletId: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      comment: '钱包ID'
    },
    username: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      comment: '玩家帳號'
    },
    channelId: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      comment: '玩家帳號'
    },
    gameId: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      defaultValue: '',
      comment: '游戏编码'
    },
    roomCode: {
      type: DataTypes.CHAR(36),
      allowNull: true,
      comment: '房间编码'
    },
    recordDetails: {
      type: DataTypes.JSON,
      allowNull: true,
      comment: '明细'
    },
    cellScore: {
      type: DataTypes.BIGINT,
      allowNull: true,
      comment: '有效投注额'
    },
    allBet: {
      type: DataTypes.BIGINT,
      allowNull: true,
      comment: '总投注'
    },
    profit: {
      type: DataTypes.BIGINT,
      allowNull: true,
      comment: '输赢金额'
    },
    revenue: {
      type: DataTypes.INTEGER,
      allowNull: true,
      comment: '抽水'
    },
    gameUserNo: {
      type: DataTypes.STRING(50),
      allowNull: true
    },
    gameStartTime: {
      type: DataTypes.DATE,
      allowNull: true,
      comment: '游戏开始时间'
    },
    gameEndTime: {
      type: DataTypes.DATE,
      allowNull: true,
      comment: '游戏结束时间'
    },
    language: {
      type: DataTypes.STRING(50),
      allowNull: false,
      comment: '语言'
    },
    currency: {
      type: DataTypes.STRING(50),
      allowNull: false,
      comment: '币别'
    },
    status: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1,
      comment: '状态 1提交 2 完成 3撤回'
    }
  }, {
    sequelize,
    tableName: 'vp_log',
    underscored: true,
    timestamps: true,
    indexes: [
      {
        name: 'PRIMARY',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'id' }
        ]
      },
      {
        name: 'uni_idx_GameUserNO_GameEndTime',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'GameUserNO' },
          { name: 'GameEndTime' }
        ]
      },
      {
        name: 'idx_user_id_GameEndTime',
        using: 'BTREE',
        fields: [
          { name: 'user_id' },
          { name: 'GameEndTime' }
        ]
      },
      {
        name: 'idx_roomCode',
        using: 'BTREE',
        fields: [
          { name: 'roomCode' }
        ]
      }
    ]
  });
};
