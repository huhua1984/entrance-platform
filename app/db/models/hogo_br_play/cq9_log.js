module.exports = function (sequelize, DataTypes) {
  return sequelize.define('cq9_log', {
    id: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    wallet_id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      comment: '錢包ID'
    },
    event_time: {
      type: DataTypes.STRING(35),
      allowNull: false,
      comment: '事件時間 格式為 RFC3339'
    },
    gamehall: {
      type: DataTypes.STRING(36),
      allowNull: false,
      comment: '遊戲廠商代號'
    },
    gamecode: {
      type: DataTypes.STRING(36),
      allowNull: false,
      comment: '遊戲代號'
    },
    roundid: {
      type: DataTypes.STRING(50),
      allowNull: false,
      comment: '注單號(為唯一值)'
    },
    amount: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
      comment: '下注金額'
    },
    mtcode: {
      type: DataTypes.STRING(70),
      allowNull: false,
      comment: '交易代碼'
    },
    session: {
      type: DataTypes.STRING(255),
      allowNull: false,
      comment: '交易ID'
    },
    platform: {
      type: DataTypes.STRING(20),
      allowNull: false,
      comment: '交易ID'
    },
    bet_return: {
      type: DataTypes.TEXT,
      allowNull: true
    },
    status: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 1,
      comment: '状态 1提交 2 完成 3撤回'
    },
    refund_return: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    result_mtcode: {
      type: DataTypes.STRING(70),
      allowNull: false,
      comment: '獲獎交易代碼(data內第一筆)'
    },
    result_data: {
      type: DataTypes.TEXT,
      allowNull: false
    },
    result_amount: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0
    },
    result_time: {
      type: DataTypes.STRING(35),
      allowNull: false,
      comment: '系統成單時間 格式為 RFC3339'
    },
    freegame: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    bonus: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    luckydraw: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    jackpot: {
      type: DataTypes.DECIMAL(30, 2),
      allowNull: false,
      defaultValue: 0.00
    },
    jackpotcontribution: {
      type: DataTypes.DECIMAL(30, 8),
      allowNull: false,
      defaultValue: 0.00000000
    },
    freeticket: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0
    },
    result_return: {
      type: DataTypes.TEXT,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'cq9_log',
    timestamps: true,
    indexes: [
      {
        name: 'index_id_creatime',
        using: 'BTREE',
        fields: [
          { name: 'id' },
          { name: 'created_at' }
        ]
      }
    ]
  });
};
