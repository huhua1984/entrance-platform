'use strict';

const path = require('path');
const Sequelize = require('sequelize');

const config = require('../../config/database.js');
const db = {};

for (const [database, dbConfig] of Object.entries(config)) {
  db[database] = new Sequelize(
    dbConfig.database,
    dbConfig.username,
    dbConfig.password,
    dbConfig
  );
}

for (const [dbName, sequelize] of Object.entries(db)) {
  const initModelPath = path.join(__dirname, dbName);
  const models = require(path.join(initModelPath, 'init-models'))(sequelize);
  sequelize.authenticate()
    .then(function () {
      logger.info(`Database [${dbName}] connected.`);
    })
    .catch(function (err) {
      err.message = `Error to connect to the database [${dbName}]: ${err.message}`;
      logger.error(err);
      process.exit();
    });
  Object.entries(models).forEach(([modelName, model]) => {
    db[modelName] = model;
  });
}

module.exports = db;
