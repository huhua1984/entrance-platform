const Sequelize = require('sequelize');
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('treasure_chests', {
    id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      primaryKey: true,
      comment: 'ID'
    },
    user_id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      comment: '用戶ID',
      references: {
        model: 'users',
        key: 'id'
      }
    },
    type_id: {
      type: DataTypes.STRING(255),
      allowNull: false,
      comment: '寶箱的唯一識別ID'
    },
    category: {
      type: DataTypes.STRING(255),
      allowNull: false,
      comment: '寶箱類別'
    },
    status: {
      type: DataTypes.TINYINT,
      allowNull: false,
      defaultValue: 0,
      comment: '領取狀態 0:可領取 1:已領取 2:禁止領取'
    }
  }, {
    sequelize,
    tableName: 'treasure_chests',
    timestamps: true,
    indexes: [
      {
        name: 'PRIMARY',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'id' }
        ]
      },
      {
        name: 'treasure_chests_user_id_type_id_unique',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'user_id' },
          { name: 'type_id' }
        ]
      }
    ]
  });
};
