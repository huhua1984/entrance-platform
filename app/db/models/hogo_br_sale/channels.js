module.exports = function (sequelize, DataTypes) {
  return sequelize.define('channels', {
    id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      comment: 'ID',
      unique: 'channels_id_unique'
    },
    parentId: {
      type: DataTypes.CHAR(36),
      allowNull: true,
      defaultValue: '00000000000000000000000000000000',
      comment: '上级代理',
      references: {
        model: 'channels',
        key: 'id'
      }
    },
    rootId: {
      type: DataTypes.CHAR(36),
      allowNull: true,
      comment: '根级代理'
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false,
      comment: '代理名称',
      unique: 'channels_name_unique'
    },
    authParameter: {
      type: DataTypes.JSON,
      defaultValue: null,
      allowNull: true,
      comment: '設定參數為json',
      collate: 'utf8mb4_unicode_ci'
    },
    promotionUrl: {
      type: DataTypes.STRING(255),
      allowNull: false,
      comment: '推廣網址',
      collate: 'utf8mb4_unicode_ci'
    },
    cooperationFunc: {
      type: DataTypes.INTEGER(10),
      defaultValue: 2,
      allowNull: true,
      comment: '合作模式 1=OTP,2=免驗證'
    },
    promotionUrlType: {
      type: DataTypes.INTEGER(10),
      allowNull: false,
      comment: '推廣網址類型 0: Auth 1: Landing',
      collate: 'utf8mb4_unicode_ci'
    },
    serial: {
      autoIncrement: true,
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true,
      comment: '代理商號'
    },
    symbol: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: 'BRL',
      comment: '貨幣單位'
    },
    language: {
      type: DataTypes.STRING(32),
      allowNull: true
    },
    status: {
      type: DataTypes.TINYINT,
      allowNull: false,
      defaultValue: 0,
      comment: '启用状态 0:停用 1:启用'
    },
    statusMaintain: {
      type: DataTypes.TINYINT,
      allowNull: false,
      defaultValue: 0,
      comment: '维护状态 0:运行 1:维护'
    },
    remark: {
      type: DataTypes.STRING(255),
      allowNull: true,
      defaultValue: '',
      comment: '备注'
    }
  }, {
    sequelize,
    tableName: 'channels',
    hasTrigger: true,
    timestamps: true,
    paranoid: true,
    underscored: true,
    indexes: [
      {
        name: 'PRIMARY',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'serial' }
        ]
      },
      {
        name: 'channels_id_unique',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'id' }
        ]
      },
      {
        name: 'channels_name_unique',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'name' }
        ]
      },
      {
        name: 'channels_parent_id_index',
        using: 'BTREE',
        fields: [
          { name: 'parent_id' }
        ]
      },
      {
        name: 'channels_root_id_index',
        using: 'BTREE',
        fields: [
          { name: 'root_id' }
        ]
      }
    ]
  });
};
