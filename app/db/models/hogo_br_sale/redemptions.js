const Sequelize = require('sequelize');
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('redemptions', {
    id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      primaryKey: true,
      comment: 'ID'
    },
    wallet_id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      comment: '转出钱包ID',
      references: {
        model: 'wallets',
        key: 'id'
      }
    },
    dst_wallet_id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      comment: '转入钱包ID',
      references: {
        model: 'wallets',
        key: 'id'
      }
    },
    deduct: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
      comment: '转出钱包扣除金额，单位分'
    },
    amount: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
      comment: '转账金额，单位分'
    },
    remark: {
      type: DataTypes.STRING(255),
      allowNull: true,
      defaultValue: '',
      comment: '备注'
    }
  }, {
    sequelize,
    tableName: 'redemptions',
    timestamps: true,
    indexes: [
      {
        name: 'PRIMARY',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'id' }
        ]
      },
      {
        name: 'redemptions_wallet_id_foreign',
        using: 'BTREE',
        fields: [
          { name: 'wallet_id' }
        ]
      },
      {
        name: 'redemptions_dst_wallet_id_foreign',
        using: 'BTREE',
        fields: [
          { name: 'dst_wallet_id' }
        ]
      }
    ]
  });
};
