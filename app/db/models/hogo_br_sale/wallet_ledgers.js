module.exports = function (sequelize, DataTypes) {
  return sequelize.define('wallet_ledgers', {
    id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      primaryKey: true,
      comment: 'ID'
    },
    walletId: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      comment: '錢包ID'
    },
    gatewayId: {
      type: DataTypes.CHAR(36),
      allowNull: true,
      comment: '通道ID'
    },
    orderId: {
      type: DataTypes.STRING(255),
      allowNull: true,
      comment: '订单编号'
    },
    gain: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
      comment: '收入，单位分'
    },
    loss: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
      comment: '支出，单位分'
    },
    balanceBefore: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
      comment: '交易前余额，单位分'
    },
    balanceRemain: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
      comment: '交易后余额，单位分'
    },
    type: {
      type: DataTypes.STRING(32),
      allowNull: true,
      comment: '帳变類型'
    },
    remark: {
      type: DataTypes.STRING(255),
      allowNull: true,
      defaultValue: '',
      comment: '备注'
    },
    partitionDate: {
      type: DataTypes.DATEONLY,
      allowNull: false,
      primaryKey: true,
      comment: '分區日期'
    }
  }, {
    sequelize,
    tableName: 'wallet_ledgers',
    hasTrigger: true,
    timestamps: true,
    underscored: true,
    paranoid: true,
    indexes: [
      {
        name: 'PRIMARY',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'id' },
          { name: 'partition_date' }
        ]
      },
      {
        name: 'wallet_ledgers_wallet_id_foreign',
        using: 'BTREE',
        fields: [
          { name: 'wallet_id' }
        ]
      },
      {
        name: 'wallet_ledgers_gateway_id_foreign',
        using: 'BTREE',
        fields: [
          { name: 'gateway_id' }
        ]
      },
      {
        name: 'wallet_ledgers_type_index',
        using: 'BTREE',
        fields: [
          { name: 'type' }
        ]
      }
    ]
  });
};
