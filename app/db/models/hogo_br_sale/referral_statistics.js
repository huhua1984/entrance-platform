const Sequelize = require('sequelize');
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('referral_statistics', {
    referral_id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      primaryKey: true,
      comment: '推荐关系id',
      references: {
        model: 'referrals',
        key: 'id'
      }
    },
    bet: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
      comment: '直屬推荐用戶累積投注金额'
    },
    bets: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
      comment: '直屬推荐用戶累積投注次数'
    },
    win: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
      comment: '直屬推荐用戶累積獲利金额'
    },
    topup: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
      comment: '直屬推荐用戶累積充值金额'
    },
    topups: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
      comment: '直屬推荐用戶累積充值次数'
    },
    topup_payout: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
      comment: '直屬推荐用戶累積充值实付金额'
    },
    topup_fee: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
      comment: '直屬推荐用戶累積充值手续费'
    },
    withdrawal: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
      comment: '直屬推荐用戶累積提现金额'
    },
    withdrawals: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
      comment: '直屬推荐用戶累積提现次数'
    },
    withdrawal_payout: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
      comment: '直屬推荐用戶累積提现实付金额'
    },
    withdrawal_fee: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
      comment: '直屬推荐用戶累積提现手续费'
    },
    referrals: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
      comment: '直屬推荐人数'
    },
    referrals_effective: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
      comment: '直屬有效推荐人数'
    },
    referrals_with_bets: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
      comment: '直屬推荐人数中有投注的人数'
    },
    referrals_with_topups: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
      comment: '直屬推荐人数中有充值的人数'
    },
    referrals_with_withdrawals: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
      comment: '直屬推荐人数中有提现的人数'
    }
  }, {
    sequelize,
    tableName: 'referral_statistics',
    timestamps: true,
    indexes: [
      {
        name: 'PRIMARY',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'referral_id' }
        ]
      }
    ]
  });
};
