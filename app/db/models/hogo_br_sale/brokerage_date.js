const Sequelize = require('sequelize');
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('brokerage_date', {
    id: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    user_id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      comment: '获得佣金玩家ID'
    },
    symbol: {
      type: DataTypes.STRING(255),
      allowNull: false,
      comment: '幣種'
    },
    record_date: {
      type: DataTypes.DATEONLY,
      allowNull: true,
      comment: '日期'
    },
    brokerage: {
      type: DataTypes.BIGINT,
      allowNull: true,
      comment: '當日佣金加總'
    },
    status: {
      type: DataTypes.TINYINT,
      allowNull: false,
      defaultValue: 0,
      comment: '0未处理，1为已结算\t'
    }
  }, {
    sequelize,
    tableName: 'brokerage_date',
    timestamps: true,
    indexes: [
      {
        name: 'index_userId',
        using: 'BTREE',
        fields: [
          { name: 'user_id' }
        ]
      },
      {
        name: 'index_userId_recordDate',
        using: 'BTREE',
        fields: [
          { name: 'user_id' },
          { name: 'record_date' }
        ]
      },
      {
        name: 'index_id_recordDate',
        using: 'BTREE',
        fields: [
          { name: 'id' },
          { name: 'record_date' }
        ]
      }
    ]
  });
};
