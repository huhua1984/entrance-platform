const Sequelize = require('sequelize');
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('label_users', {
    label_id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      comment: '標籤ID',
      references: {
        model: 'labels',
        key: 'id'
      }
    },
    user_id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      comment: '用户ID',
      references: {
        model: 'users',
        key: 'id'
      }
    }
  }, {
    sequelize,
    tableName: 'label_users',
    timestamps: true,
    indexes: [
      {
        name: 'label_id_user_id_unique',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'label_id' },
          { name: 'user_id' }
        ]
      },
      {
        name: 'label_users_user_id_foreign',
        using: 'BTREE',
        fields: [
          { name: 'user_id' }
        ]
      }
    ]
  });
};
