module.exports = function (sequelize, DataTypes) {
  return sequelize.define('admin_users', {
    id: {
      autoIncrement: true,
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    username: {
      type: DataTypes.STRING(120),
      allowNull: false,
      unique: 'admin_users_username_unique'
    },
    password: {
      type: DataTypes.STRING(80),
      allowNull: false
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    avatar: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    remember_token: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    channel_id: {
      type: DataTypes.CHAR(36),
      allowNull: true,
      comment: '代理',
      references: {
        model: 'channels',
        key: 'id'
      }
    }
  }, {
    sequelize,
    tableName: 'admin_users',
    timestamps: true,
    underscored: true,
    indexes: [
      {
        name: 'PRIMARY',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'id' }
        ]
      },
      {
        name: 'admin_users_username_unique',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'username' }
        ]
      },
      {
        name: 'admin_users_channel_id_index',
        using: 'BTREE',
        fields: [
          { name: 'channel_id' }
        ]
      }
    ]
  });
};
