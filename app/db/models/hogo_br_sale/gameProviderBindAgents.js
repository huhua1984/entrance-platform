const Sequelize = require('sequelize');
module.exports = function (sequelize, DataTypes) {
  const gameProviderBindAgents = sequelize.define('gameProviderBindAgents', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER
    },
    agentId: {
      allowNull: false,
      type: Sequelize.INTEGER
    },
    gameProviderId: {
      allowNull: false,
      type: Sequelize.INTEGER
    },
    setInfo: {
      type: Sequelize.STRING(25)
    },
    status: {
      allowNull: false,
      type: Sequelize.SMALLINT(1),
      defaultValue: 1,
      comment: '1: 開啟, 0: 關閉, -1: 刪除'
    },
    updateTime: {
      type: Sequelize.DATE,
      defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
      onUpdate: 'CURRENT_TIMESTAMP'
    },
    createTime: {
      allowNull: false,
      type: Sequelize.DATE,
      defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    sequelize,
    tableName: 'gameProviderBindAgents',
    timestamps: false,
    indexes: [
      {
        name: 'PRIMARY',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'id' }
        ]
      },
      {
        name: 'agentId',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'agentId' }
        ]
      },
      {
        name: 'gameProviderId',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'gameProviderId' }
        ]
      }
    ]
  });
  gameProviderBindAgents.associate = function (models) {
    gameProviderBindAgents.belongsTo(models.agents, { as: 'agent' });
    gameProviderBindAgents.belongsTo(models.gameProviders, { as: 'gameProvider' });
  };
  return gameProviderBindAgents;
};
