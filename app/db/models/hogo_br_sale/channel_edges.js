const Sequelize = require('sequelize');
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('channel_edges', {
    channel_id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      comment: 'ID',
      references: {
        model: 'channels',
        key: 'id'
      }
    },
    depth: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      defaultValue: 0,
      comment: '边的数量或关联记录的数量'
    },
    count: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      defaultValue: 0,
      comment: '子代节点数'
    }
  }, {
    sequelize,
    tableName: 'channel_edges',
    timestamps: true,
    indexes: [
      {
        name: 'channel_edges_edge_id_unique',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'channel_id' }
        ]
      }
    ]
  });
};
