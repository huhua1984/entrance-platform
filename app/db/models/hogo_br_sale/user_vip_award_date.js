const Sequelize = require('sequelize');
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('user_vip_award_date', {
    id: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    user_id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      comment: '玩家ID'
    },
    symbol: {
      type: DataTypes.STRING(255),
      allowNull: false,
      comment: '幣種'
    },
    acc_level: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: 0,
      comment: 'vip'
    },
    vip_award_rate: {
      type: DataTypes.DECIMAL(3, 2),
      allowNull: true,
      defaultValue: 0.00,
      comment: 'vip等级返佣金'
    },
    total_consume: {
      type: DataTypes.BIGINT,
      allowNull: true,
      defaultValue: 0,
      comment: '下注'
    },
    vip_award: {
      type: DataTypes.BIGINT,
      allowNull: true,
      defaultValue: 0,
      comment: '获取佣金'
    },
    record_date: {
      type: DataTypes.DATEONLY,
      allowNull: false,
      comment: '统计日期'
    },
    status: {
      type: DataTypes.TINYINT,
      allowNull: false,
      defaultValue: 0,
      comment: '0未处理，1为已结算'
    }
  }, {
    sequelize,
    tableName: 'user_vip_award_date',
    timestamps: true,
    indexes: [
      {
        name: 'index_userId',
        using: 'BTREE',
        fields: [
          { name: 'user_id' }
        ]
      },
      {
        name: 'index_userId_recordDate',
        using: 'BTREE',
        fields: [
          { name: 'user_id' },
          { name: 'record_date' }
        ]
      },
      {
        name: 'index_id_recordDate',
        using: 'BTREE',
        fields: [
          { name: 'id' },
          { name: 'record_date' }
        ]
      }
    ]
  });
};
