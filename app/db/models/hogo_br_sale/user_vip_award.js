const Sequelize = require('sequelize');
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('user_vip_award', {
    id: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    user_id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      comment: '玩家ID'
    },
    game_name: {
      type: DataTypes.STRING(255),
      allowNull: true,
      comment: '游戏名称'
    },
    symbol: {
      type: DataTypes.STRING(255),
      allowNull: true,
      comment: '货币单位'
    },
    acc_level: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: 0,
      comment: 'vip'
    },
    vip_award_rate: {
      type: DataTypes.DECIMAL(3, 2),
      allowNull: true,
      defaultValue: 0.00,
      comment: 'vip等级返佣金'
    },
    vip_award: {
      type: DataTypes.BIGINT,
      allowNull: true,
      defaultValue: 0,
      comment: '获取佣金'
    },
    game_code: {
      type: DataTypes.STRING(255),
      allowNull: true,
      comment: '游戏编号'
    },
    oid: {
      type: DataTypes.STRING(255),
      allowNull: false,
      comment: '订单号'
    },
    batch: {
      type: DataTypes.STRING(255),
      allowNull: false,
      comment: '批次'
    },
    consume: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
      comment: '下注金额'
    },
    status: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
      comment: '0未处理，1结算中，2为已结算'
    }
  }, {
    sequelize,
    tableName: 'user_vip_award',
    timestamps: true,
    indexes: [
      {
        name: 'index_id_created',
        using: 'BTREE',
        fields: [
          { name: 'id' },
          { name: 'created_at' }
        ]
      }
    ]
  });
};
