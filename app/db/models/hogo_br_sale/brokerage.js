const Sequelize = require('sequelize');
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('brokerage', {
    id: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    user_id: {
      type: DataTypes.CHAR(36),
      allowNull: true,
      comment: '获得佣金用户ID'
    },
    game_name: {
      type: DataTypes.STRING(255),
      allowNull: true,
      comment: '游戏名称'
    },
    child_id: {
      type: DataTypes.CHAR(36),
      allowNull: true,
      comment: '产生佣金下级用户ID'
    },
    symbol: {
      type: DataTypes.STRING(255),
      allowNull: true,
      comment: '货币单位'
    },
    game_code: {
      type: DataTypes.STRING(255),
      allowNull: true,
      comment: '游戏编号'
    },
    batch: {
      type: DataTypes.STRING(255),
      allowNull: true,
      comment: '批次'
    },
    oid: {
      type: DataTypes.STRING(100),
      allowNull: true,
      comment: '订单号'
    },
    bmoney: {
      type: DataTypes.BIGINT,
      allowNull: true,
      comment: '产生的佣金值'
    },
    lv: {
      type: DataTypes.INTEGER,
      allowNull: true,
      comment: '层级'
    },
    consume: {
      type: DataTypes.BIGINT,
      allowNull: true,
      defaultValue: 0
    },
    reward: {
      type: DataTypes.BIGINT,
      allowNull: true,
      comment: '下注佣金'
    },
    status: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
      comment: '0未处理，1结算中，2为已结算'
    }
  }, {
    sequelize,
    tableName: 'brokerage',
    timestamps: true,
    indexes: [
      {
        name: 'index_id_created',
        using: 'BTREE',
        fields: [
          { name: 'id' },
          { name: 'created_at' }
        ]
      }
    ]
  });
};
