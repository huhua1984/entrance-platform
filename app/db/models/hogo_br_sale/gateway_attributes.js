const Sequelize = require('sequelize');
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('gateway_attributes', {
    id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      primaryKey: true,
      comment: 'ID'
    },
    gateway_id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      comment: '通道ID',
      references: {
        model: 'gateways',
        key: 'id'
      }
    },
    integration_id: {
      type: DataTypes.CHAR(36),
      allowNull: true,
      comment: '通道集成ID',
      references: {
        model: 'gateway_integrations',
        key: 'id'
      }
    },
    key: {
      type: DataTypes.STRING(255),
      allowNull: false,
      comment: '鍵'
    },
    value: {
      type: DataTypes.STRING(5000),
      allowNull: false,
      comment: '值'
    }
  }, {
    sequelize,
    tableName: 'gateway_attributes',
    timestamps: true,
    indexes: [
      {
        name: 'PRIMARY',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'id' }
        ]
      },
      {
        name: 'gateway_attributes_gateway_id_integration_id_key_unique',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'gateway_id' },
          { name: 'integration_id' },
          { name: 'key' }
        ]
      },
      {
        name: 'gateway_attributes_integration_id_foreign',
        using: 'BTREE',
        fields: [
          { name: 'integration_id' }
        ]
      }
    ]
  });
};
