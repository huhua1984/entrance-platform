module.exports = function (sequelize, DataTypes) {
  return sequelize.define('vips', {
    id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      primaryKey: true,
      comment: 'ID'
    },
    level: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      defaultValue: 0,
      comment: '等级'
    },
    channel_id: {
      type: DataTypes.CHAR(36),
      allowNull: true,
      comment: '代理ID',
      references: {
        model: 'channels',
        key: 'id'
      }
    },
    topup_total_threshold: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
      comment: '充值总额門檻'
    },
    bet_total_threshold: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
      comment: '投注总额門檻'
    },
    previous_day_commission_rate: {
      type: DataTypes.DECIMAL(3, 2),
      allowNull: false,
      defaultValue: 0.00,
      comment: '前一日返佣比例'
    },
    promote_reward_amount: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
      comment: '晉升奖励金额'
    },
    daily_withdraw_count: {
      type: DataTypes.TINYINT,
      allowNull: false,
      defaultValue: 0,
      comment: '每日提现次数'
    },
    withdraw_fee_rate: {
      type: DataTypes.DECIMAL(3, 2),
      allowNull: false,
      defaultValue: 0.00,
      comment: '提现手续费率'
    },
    singular_withdraw_limit: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
      comment: '单次最大提现额度'
    },
    monthly_free_withdraw_count: {
      type: DataTypes.TINYINT,
      allowNull: false,
      defaultValue: 0,
      comment: '每月免费提现次数'
    }
  }, {
    sequelize,
    tableName: 'vips',
    timestamps: true,
    indexes: [
      {
        name: 'PRIMARY',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'id' }
        ]
      },
      {
        name: 'channel_id_level_unique',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'channel_id' },
          { name: 'level' }
        ]
      }
    ]
  });
};
