const Sequelize = require('sequelize');
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('wallets', {
    id: {
      type: DataTypes.UUID,
      defaultValue: Sequelize.UUIDV4,
      allowNull: false,
      comment: 'ID',
      unique: 'wallets_id_unique'
    },
    serial: {
      autoIncrement: true,
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    userId: {
      type: DataTypes.STRING(255),
      allowNull: true,
      comment: 'UID',
      references: {
        model: 'users',
        key: 'id'
      }
    },
    symbol: {
      type: DataTypes.STRING(255),
      allowNull: false,
      comment: '貨幣單位'
    },
    position: {
      type: DataTypes.STRING(255),
      allowNull: false,
      comment: '貨幣倉位'
    },
    balance: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
      comment: '貨幣餘額'
    },
    remark: {
      type: DataTypes.STRING(255),
      allowNull: true,
      defaultValue: '',
      comment: '备注'
    }
  }, {
    sequelize,
    tableName: 'wallets',
    hasTrigger: true,
    timestamps: true,
    underscored: true,
    paranoid: true,
    indexes: [
      {
        name: 'PRIMARY',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'serial' }
        ]
      },
      {
        name: 'wallets_id_unique',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'id' }
        ]
      },
      {
        name: 'wallets_user_id_symbol_position_unique',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'user_id' },
          { name: 'symbol' },
          { name: 'position' }
        ]
      }
    ]
  });
};
