module.exports = function (sequelize, DataTypes) {
  return sequelize.define('channel_attributes', {
    id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      primaryKey: true,
      comment: 'ID'
    },
    channel_id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      comment: '代理ID',
      references: {
        model: 'channels',
        key: 'id'
      }
    },
    key: {
      type: DataTypes.STRING(255),
      allowNull: false,
      comment: '鍵'
    },
    value: {
      type: DataTypes.STRING(5000),
      allowNull: true,
      comment: '值'
    },
    status: {
      type: DataTypes.TINYINT,
      allowNull: false,
      defaultValue: 1,
      comment: '状态 0:停用 1:启用'
    }
  }, {
    sequelize,
    tableName: 'channel_attributes',
    timestamps: true,
    underscored: true,
    indexes: [
      {
        name: 'PRIMARY',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'id' }
        ]
      },
      {
        name: 'channel_id_key_unique',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'channel_id' },
          { name: 'key' }
        ]
      }
    ]
  });
};
