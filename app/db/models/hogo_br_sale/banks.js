const Sequelize = require('sequelize');
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('banks', {
    id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      primaryKey: true,
      comment: 'ID'
    },
    title: {
      type: DataTypes.STRING(255),
      allowNull: false,
      comment: '機構單位'
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false,
      comment: '银行名称'
    },
    code: {
      type: DataTypes.STRING(255),
      allowNull: false,
      comment: '银行代码'
    },
    slug: {
      type: DataTypes.STRING(255),
      allowNull: false,
      comment: '银行标识',
      unique: 'banks_slug_unique'
    },
    symbol: {
      type: DataTypes.STRING(255),
      allowNull: false,
      comment: '货币符号'
    },
    type: {
      type: DataTypes.TINYINT,
      allowNull: false,
      comment: '银行类型 0: 银行卡 1: 加密货币'
    },
    remark: {
      type: DataTypes.STRING(255),
      allowNull: true,
      defaultValue: '',
      comment: '备注'
    }
  }, {
    sequelize,
    tableName: 'banks',
    timestamps: true,
    indexes: [
      {
        name: 'PRIMARY',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'id' }
        ]
      },
      {
        name: 'banks_slug_unique',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'slug' }
        ]
      }
    ]
  });
};
