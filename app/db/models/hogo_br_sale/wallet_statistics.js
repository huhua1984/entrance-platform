module.exports = function (sequelize, DataTypes) {
  return sequelize.define('wallet_statistics', {
    wallet_id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      comment: 'ID',
      references: {
        model: 'wallets',
        key: 'id'
      }
    },
    withdrawals: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      defaultValue: 0,
      comment: '提现次数'
    },
    withdrawal: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
      comment: '提现金额'
    },
    withdrawal_fee: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
      comment: '提现手续费'
    },
    withdrawal_payout: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
      comment: '提现实付金额'
    },
    withdraw_at: {
      type: DataTypes.DATE,
      allowNull: true,
      comment: '最後提现時間'
    },
    topups: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      defaultValue: 0,
      comment: '充值次数'
    },
    topup_fee: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
      comment: '充值手续费'
    },
    topup_payout: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
      comment: '充值实付金额'
    },
    topup: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
      comment: '充值金额'
    },
    topup_at: {
      type: DataTypes.DATE,
      allowNull: true,
      comment: '最後充值時間'
    },
    bets: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      defaultValue: 0,
      comment: '投注次数'
    },
    bet: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
      comment: '投注金额'
    },
    bet_at: {
      type: DataTypes.DATE,
      allowNull: true,
      comment: '最後下注時間'
    },
    win: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
      comment: '獲利金额'
    },
    inflow: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
      comment: '流入金额'
    },
    outflow: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
      comment: '流出金额'
    }
  }, {
    sequelize,
    tableName: 'wallet_statistics',
    timestamps: true,
    indexes: [
      {
        name: 'wallet_statistics_wallet_id_unique',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'wallet_id' }
        ]
      }
    ]
  });
};
