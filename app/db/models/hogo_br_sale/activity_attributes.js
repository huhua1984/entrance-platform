module.exports = function (sequelize, DataTypes) {
  return sequelize.define('activity_attributes', {
    id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      primaryKey: true,
      comment: 'ID'
    },
    activity_id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      comment: '活动ID',
      references: {
        model: 'activities',
        key: 'id'
      }
    },
    integration_id: {
      type: DataTypes.CHAR(36),
      allowNull: true,
      comment: '活动集成ID',
      references: {
        model: 'activity_integrations',
        key: 'id'
      }
    },
    key: {
      type: DataTypes.STRING(255),
      allowNull: false,
      comment: '键'
    },
    value: {
      type: DataTypes.STRING(255),
      allowNull: false,
      comment: '值'
    },
    tips: {
      type: DataTypes.STRING(255),
      allowNull: false,
      comment: '参数提示'
    },
    rank: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
      comment: '排序'
    },
    condition: {
      type: DataTypes.STRING(255),
      allowNull: true,
      comment: '条件|分开'
    },
    rule_no: {
      type: DataTypes.STRING(255),
      allowNull: true,
      comment: '规则编码'
    },
    remark: {
      type: DataTypes.STRING(255),
      allowNull: true,
      comment: '备注'
    }
  }, {
    sequelize,
    tableName: 'activity_attributes',
    timestamps: true,
    indexes: [
      {
        name: 'PRIMARY',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'id' }
        ]
      },
      {
        name: 'activity_attributes_activity_id_integration_id_key_unique',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'activity_id' },
          { name: 'integration_id' },
          { name: 'key' }
        ]
      },
      {
        name: 'activity_attributes_integration_id_foreign',
        using: 'BTREE',
        fields: [
          { name: 'integration_id' }
        ]
      }
    ]
  });
};
