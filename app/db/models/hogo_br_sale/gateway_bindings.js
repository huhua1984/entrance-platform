const Sequelize = require('sequelize');
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('gateway_bindings', {
    id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      primaryKey: true,
      comment: 'ID'
    },
    gateway_id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      comment: '通道ID',
      references: {
        model: 'gateways',
        key: 'id'
      }
    },
    key: {
      type: DataTypes.STRING(255),
      allowNull: false,
      comment: '鍵'
    },
    binding: {
      type: DataTypes.STRING(255),
      allowNull: false,
      comment: 'PHP Class'
    },
    remark: {
      type: DataTypes.STRING(255),
      allowNull: true,
      defaultValue: '',
      comment: '备注'
    }
  }, {
    sequelize,
    tableName: 'gateway_bindings',
    timestamps: true,
    indexes: [
      {
        name: 'PRIMARY',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'id' }
        ]
      },
      {
        name: 'gateway_id_key_unique',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'gateway_id' },
          { name: 'key' }
        ]
      }
    ]
  });
};
