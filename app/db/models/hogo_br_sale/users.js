const Sequelize = require('sequelize');
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('users', {
    id: {
      type: DataTypes.UUID,
      defaultValue: Sequelize.UUIDV4,
      allowNull: false,
      comment: 'ID',
      unique: 'users_id_unique'
    },
    serial: {
      autoIncrement: true,
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    email: {
      type: DataTypes.STRING(255)
    },
    phoneNum: {
      type: DataTypes.STRING(32),
      allowNull: false,
      collate: 'utf8mb4_unicode_ci'
    },
    username: {
      type: DataTypes.STRING(255),
      allowNull: false,
      unique: 'users_username_unique'
    },
    verifiedAt: {
      type: DataTypes.DATE,
      allowNull: true
    },
    password: {
      type: DataTypes.STRING(255),
      allowNull: false
    },
    avatar: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: 'https:\\ui-avatars.com/api/?name=John+Doe&color=7F9CF5&background=EBF4FF'
    },
    rememberToken: {
      type: DataTypes.STRING(100),
      allowNull: true
    },
    parentPath: {
      type: DataTypes.STRING(4000),
      allowNull: true
    },
    loginAt: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: Sequelize.Sequelize.literal('CURRENT_TIMESTAMP')
    },
    ip: {
      type: DataTypes.STRING(45),
      allowNull: true
    },
    status: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    channelId: {
      type: DataTypes.CHAR(36),
      allowNull: true,
      defaultValue: '00000000000000000000000000000000',
      comment: '代理',
      references: {
        model: 'channels',
        key: 'id'
      }
    },
    vipId: {
      type: DataTypes.CHAR(36),
      allowNull: true,
      comment: 'VIP ID',
      references: {
        model: 'vips',
        key: 'id'
      }
    }
  }, {
    sequelize,
    tableName: 'users',
    timestamps: true,
    underscored: true,
    indexes: [
      {
        name: 'PRIMARY',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'serial' }
        ]
      },
      {
        name: 'users_id_unique',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'id' }
        ]
      },
      {
        name: 'users_username_unique',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'username' }
        ]
      },
      {
        name: 'users_channel_index',
        using: 'BTREE',
        fields: [
          { name: 'channel_id' }
        ]
      },
      {
        name: 'users_vip_id_foreign',
        using: 'BTREE',
        fields: [
          { name: 'vip_id' }
        ]
      }
    ]
  });
};
