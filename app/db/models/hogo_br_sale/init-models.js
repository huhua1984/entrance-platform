/* eslint-disable camelcase */
const DataTypes = require('sequelize').DataTypes;
const _activities = require('./activities');
const _activity_attributes = require('./activity_attributes');
const _activity_integrations = require('./activity_integrations');
const _activity_user_variable = require('./activity_user_variable');
const _admin_menu = require('./admin_menu');
const _admin_operation_log = require('./admin_operation_log');
const _admin_permission_menu = require('./admin_permission_menu');
const _admin_permissions = require('./admin_permissions');
const _admin_role_menu = require('./admin_role_menu');
const _admin_role_permissions = require('./admin_role_permissions');
const _admin_role_users = require('./admin_role_users');
const _admin_roles = require('./admin_roles');
const _admin_users = require('./admin_users');
const _bank_account_attributes = require('./bank_account_attributes');
const _bank_accounts = require('./bank_accounts');
const _bank_attributes = require('./bank_attributes');
const _banks = require('./banks');
const _brokerage = require('./brokerage');
const _brokerage_date = require('./brokerage_date');
const _channel_attributes = require('./channel_attributes');
const _channel_attributes_keys = require('./channel_attributes_keys');
const _channel_edges = require('./channel_edges');
const _channels = require('./channels');
const _check_in_awards = require('./check_in_awards');
const _check_in_users = require('./check_in_users');
const _daily_report_all = require('./daily_report_all');
const _daily_report_user = require('./daily_report_user');
const _domains = require('./domains');
const _exchange_rates = require('./exchange_rates');
const _game_integrations = require('./game_integrations');
const _games = require('./games');
const _gateway_attributes = require('./gateway_attributes');
const _gateway_banks = require('./gateway_banks');
const _gateway_bindings = require('./gateway_bindings');
const _gateway_integrations = require('./gateway_integrations');
const _gateways = require('./gateways');
const _invite_reward_logs = require('./invite_reward_logs');
const _job_batches = require('./job_batches');
const _jobs = require('./jobs');
const _label_users = require('./label_users');
const _labels = require('./labels');
const _migrations = require('./migrations');
const _oauth_access_tokens = require('./oauth_access_tokens');
const _oauth_auth_codes = require('./oauth_auth_codes');
const _oauth_clients = require('./oauth_clients');
const _oauth_personal_access_clients = require('./oauth_personal_access_clients');
const _oauth_refresh_tokens = require('./oauth_refresh_tokens');
const _one_touch_users = require('./one_touch_users');
const _password_resets = require('./password_resets');
const _promotion = require('./promotion');
const _redemptions = require('./redemptions');
const _referral_edges = require('./referral_edges');
const _referral_statistics = require('./referral_statistics');
const _referrals = require('./referrals');
const _rely_bet = require('./rely_bet');
const _styles = require('./styles');
const _sys_cron_partition = require('./sys_cron_partition');
const _sys_links = require('./sys_links');
const _sys_prolog = require('./sys_prolog');
const _topups = require('./topups');
const _treasure_chest_rewards = require('./treasure_chest_rewards');
const _treasure_chests = require('./treasure_chests');
const _user_login_logs = require('./user_login_logs');
const _user_online_detail = require('./user_online_detail');
const _user_ranking_list = require('./user_ranking_list');
const _user_ranking_list_total = require('./user_ranking_list_total');
const _user_statistics = require('./user_statistics');
const _user_vip_award = require('./user_vip_award');
const _user_vip_award_date = require('./user_vip_award_date');
const _users = require('./users');
const _vips = require('./vips');
const _wallet_ledgers = require('./wallet_ledgers');
const _wallet_statistics = require('./wallet_statistics');
const _wallets = require('./wallets');
const _withdrawals = require('./withdrawals');

function initModels (sequelize) {
  const activities = _activities(sequelize, DataTypes);
  const activity_attributes = _activity_attributes(sequelize, DataTypes);
  const activity_integrations = _activity_integrations(sequelize, DataTypes);
  const activity_user_variable = _activity_user_variable(sequelize, DataTypes);
  const admin_menu = _admin_menu(sequelize, DataTypes);
  const admin_operation_log = _admin_operation_log(sequelize, DataTypes);
  const admin_permission_menu = _admin_permission_menu(sequelize, DataTypes);
  const admin_permissions = _admin_permissions(sequelize, DataTypes);
  const admin_role_menu = _admin_role_menu(sequelize, DataTypes);
  const admin_role_permissions = _admin_role_permissions(sequelize, DataTypes);
  const admin_role_users = _admin_role_users(sequelize, DataTypes);
  const admin_roles = _admin_roles(sequelize, DataTypes);
  const admin_users = _admin_users(sequelize, DataTypes);
  const bank_account_attributes = _bank_account_attributes(sequelize, DataTypes);
  const bank_accounts = _bank_accounts(sequelize, DataTypes);
  const bank_attributes = _bank_attributes(sequelize, DataTypes);
  const banks = _banks(sequelize, DataTypes);
  const brokerage = _brokerage(sequelize, DataTypes);
  const brokerage_date = _brokerage_date(sequelize, DataTypes);
  const channel_attributes = _channel_attributes(sequelize, DataTypes);
  const channel_attributes_keys = _channel_attributes_keys(sequelize, DataTypes);
  const channel_edges = _channel_edges(sequelize, DataTypes);
  const channels = _channels(sequelize, DataTypes);
  const check_in_awards = _check_in_awards(sequelize, DataTypes);
  const check_in_users = _check_in_users(sequelize, DataTypes);
  const daily_report_all = _daily_report_all(sequelize, DataTypes);
  const daily_report_user = _daily_report_user(sequelize, DataTypes);
  const domains = _domains(sequelize, DataTypes);
  const exchange_rates = _exchange_rates(sequelize, DataTypes);
  const game_integrations = _game_integrations(sequelize, DataTypes);
  const games = _games(sequelize, DataTypes);
  const gateway_attributes = _gateway_attributes(sequelize, DataTypes);
  const gateway_banks = _gateway_banks(sequelize, DataTypes);
  const gateway_bindings = _gateway_bindings(sequelize, DataTypes);
  const gateway_integrations = _gateway_integrations(sequelize, DataTypes);
  const gateways = _gateways(sequelize, DataTypes);
  const invite_reward_logs = _invite_reward_logs(sequelize, DataTypes);
  const job_batches = _job_batches(sequelize, DataTypes);
  const jobs = _jobs(sequelize, DataTypes);
  const label_users = _label_users(sequelize, DataTypes);
  const labels = _labels(sequelize, DataTypes);
  const migrations = _migrations(sequelize, DataTypes);
  const oauth_access_tokens = _oauth_access_tokens(sequelize, DataTypes);
  const oauth_auth_codes = _oauth_auth_codes(sequelize, DataTypes);
  const oauth_clients = _oauth_clients(sequelize, DataTypes);
  const oauth_personal_access_clients = _oauth_personal_access_clients(sequelize, DataTypes);
  const oauth_refresh_tokens = _oauth_refresh_tokens(sequelize, DataTypes);
  const one_touch_users = _one_touch_users(sequelize, DataTypes);
  const password_resets = _password_resets(sequelize, DataTypes);
  const promotion = _promotion(sequelize, DataTypes);
  const redemptions = _redemptions(sequelize, DataTypes);
  const referral_edges = _referral_edges(sequelize, DataTypes);
  const referral_statistics = _referral_statistics(sequelize, DataTypes);
  const referrals = _referrals(sequelize, DataTypes);
  const rely_bet = _rely_bet(sequelize, DataTypes);
  const styles = _styles(sequelize, DataTypes);
  const sys_cron_partition = _sys_cron_partition(sequelize, DataTypes);
  const sys_links = _sys_links(sequelize, DataTypes);
  const sys_prolog = _sys_prolog(sequelize, DataTypes);
  const topups = _topups(sequelize, DataTypes);
  const treasure_chest_rewards = _treasure_chest_rewards(sequelize, DataTypes);
  const treasure_chests = _treasure_chests(sequelize, DataTypes);
  const user_login_logs = _user_login_logs(sequelize, DataTypes);
  const user_online_detail = _user_online_detail(sequelize, DataTypes);
  const user_ranking_list = _user_ranking_list(sequelize, DataTypes);
  const user_ranking_list_total = _user_ranking_list_total(sequelize, DataTypes);
  const user_statistics = _user_statistics(sequelize, DataTypes);
  const user_vip_award = _user_vip_award(sequelize, DataTypes);
  const user_vip_award_date = _user_vip_award_date(sequelize, DataTypes);
  const users = _users(sequelize, DataTypes);
  const vips = _vips(sequelize, DataTypes);
  const wallet_ledgers = _wallet_ledgers(sequelize, DataTypes);
  const wallet_statistics = _wallet_statistics(sequelize, DataTypes);
  const wallets = _wallets(sequelize, DataTypes);
  const withdrawals = _withdrawals(sequelize, DataTypes);

  banks.belongsToMany(gateways, { as: 'gateway_id_gateways', through: gateway_banks, foreignKey: 'bank_id', otherKey: 'gateway_id' });
  gateways.belongsToMany(banks, { as: 'bank_id_banks', through: gateway_banks, foreignKey: 'gateway_id', otherKey: 'bank_id' });

  activity_attributes.belongsTo(activities, { as: 'activity', foreignKey: 'activity_id' });
  activities.hasMany(activity_attributes, { as: 'activity_attributes', foreignKey: 'activity_id' });
  activity_integrations.belongsTo(activities, { as: 'activity', foreignKey: 'activity_id' });
  activities.hasMany(activity_integrations, { as: 'activity_integrations', foreignKey: 'activity_id' });
  activity_user_variable.belongsTo(activities, { as: 'activity', foreignKey: 'activity_id' });
  activities.hasMany(activity_user_variable, { as: 'activity_user_variables', foreignKey: 'activity_id' });
  activity_attributes.belongsTo(activity_integrations, { as: 'integration', foreignKey: 'integration_id' });
  activity_integrations.hasMany(activity_attributes, { as: 'activity_attributes', foreignKey: 'integration_id' });
  bank_account_attributes.belongsTo(bank_accounts, { as: 'bank_account', foreignKey: 'bank_account_id' });
  bank_accounts.hasMany(bank_account_attributes, { as: 'bank_account_attributes', foreignKey: 'bank_account_id' });
  withdrawals.belongsTo(bank_accounts, { as: 'bank_account', foreignKey: 'bank_account_id' });
  bank_accounts.hasMany(withdrawals, { as: 'withdrawals', foreignKey: 'bank_account_id' });
  bank_accounts.belongsTo(banks, { as: 'bank', foreignKey: 'bank_id' });
  banks.hasMany(bank_accounts, { as: 'bank_accounts', foreignKey: 'bank_id' });
  bank_attributes.belongsTo(banks, { as: 'bank', foreignKey: 'bank_id' });
  banks.hasMany(bank_attributes, { as: 'bank_attributes', foreignKey: 'bank_id' });
  gateway_banks.belongsTo(banks, { as: 'bank', foreignKey: 'bank_id' });
  banks.hasMany(gateway_banks, { as: 'gateway_banks', foreignKey: 'bank_id' });
  activity_integrations.belongsTo(channels, { as: 'channel', foreignKey: 'channel_id' });
  channels.hasMany(activity_integrations, { as: 'activity_integrations', foreignKey: 'channel_id' });
  admin_users.belongsTo(channels, { as: 'channel', foreignKey: 'channel_id', targetKey: 'id' });
  channels.hasMany(admin_users, { as: 'admin_users', foreignKey: 'channel_id', sourceKey: 'id' });
  channel_attributes.belongsTo(channels, { as: 'channel', foreignKey: 'channel_id' });
  channels.hasMany(channel_attributes, { as: 'channel_attributes', foreignKey: 'channel_id' });
  channel_edges.belongsTo(channels, { as: 'channel', foreignKey: 'channel_id' });
  channels.hasMany(channel_edges, { as: 'channel_edges', foreignKey: 'channel_id' });
  channels.belongsTo(channels, { as: 'parent', foreignKey: 'parent_id' });
  channels.hasMany(channels, { as: 'channels', foreignKey: 'parent_id' });
  domains.belongsTo(channels, { as: 'channel', foreignKey: 'channel_id' });
  channels.hasMany(domains, { as: 'domains', foreignKey: 'channel_id' });
  game_integrations.belongsTo(channels, { as: 'channel', foreignKey: 'channel_id' });
  channels.hasMany(game_integrations, { as: 'game_integrations', foreignKey: 'channel_id' });
  gateway_integrations.belongsTo(channels, { as: 'channel', foreignKey: 'channel_id', targetKey: 'id' });
  channels.hasMany(gateway_integrations, { as: 'gateway_integrations', foreignKey: 'channel_id', sourceKey: 'id' });
  promotion.belongsTo(channels, { as: 'channel', foreignKey: 'channel_id' });
  channels.hasMany(promotion, { as: 'promotions', foreignKey: 'channel_id' });
  styles.belongsTo(channels, { as: 'channel', foreignKey: 'channel_id' });
  channels.hasOne(styles, { as: 'style', foreignKey: 'channel_id' });
  users.belongsTo(channels, { as: 'channel', foreignKey: 'channel_id', targetKey: 'id' });
  channels.hasMany(users, { as: 'users', foreignKey: 'channel_id', sourceKey: 'id' });
  vips.belongsTo(channels, { as: 'channel', foreignKey: 'channel_id' });
  channels.hasMany(vips, { as: 'vips', foreignKey: 'channel_id' });
  game_integrations.belongsTo(games, { as: 'game', foreignKey: 'game_id' });
  games.hasMany(game_integrations, { as: 'game_integrations', foreignKey: 'game_id' });
  gateway_attributes.belongsTo(gateway_integrations, { as: 'integration', foreignKey: 'integration_id' });
  gateway_integrations.hasMany(gateway_attributes, { as: 'gateway_attributes', foreignKey: 'integration_id' });
  game_integrations.belongsTo(gateways, { as: 'gateway', foreignKey: 'gateway_id' });
  gateways.hasMany(game_integrations, { as: 'game_integrations', foreignKey: 'gateway_id' });
  games.belongsTo(gateways, { as: 'gateway', foreignKey: 'gateway_id' });
  gateways.hasMany(games, { as: 'games', foreignKey: 'gateway_id' });
  gateway_attributes.belongsTo(gateways, { as: 'gateway', foreignKey: 'gateway_id' });
  gateways.hasMany(gateway_attributes, { as: 'gateway_attributes', foreignKey: 'gateway_id' });
  gateway_banks.belongsTo(gateways, { as: 'gateway', foreignKey: 'gateway_id' });
  gateways.hasMany(gateway_banks, { as: 'gateway_banks', foreignKey: 'gateway_id' });
  gateway_bindings.belongsTo(gateways, { as: 'gateway', foreignKey: 'gateway_id' });
  gateways.hasMany(gateway_bindings, { as: 'gateway_bindings', foreignKey: 'gateway_id' });
  gateway_integrations.belongsTo(gateways, { as: 'gateway', foreignKey: 'gateway_id' });
  gateways.hasMany(gateway_integrations, { as: 'gateway_integrations', foreignKey: 'gateway_id' });
  topups.belongsTo(gateways, { as: 'gateway', foreignKey: 'gateway_id' });
  gateways.hasMany(topups, { as: 'topups', foreignKey: 'gateway_id' });
  withdrawals.belongsTo(gateways, { as: 'gateway', foreignKey: 'gateway_id' });
  gateways.hasMany(withdrawals, { as: 'withdrawals', foreignKey: 'gateway_id' });
  label_users.belongsTo(labels, { as: 'label', foreignKey: 'label_id' });
  labels.hasMany(label_users, { as: 'label_users', foreignKey: 'label_id' });
  referral_edges.belongsTo(referrals, { as: 'referral', foreignKey: 'referral_id' });
  referrals.hasOne(referral_edges, { as: 'referral_edge', foreignKey: 'referral_id' });
  referral_statistics.belongsTo(referrals, { as: 'referral', foreignKey: 'referral_id' });
  referrals.hasOne(referral_statistics, { as: 'referral_statistic', foreignKey: 'referral_id' });
  referrals.belongsTo(referrals, { as: 'parent', foreignKey: 'parent_id' });
  referrals.hasMany(referrals, { as: 'referrals', foreignKey: 'parent_id' });
  treasure_chest_rewards.belongsTo(treasure_chests, { as: 'treasure_chest', foreignKey: 'treasure_chest_id' });
  treasure_chests.hasMany(treasure_chest_rewards, { as: 'treasure_chest_rewards', foreignKey: 'treasure_chest_id' });
  activity_user_variable.belongsTo(users, { as: 'user', foreignKey: 'user_id' });
  users.hasMany(activity_user_variable, { as: 'activity_user_variables', foreignKey: 'user_id' });
  bank_accounts.belongsTo(users, { as: 'user', foreignKey: 'user_id' });
  users.hasMany(bank_accounts, { as: 'bank_accounts', foreignKey: 'user_id' });
  label_users.belongsTo(users, { as: 'user', foreignKey: 'user_id' });
  users.hasMany(label_users, { as: 'label_users', foreignKey: 'user_id' });
  referrals.belongsTo(users, { as: 'id_user', foreignKey: 'id' });
  users.hasOne(referrals, { as: 'referral', foreignKey: 'id' });
  referrals.belongsTo(users, { as: 'root', foreignKey: 'root_id' });
  users.hasMany(referrals, { as: 'root_referrals', foreignKey: 'root_id' });
  treasure_chests.belongsTo(users, { as: 'user', foreignKey: 'user_id' });
  users.hasMany(treasure_chests, { as: 'treasure_chests', foreignKey: 'user_id' });
  wallets.belongsTo(users, { as: 'user', foreignKey: 'user_id' });
  wallet_ledgers.belongsTo(wallets, { as: 'wallet', foreignKey: 'wallet_id', targetKey: 'id' });
  users.hasMany(wallets, { as: 'wallets', foreignKey: 'user_id' });
  users.belongsTo(vips, { as: 'vip', foreignKey: 'vip_id' });
  vips.hasMany(users, { as: 'users', foreignKey: 'vip_id' });
  redemptions.belongsTo(wallets, { as: 'dst_wallet', foreignKey: 'dst_wallet_id' });
  wallets.hasMany(redemptions, { as: 'redemptions', foreignKey: 'dst_wallet_id' });
  wallets.hasMany(wallet_ledgers, { as: 'wallet_ledgers', foreignKey: 'wallet_id', sourceKey: 'id' });
  redemptions.belongsTo(wallets, { as: 'wallet', foreignKey: 'wallet_id' });
  wallets.hasMany(redemptions, { as: 'wallet_redemptions', foreignKey: 'wallet_id' });
  topups.belongsTo(wallets, { as: 'wallet', foreignKey: 'wallet_id' });
  wallets.hasMany(topups, { as: 'topups', foreignKey: 'wallet_id' });
  wallet_statistics.belongsTo(wallets, { as: 'wallet', foreignKey: 'wallet_id' });
  wallets.hasMany(wallet_statistics, { as: 'wallet_statistics', foreignKey: 'wallet_id' });
  withdrawals.belongsTo(wallets, { as: 'wallet', foreignKey: 'wallet_id' });
  wallets.hasMany(withdrawals, { as: 'withdrawals', foreignKey: 'wallet_id' });

  return {
    activities,
    activity_attributes,
    activity_integrations,
    activity_user_variable,
    admin_menu,
    admin_operation_log,
    admin_permission_menu,
    admin_permissions,
    admin_role_menu,
    admin_role_permissions,
    admin_role_users,
    admin_roles,
    admin_users,
    bank_account_attributes,
    bank_accounts,
    bank_attributes,
    banks,
    brokerage,
    brokerage_date,
    channel_attributes,
    channel_attributes_keys,
    channel_edges,
    channels,
    check_in_awards,
    check_in_users,
    daily_report_all,
    daily_report_user,
    domains,
    exchange_rates,
    game_integrations,
    games,
    gateway_attributes,
    gateway_banks,
    gateway_bindings,
    gateway_integrations,
    gateways,
    invite_reward_logs,
    job_batches,
    jobs,
    label_users,
    labels,
    migrations,
    oauth_access_tokens,
    oauth_auth_codes,
    oauth_clients,
    oauth_personal_access_clients,
    oauth_refresh_tokens,
    one_touch_users,
    password_resets,
    promotion,
    redemptions,
    referral_edges,
    referral_statistics,
    referrals,
    rely_bet,
    styles,
    sys_cron_partition,
    sys_links,
    sys_prolog,
    topups,
    treasure_chest_rewards,
    treasure_chests,
    user_login_logs,
    user_online_detail,
    user_ranking_list,
    user_ranking_list_total,
    user_statistics,
    user_vip_award,
    user_vip_award_date,
    users,
    vips,
    wallet_ledgers,
    wallet_statistics,
    wallets,
    withdrawals
  };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;
