const Sequelize = require('sequelize');
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('admin_permission_menu', {
    permission_id: {
      type: DataTypes.BIGINT,
      allowNull: false
    },
    menu_id: {
      type: DataTypes.BIGINT,
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'admin_permission_menu',
    timestamps: true,
    indexes: [
      {
        name: 'admin_permission_menu_permission_id_menu_id_unique',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'permission_id' },
          { name: 'menu_id' }
        ]
      }
    ]
  });
};
