module.exports = function (sequelize, DataTypes) {
  return sequelize.define('gateways', {
    id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      primaryKey: true,
      comment: 'ID'
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false,
      comment: '通道名称'
    },
    nick_name: {
      type: DataTypes.STRING(255),
      allowNull: true,
      comment: '通道简称'
    },
    slug: {
      type: DataTypes.STRING(255),
      allowNull: false,
      comment: '通道标识符',
      unique: 'gateway_slug_unique'
    },
    type: {
      type: DataTypes.TINYINT,
      allowNull: false,
      comment: '通道类型 0:系统通道 1:遊戲通道 2:充值通道 3:提现通道'
    },
    status: {
      type: DataTypes.TINYINT,
      allowNull: false,
      defaultValue: 0,
      comment: '状态 0:停用 1:启用'
    },
    remark: {
      type: DataTypes.STRING(255),
      allowNull: true,
      defaultValue: '',
      comment: '备注'
    }
  }, {
    sequelize,
    tableName: 'gateways',
    timestamps: true,
    underscored: true,
    paranoid: true,
    indexes: [
      {
        name: 'PRIMARY',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'id' }
        ]
      },
      {
        name: 'gateway_slug_unique',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'slug' }
        ]
      }
    ]
  });
};
