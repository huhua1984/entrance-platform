const Sequelize = require('sequelize');
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('user_statistics', {
    user_id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      primaryKey: true,
      comment: '玩家ID'
    },
    betting_requirement: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0
    },
    total_bet_last_withdrawal: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize,
    tableName: 'user_statistics',
    timestamps: true,
    indexes: [
      {
        name: 'PRIMARY',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'user_id' }
        ]
      }
    ]
  });
};
