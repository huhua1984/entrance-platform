const Sequelize = require('sequelize');
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('sys_links', {
    id: {
      autoIncrement: true,
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    links_typs: {
      type: DataTypes.CHAR(80),
      allowNull: false,
      comment: '链接类型'
    },
    url: {
      type: DataTypes.TEXT,
      allowNull: false,
      comment: '链接内容'
    },
    orderindex: {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: '序号'
    }
  }, {
    sequelize,
    tableName: 'sys_links',
    timestamps: true,
    indexes: [
      {
        name: 'PRIMARY',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'id' }
        ]
      }
    ]
  });
};
