const Sequelize = require('sequelize');
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('check_in_awards', {
    id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      primaryKey: true,
      comment: 'ID'
    },
    combo: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
      comment: '连续签到天数',
      unique: 'check_in_awards_combo_unique'
    },
    award: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
      comment: '奖励'
    }
  }, {
    sequelize,
    tableName: 'check_in_awards',
    timestamps: true,
    indexes: [
      {
        name: 'PRIMARY',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'id' }
        ]
      },
      {
        name: 'check_in_awards_combo_unique',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'combo' }
        ]
      }
    ]
  });
};
