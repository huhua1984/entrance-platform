const Sequelize = require('sequelize');
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('admin_role_users', {
    role_id: {
      type: DataTypes.BIGINT,
      allowNull: false
    },
    user_id: {
      type: DataTypes.BIGINT,
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'admin_role_users',
    timestamps: true,
    indexes: [
      {
        name: 'admin_role_users_role_id_user_id_unique',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'role_id' },
          { name: 'user_id' }
        ]
      }
    ]
  });
};
