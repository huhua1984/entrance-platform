module.exports = function (sequelize, DataTypes) {
  return sequelize.define('activity_integrations', {
    id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      primaryKey: true,
      comment: 'ID'
    },
    activity_id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      comment: '活动ID',
      references: {
        model: 'activities',
        key: 'id'
      }
    },
    channel_id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      comment: '代理ID',
      references: {
        model: 'channels',
        key: 'id'
      }
    },
    status: {
      type: DataTypes.TINYINT,
      allowNull: false,
      defaultValue: 1,
      comment: '状态 0:停用 1:启用'
    }
  }, {
    sequelize,
    tableName: 'activity_integrations',
    timestamps: true,
    indexes: [
      {
        name: 'PRIMARY',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'id' }
        ]
      },
      {
        name: 'activity_channel_unique',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'activity_id' },
          { name: 'channel_id' }
        ]
      },
      {
        name: 'activity_integrations_channel_id_foreign',
        using: 'BTREE',
        fields: [
          { name: 'channel_id' }
        ]
      }
    ]
  });
};
