module.exports = function (sequelize, DataTypes) {
  return sequelize.define('games', {
    id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      primaryKey: true,
      comment: 'ID'
    },
    gatewayId: {
      type: DataTypes.CHAR(36),
      allowNull: true,
      comment: '通道ID',
      references: {
        model: 'gateways',
        key: 'id'
      }
    },
    gameName: {
      type: DataTypes.STRING(255),
      allowNull: false,
      comment: '游戏名称'
    },
    gameNickName: {
      type: DataTypes.STRING(255),
      allowNull: true,
      comment: '游戏简称'
    },
    gameCode: {
      type: DataTypes.STRING(255),
      allowNull: false,
      comment: '游戏编号'
    },
    status: {
      type: DataTypes.TINYINT,
      allowNull: false,
      defaultValue: 1,
      comment: '状态 0:停用 1:启用'
    },
    maintainStatus: {
      type: DataTypes.TINYINT,
      allowNull: false,
      defaultValue: 0,
      comment: '状态 0:非維護 1:維護中'
    },
    gameType: {
      type: DataTypes.TINYINT,
      allowNull: false,
      defaultValue: 0,
      comment: '类型 0:无 1:Slots 2:Live 3:Table 4:Fish'
    },
    sortId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
      comment: '排序'
    },
    isHot: {
      type: DataTypes.TINYINT,
      allowNull: false,
      defaultValue: 0,
      comment: '热门 0:无 1:热 2:新'
    },
    hotOrder: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
      comment: '热门游戏排序'
    },
    flag: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
      comment: '标签分类(大厅、直播) 0:无 1:Lobby'
    },
    group: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
      comment: '群组分类(最佳、特色) 0:无 1:IN-HOUSEGAMES 2:BEST FROM THE SLOTS 3:FEATURE BUY-IN SLOTS 4:RECOMMENDED LIVEGAME'
    },
    remark: {
      type: DataTypes.STRING(255),
      allowNull: true,
      defaultValue: '',
      comment: '备注'
    },
    gameImg: {
      type: DataTypes.STRING(255),
      allowNull: true,
      defaultValue: '',
      comment: '路径'
    },
    lang: {
      type: DataTypes.STRING(255),
      allowNull: true,
      defaultValue: '',
      comment: '语系'
    }
  }, {
    sequelize,
    tableName: 'games',
    timestamps: true,
    underscored: true,
    paranoid: true,
    indexes: [
      {
        name: 'PRIMARY',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'id' }
        ]
      },
      {
        name: 'games_gateway_id_foreign',
        using: 'BTREE',
        fields: [
          { name: 'gateway_id' }
        ]
      }
    ]
  });
};
