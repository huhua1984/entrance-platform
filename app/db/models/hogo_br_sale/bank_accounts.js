const Sequelize = require('sequelize');
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('bank_accounts', {
    id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      primaryKey: true,
      comment: 'ID'
    },
    bank_id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      comment: '银行ID',
      references: {
        model: 'banks',
        key: 'id'
      }
    },
    user_id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      comment: '用户ID',
      references: {
        model: 'users',
        key: 'id'
      }
    },
    account: {
      type: DataTypes.STRING(255),
      allowNull: false,
      comment: '银行账号'
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false,
      comment: '银行账户名'
    },
    paid_at: {
      type: DataTypes.DATE,
      allowNull: true,
      comment: '支付时间'
    },
    remark: {
      type: DataTypes.STRING(255),
      allowNull: true,
      defaultValue: '',
      comment: '备注'
    }
  }, {
    sequelize,
    tableName: 'bank_accounts',
    timestamps: true,
    indexes: [
      {
        name: 'PRIMARY',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'id' }
        ]
      },
      {
        name: 'bank_user_id',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'bank_id' },
          { name: 'user_id' }
        ]
      },
      {
        name: 'bank_accounts_user_id_foreign',
        using: 'BTREE',
        fields: [
          { name: 'user_id' }
        ]
      }
    ]
  });
};
