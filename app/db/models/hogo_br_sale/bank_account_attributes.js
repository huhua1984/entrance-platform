const Sequelize = require('sequelize');
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('bank_account_attributes', {
    id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      primaryKey: true,
      comment: 'ID'
    },
    bank_account_id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      comment: '銀行帳戶ID',
      references: {
        model: 'bank_accounts',
        key: 'id'
      }
    },
    key: {
      type: DataTypes.STRING(255),
      allowNull: false,
      comment: '鍵'
    },
    value: {
      type: DataTypes.STRING(255),
      allowNull: false,
      comment: '值'
    }
  }, {
    sequelize,
    tableName: 'bank_account_attributes',
    timestamps: true,
    indexes: [
      {
        name: 'PRIMARY',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'id' }
        ]
      },
      {
        name: 'bank_account_id_key_unique',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'bank_account_id' },
          { name: 'key' }
        ]
      }
    ]
  });
};
