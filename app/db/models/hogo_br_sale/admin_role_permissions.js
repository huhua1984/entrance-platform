const Sequelize = require('sequelize');
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('admin_role_permissions', {
    role_id: {
      type: DataTypes.BIGINT,
      allowNull: false
    },
    permission_id: {
      type: DataTypes.BIGINT,
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'admin_role_permissions',
    timestamps: true,
    indexes: [
      {
        name: 'admin_role_permissions_role_id_permission_id_unique',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'role_id' },
          { name: 'permission_id' }
        ]
      }
    ]
  });
};
