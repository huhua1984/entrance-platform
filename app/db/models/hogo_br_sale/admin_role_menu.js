const Sequelize = require('sequelize');
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('admin_role_menu', {
    role_id: {
      type: DataTypes.BIGINT,
      allowNull: false
    },
    menu_id: {
      type: DataTypes.BIGINT,
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'admin_role_menu',
    timestamps: true,
    indexes: [
      {
        name: 'admin_role_menu_role_id_menu_id_unique',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'role_id' },
          { name: 'menu_id' }
        ]
      }
    ]
  });
};
