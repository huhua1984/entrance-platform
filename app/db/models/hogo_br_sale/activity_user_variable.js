module.exports = function (sequelize, DataTypes) {
  return sequelize.define('activity_user_variable', {
    id: {
      autoIncrement: true,
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    user_id: {
      type: DataTypes.STRING(255),
      allowNull: false,
      comment: 'UID',
      references: {
        model: 'users',
        key: 'id'
      }
    },
    activity_id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      comment: '活动ID',
      references: {
        model: 'activities',
        key: 'id'
      }
    },
    key: {
      type: DataTypes.STRING(255),
      allowNull: false,
      comment: '键'
    },
    value: {
      type: DataTypes.STRING(255),
      allowNull: false,
      comment: '值'
    }
  }, {
    sequelize,
    tableName: 'activity_user_variable',
    timestamps: true,
    indexes: [
      {
        name: 'PRIMARY',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'id' }
        ]
      },
      {
        name: 'activity_user_variable_user_id_activity_id_key_unique',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'user_id' },
          { name: 'activity_id' },
          { name: 'key' }
        ]
      },
      {
        name: 'activity_user_variable_activity_id_foreign',
        using: 'BTREE',
        fields: [
          { name: 'activity_id' }
        ]
      }
    ]
  });
};
