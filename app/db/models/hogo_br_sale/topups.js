const Sequelize = require('sequelize');
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('topups', {
    id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      primaryKey: true,
      comment: '订单编号'
    },
    wallet_id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      comment: '錢包ID',
      references: {
        model: 'wallets',
        key: 'id'
      }
    },
    order_id: {
      type: DataTypes.STRING(255),
      allowNull: true,
      comment: '第三方订单编号'
    },
    gateway_id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      comment: '通道ID',
      references: {
        model: 'gateways',
        key: 'id'
      }
    },
    amount: {
      type: DataTypes.BIGINT,
      allowNull: false,
      comment: '充值金额，单位分'
    },
    fee: {
      type: DataTypes.BIGINT,
      allowNull: true,
      comment: '手续费，单位分'
    },
    payout: {
      type: DataTypes.BIGINT,
      allowNull: true,
      comment: '实付金额，单位分'
    },
    response: {
      type: DataTypes.JSON,
      allowNull: true,
      comment: '第三方返回数据'
    },
    status: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: '0',
      comment: '状态 0:待處理 1:處理中 2:充值成功 3:充值失败'
    },
    paid_at: {
      type: DataTypes.DATE,
      allowNull: true,
      comment: '支付时间'
    },
    remark: {
      type: DataTypes.STRING(255),
      allowNull: true,
      defaultValue: '',
      comment: '备注'
    }
  }, {
    sequelize,
    tableName: 'topups',
    hasTrigger: true,
    timestamps: true,
    paranoid: true,
    indexes: [
      {
        name: 'PRIMARY',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'id' }
        ]
      },
      {
        name: 'topups_wallet_id_foreign',
        using: 'BTREE',
        fields: [
          { name: 'wallet_id' }
        ]
      },
      {
        name: 'topups_gateway_id_foreign',
        using: 'BTREE',
        fields: [
          { name: 'gateway_id' }
        ]
      }
    ]
  });
};
