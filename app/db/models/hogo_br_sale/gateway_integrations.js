module.exports = function (sequelize, DataTypes) {
  return sequelize.define('gateway_integrations', {
    id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      primaryKey: true,
      comment: 'ID'
    },
    gatewayId: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      comment: '通道ID',
      references: {
        model: 'gateways',
        key: 'id'
      }
    },
    channelId: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      comment: '代理ID',
      references: {
        model: 'channels',
        key: 'id'
      }
    },
    status: {
      type: DataTypes.TINYINT,
      allowNull: false,
      defaultValue: 0,
      comment: '状态 0:停用 1:启用'
    },
    sortId: {
      type: DataTypes.TINYINT,
      allowNull: true,
      defaultValue: 0,
      comment: '排序'
    },
    remark: {
      type: DataTypes.STRING(255),
      allowNull: true,
      defaultValue: '',
      comment: '备注'
    }
  }, {
    sequelize,
    tableName: 'gateway_integrations',
    timestamps: true,
    underscored: true,
    indexes: [
      {
        name: 'PRIMARY',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'id' }
        ]
      },
      {
        name: 'gateway_id_channel_id_unique',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'gateway_id' },
          { name: 'channel_id' }
        ]
      },
      {
        name: 'gateway_integrations_channel_id_foreign',
        using: 'BTREE',
        fields: [
          { name: 'channel_id' }
        ]
      }
    ]
  });
};
