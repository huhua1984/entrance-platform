const Sequelize = require('sequelize');
module.exports = function (sequelize, DataTypes) {
  const gameProviders = sequelize.define('gameProviders', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER
    },
    code: {
      allowNull: false,
      type: Sequelize.STRING(64)
    },
    name: {
      allowNull: false,
      type: Sequelize.STRING(64)
    },
    channelUrl: {
      type: Sequelize.STRING(128)
    },
    recordUrl: {
      type: Sequelize.STRING(128)
    },
    logo: {
      type: Sequelize.STRING(128)
    },
    status: {
      allowNull: false,
      type: Sequelize.SMALLINT(1),
      defaultValue: 1,
      comment: '1: 開啟, 0: 關閉, -1: 刪除'
    },
    updateTime: {
      type: Sequelize.DATE,
      defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
      onUpdate: 'CURRENT_TIMESTAMP'
    },
    createTime: {
      allowNull: false,
      type: Sequelize.DATE,
      defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    sequelize,
    tableName: 'gameProviders',
    timestamps: false,
    indexes: [
      {
        name: 'PRIMARY',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'id' }
        ]
      }

    ]
  });
  gameProviders.associate = function (models) {
    gameProviders.belongsToMany(models.agents, { through: 'gameProviderBindAgents' });
  };
  return gameProviders;
};
