const Sequelize = require('sequelize');
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('styles', {
    id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      primaryKey: true,
      comment: 'ID'
    },
    channel_id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      comment: '代理ID',
      references: {
        model: 'channels',
        key: 'id'
      },
      unique: 'styles_channel_id_foreign'
    },
    icon: {
      type: DataTypes.STRING(255),
      allowNull: true,
      comment: '图标'
    },
    logo: {
      type: DataTypes.STRING(255),
      allowNull: true,
      comment: 'logo'
    },
    banner: {
      type: DataTypes.STRING(255),
      allowNull: true,
      comment: '横幅'
    },
    background: {
      type: DataTypes.STRING(255),
      allowNull: true,
      comment: '背景图'
    }
  }, {
    sequelize,
    tableName: 'styles',
    timestamps: true,
    indexes: [
      {
        name: 'PRIMARY',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'id' }
        ]
      },
      {
        name: 'styles_channel_id_unique',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'channel_id' }
        ]
      }
    ]
  });
};
