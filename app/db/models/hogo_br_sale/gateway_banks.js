const Sequelize = require('sequelize');
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('gateway_banks', {
    gateway_id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      primaryKey: true,
      comment: '通道ID',
      references: {
        model: 'gateways',
        key: 'id'
      }
    },
    bank_id: {
      type: DataTypes.STRING(255),
      allowNull: false,
      primaryKey: true,
      comment: '银行ID',
      references: {
        model: 'banks',
        key: 'id'
      }
    }
  }, {
    sequelize,
    tableName: 'gateway_banks',
    timestamps: true,
    indexes: [
      {
        name: 'PRIMARY',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'gateway_id' },
          { name: 'bank_id' }
        ]
      },
      {
        name: 'gateway_banks_bank_id_foreign',
        using: 'BTREE',
        fields: [
          { name: 'bank_id' }
        ]
      }
    ]
  });
};
