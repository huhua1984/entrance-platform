module.exports = function (sequelize, DataTypes) {
  return sequelize.define('withdrawals', {
    id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      primaryKey: true,
      comment: '订单编号'
    },
    wallet_id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      comment: '錢包ID',
      references: {
        model: 'wallets',
        key: 'id'
      }
    },
    bank_account_id: {
      type: DataTypes.CHAR(36),
      allowNull: true,
      comment: '银行帳户ID',
      references: {
        model: 'bank_accounts',
        key: 'id'
      }
    },
    order_id: {
      type: DataTypes.STRING(255),
      allowNull: true,
      comment: '第三方订单编号'
    },
    gateway_id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      comment: '通道ID',
      references: {
        model: 'gateways',
        key: 'id'
      }
    },
    amount: {
      type: DataTypes.BIGINT,
      allowNull: false,
      comment: '提现金额，单位分'
    },
    fee: {
      type: DataTypes.BIGINT,
      allowNull: true,
      comment: '手续费，单位分'
    },
    payout: {
      type: DataTypes.BIGINT,
      allowNull: true,
      comment: '实付金额，单位分'
    },
    response: {
      type: DataTypes.JSON,
      allowNull: true,
      comment: '第三方返回数据'
    },
    admin_id: {
      type: DataTypes.BIGINT,
      allowNull: true,
      comment: '审核人员'
    },
    verified_at: {
      type: DataTypes.DATE,
      allowNull: true,
      comment: '审核时间'
    },
    status: {
      type: DataTypes.TINYINT,
      allowNull: false,
      defaultValue: 0,
      comment: '状态 0:待审核 1:审核通过 2:审核拒绝 3:提現代付 4:提现成功 5:提现失败'
    },
    remark: {
      type: DataTypes.STRING(255),
      allowNull: true,
      defaultValue: '',
      comment: '备注'
    }
  }, {
    sequelize,
    tableName: 'withdrawals',
    hasTrigger: true,
    timestamps: true,
    paranoid: true,
    indexes: [
      {
        name: 'PRIMARY',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'id' }
        ]
      },
      {
        name: 'withdrawals_wallet_id_foreign',
        using: 'BTREE',
        fields: [
          { name: 'wallet_id' }
        ]
      },
      {
        name: 'withdrawals_gateway_id_foreign',
        using: 'BTREE',
        fields: [
          { name: 'gateway_id' }
        ]
      },
      {
        name: 'withdrawals_bank_account_id_foreign',
        using: 'BTREE',
        fields: [
          { name: 'bank_account_id' }
        ]
      }
    ]
  });
};
