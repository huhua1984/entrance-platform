const Sequelize = require('sequelize');
module.exports = function (sequelize, DataTypes) {
  const agents = sequelize.define('agents', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: Sequelize.INTEGER
    },
    uid: {
      allowNull: false,
      type: Sequelize.INTEGER,
      defaultValue: 0
    },
    name: {
      allowNull: false,
      type: Sequelize.STRING(64)
    },
    account: {
      allowNull: false,
      type: Sequelize.STRING(64)
    },
    password: {
      allowNull: false,
      type: Sequelize.STRING
    },
    otp: {
      type: Sequelize.STRING(128)
    },
    status: {
      allowNull: false,
      type: Sequelize.SMALLINT(1),
      defaultValue: 1,
      comment: '1: 開啟, 0: 關閉, -1: 刪除'
    },
    brand: {
      type: Sequelize.STRING(32),
      allowNull: false
    },
    promoCode: {
      type: Sequelize.STRING(32),
      allowNull: false
    },
    promotionUrl: {
      type: Sequelize.STRING(128)
    },
    isDelete: {
      type: Sequelize.INTEGER(11),
      allowNull: false,
      defaultValue: 0
    },
    lastLoginTime: {
      type: Sequelize.DATE
    },
    deleteTime: {
      type: Sequelize.DATE
    },
    updateTime: {
      type: Sequelize.DATE,
      defaultValue: Sequelize.literal('CURRENT_TIMESTAMP'),
      onUpdate: 'CURRENT_TIMESTAMP'
    },
    createTime: {
      allowNull: false,
      type: Sequelize.DATE,
      defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    sequelize,
    tableName: 'agents',
    timestamps: false,
    indexes: [
      {
        name: 'PRIMARY',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'id' }
        ]
      },
      {
        name: 'idx_account',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'account' }
        ]
      }
    ]
  });
  agents.associate = function (models) {
    agents.hasMany(models.players);
    agents.belongsToMany(models.gameProviders, { through: 'gameProviderBindAgents' });
  };
  return agents;
};
