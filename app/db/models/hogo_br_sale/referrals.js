const Sequelize = require('sequelize');
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('referrals', {
    id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      primaryKey: true,
      comment: '用戶ID',
      references: {
        model: 'users',
        key: 'id'
      }
    },
    parent_id: {
      type: DataTypes.CHAR(36),
      allowNull: true,
      comment: '上級推荐用戶ID',
      references: {
        model: 'referrals',
        key: 'id'
      }
    },
    root_id: {
      type: DataTypes.CHAR(36),
      allowNull: true,
      comment: '根級推荐用戶ID',
      references: {
        model: 'users',
        key: 'id'
      }
    },
    remark: {
      type: DataTypes.STRING(255),
      allowNull: true,
      defaultValue: '',
      comment: '备注'
    }
  }, {
    sequelize,
    tableName: 'referrals',
    hasTrigger: true,
    timestamps: true,
    paranoid: true,
    indexes: [
      {
        name: 'PRIMARY',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'id' }
        ]
      },
      {
        name: 'referrals_root_id_index',
        using: 'BTREE',
        fields: [
          { name: 'root_id' }
        ]
      },
      {
        name: 'referrals_parent_id_foreign',
        using: 'BTREE',
        fields: [
          { name: 'parent_id' }
        ]
      }
    ]
  });
};
