module.exports = function (sequelize, DataTypes) {
  return sequelize.define('daily_report_user', {
    time: {
      type: DataTypes.DATEONLY,
      allowNull: false,
      primaryKey: true,
      comment: '日期'
    },
    user_id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      primaryKey: true,
      comment: '用戶ID'
    },
    symbol: {
      type: DataTypes.STRING(255),
      allowNull: false,
      primaryKey: true,
      comment: '貨幣單位'
    },
    topup: {
      type: DataTypes.BIGINT,
      allowNull: true,
      defaultValue: 0,
      comment: '現金充值'
    },
    withdrawal: {
      type: DataTypes.BIGINT,
      allowNull: true,
      defaultValue: 0,
      comment: '現金提款'
    },
    sys_cashier: {
      type: DataTypes.BIGINT,
      allowNull: true,
      defaultValue: 0,
      comment: '系統上下分'
    },
    token_purchase: {
      type: DataTypes.BIGINT,
      allowNull: true,
      defaultValue: 0,
      comment: '買luck幣金額'
    },
    token_receive: {
      type: DataTypes.BIGINT,
      allowNull: true,
      defaultValue: 0,
      comment: '得到luck幣金額'
    },
    roulette: {
      type: DataTypes.BIGINT,
      allowNull: true,
      defaultValue: 0,
      comment: '大轉盤投注'
    },
    roulette_reward: {
      type: DataTypes.BIGINT,
      allowNull: true,
      defaultValue: 0,
      comment: '大轉盤中獎'
    },
    sys_cashier_bonus: {
      type: DataTypes.BIGINT,
      allowNull: true,
      defaultValue: 0,
      comment: '后台赠金'
    },
    commission_withdraw: {
      type: DataTypes.BIGINT,
      allowNull: true,
      defaultValue: 0,
      comment: '团队奖励转出'
    },
    commission_redemption: {
      type: DataTypes.BIGINT,
      allowNull: true,
      defaultValue: 0,
      comment: '团队奖励转入'
    },
    sys_cashier_salary: {
      type: DataTypes.BIGINT,
      allowNull: true,
      defaultValue: 0,
      comment: '工资'
    },
    promotion_chest: {
      type: DataTypes.BIGINT,
      allowNull: true,
      defaultValue: 0,
      comment: 'VIP寶箱'
    },
    invitation_chest: {
      type: DataTypes.BIGINT,
      allowNull: true,
      defaultValue: 0,
      comment: '邀請寶箱'
    },
    bonus_topup_initial: {
      type: DataTypes.BIGINT,
      allowNull: true,
      defaultValue: 0,
      comment: '首充赠送'
    },
    bonus_topup_fever: {
      type: DataTypes.BIGINT,
      allowNull: true,
      defaultValue: 0,
      comment: '充值赠送'
    }
  }, {
    sequelize,
    tableName: 'daily_report_user',
    timestamps: true,
    indexes: [
      {
        name: 'PRIMARY',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'time' },
          { name: 'user_id' },
          { name: 'symbol' }
        ]
      },
      {
        name: 'index_time',
        using: 'BTREE',
        fields: [
          { name: 'time' }
        ]
      }
    ]
  });
};
