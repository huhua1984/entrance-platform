const Sequelize = require('sequelize');
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('daily_report_all', {
    time: {
      type: DataTypes.DATEONLY,
      allowNull: false,
      primaryKey: true,
      comment: '日期'
    },
    channel_id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      primaryKey: true,
      comment: '代理ID'
    },
    symbol: {
      type: DataTypes.STRING(255),
      allowNull: false,
      primaryKey: true,
      comment: '貨幣單位'
    },
    newuser: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: 0,
      comment: '注册人數'
    },
    activeuser: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: 0,
      comment: '活跃用户'
    },
    newaccess: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: 0,
      comment: '新用户访问'
    },
    paymoney: {
      type: DataTypes.BIGINT,
      allowNull: true,
      defaultValue: 0,
      comment: '充值金额'
    },
    peypeople: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: 0,
      comment: '充值人数'
    },
    newusermoney: {
      type: DataTypes.BIGINT,
      allowNull: true,
      defaultValue: 0,
      comment: '首存金额'
    },
    newuserpeople: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: 0,
      comment: '首存人数'
    },
    reflectnum: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: 0,
      comment: '提款人数'
    },
    reflectmoney: {
      type: DataTypes.BIGINT,
      allowNull: true,
      defaultValue: 0,
      comment: '提款金额'
    },
    reflectsalary: {
      type: DataTypes.BIGINT,
      allowNull: true,
      defaultValue: 0,
      comment: '博主提现(工资)'
    },
    reflectreward: {
      type: DataTypes.BIGINT,
      allowNull: true,
      defaultValue: 0,
      comment: '团队提现(团队奖励)'
    },
    profit: {
      type: DataTypes.BIGINT,
      allowNull: true,
      defaultValue: 0,
      comment: '利润'
    },
    payfees: {
      type: DataTypes.BIGINT,
      allowNull: true,
      defaultValue: 0,
      comment: '充值手续费'
    },
    reflectfees: {
      type: DataTypes.BIGINT,
      allowNull: true,
      defaultValue: 0,
      comment: '充值手续费'
    },
    yesterdaypaypeople: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: 0,
      comment: '昨日充值人数'
    },
    repaypeople: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: 0,
      comment: '复冲人数'
    }
  }, {
    sequelize,
    tableName: 'daily_report_all',
    timestamps: true,
    indexes: [
      {
        name: 'PRIMARY',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'time' },
          { name: 'channel_id' },
          { name: 'symbol' }
        ]
      },
      {
        name: 'index_time',
        using: 'BTREE',
        fields: [
          { name: 'time' }
        ]
      }
    ]
  });
};
