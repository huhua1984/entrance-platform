const Sequelize = require('sequelize');
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('sys_cron_partition', {
    ID: {
      autoIncrement: true,
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    PartitionDatabase: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: '',
      comment: '分区的资料库名称'
    },
    PartitionTable: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: '',
      comment: '分区的表格名称'
    },
    PartitionType: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: '',
      comment: '分区的类型依据'
    },
    PartitionColumns: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: '',
      comment: '分区的栏位名称依据'
    },
    PartitionFunction: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: '',
      comment: '分区的栏位方法依据'
    },
    PartitionStatus: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: 0,
      comment: '0: 关闭, 1:开启'
    },
    CreateTime: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: Sequelize.Sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    sequelize,
    tableName: 'sys_cron_partition',
    timestamps: false,
    indexes: [
      {
        name: 'PRIMARY',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'ID' }
        ]
      }
    ]
  });
};
