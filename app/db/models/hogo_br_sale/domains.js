const Sequelize = require('sequelize');
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('domains', {
    id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      primaryKey: true,
      comment: 'ID'
    },
    channel_id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      comment: '代理ID',
      references: {
        model: 'channels',
        key: 'id'
      }
    },
    hostname: {
      type: DataTypes.STRING(255),
      allowNull: false,
      comment: '域名',
      unique: 'domains_hostname_unique'
    },
    status: {
      type: DataTypes.TINYINT,
      allowNull: false,
      defaultValue: 0,
      comment: '状态 0:停用 1:启用'
    },
    remark: {
      type: DataTypes.STRING(255),
      allowNull: true,
      defaultValue: '',
      comment: '备注'
    }
  }, {
    sequelize,
    tableName: 'domains',
    timestamps: true,
    indexes: [
      {
        name: 'PRIMARY',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'id' }
        ]
      },
      {
        name: 'domains_hostname_unique',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'hostname' }
        ]
      },
      {
        name: 'domains_channel_id_foreign',
        using: 'BTREE',
        fields: [
          { name: 'channel_id' }
        ]
      }
    ]
  });
};
