const Sequelize = require('sequelize');
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('invite_reward_logs', {
    id: {
      autoIncrement: true,
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    user_id: {
      type: DataTypes.CHAR(36),
      allowNull: true,
      comment: '受益UID'
    },
    parent_id: {
      type: DataTypes.CHAR(36),
      allowNull: true,
      comment: '上级UID'
    },
    from_id: {
      type: DataTypes.CHAR(36),
      allowNull: true,
      comment: '觸發玩家ID'
    },
    reward: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
      comment: '奖励'
    },
    remark: {
      type: DataTypes.STRING(200),
      allowNull: true,
      comment: '描述'
    },
    from_type: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
      comment: '1.注册,2.充值，3.投注 4.扩展，5.vip'
    },
    record_date: {
      type: DataTypes.DATE,
      allowNull: true,
      comment: '统计日期'
    },
    status: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
      comment: '0未处理，1结算中，2为已结算'
    },
    reward_luck: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0
    }
  }, {
    sequelize,
    tableName: 'invite_reward_logs',
    timestamps: true,
    indexes: [
      {
        name: 'PRIMARY',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'id' }
        ]
      }
    ]
  });
};
