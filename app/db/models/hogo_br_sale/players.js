const Sequelize = require('sequelize');
module.exports = function (sequelize, DataTypes) {
  const player = sequelize.define('players', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    account: {
      allowNull: false,
      type: DataTypes.STRING(64)
    },
    password: {
      allowNull: false,
      type: DataTypes.STRING
    },
    nickname: {
      type: DataTypes.STRING
    },
    agentId: {
      allowNull: false,
      type: Sequelize.INTEGER
    },
    otp: {
      type: DataTypes.STRING(128)
    },
    isDelete: {
      type: Sequelize.INTEGER(11),
      allowNull: false,
      defaultValue: 0
    },
    createTime: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: Sequelize.Sequelize.literal('CURRENT_TIMESTAMP')
    },
    updateTime: {
      type: Sequelize.DATE,
      defaultValue: Sequelize.Sequelize.literal('CURRENT_TIMESTAMP'),
      onUpdate: 'CURRENT_TIMESTAMP'
    }
  }, {
    sequelize,
    tableName: 'players',
    timestamps: false,
    indexes: [
      {
        name: 'PRIMARY',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'id' }
        ]
      },
      {
        name: 'idx_account',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'account' }
        ]
      }
    ]
  });
  player.associate = function (models) {
    player.belongsTo(models.agents);
  };
  return player;
};
