const Sequelize = require('sequelize');
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('exchange_rates', {
    id: {
      autoIncrement: true,
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    base: {
      type: DataTypes.STRING(10),
      allowNull: false,
      comment: '基準貨幣'
    },
    quote: {
      type: DataTypes.STRING(10),
      allowNull: false,
      comment: '報價貨幣'
    },
    rate: {
      type: DataTypes.DECIMAL(15, 6),
      allowNull: false,
      comment: '匯率值'
    }
  }, {
    sequelize,
    tableName: 'exchange_rates',
    timestamps: true,
    indexes: [
      {
        name: 'PRIMARY',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'id' }
        ]
      },
      {
        name: 'currency_pair_unique',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'base' },
          { name: 'quote' }
        ]
      }
    ]
  });
};
