module.exports = function (sequelize, DataTypes) {
  return sequelize.define('activities', {
    id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      primaryKey: true,
      comment: 'ID'
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false,
      comment: '活动名称'
    },
    slug: {
      type: DataTypes.STRING(255),
      allowNull: false,
      comment: '活动标识符',
      unique: 'activity_slug_unique'
    },
    status: {
      type: DataTypes.TINYINT,
      allowNull: false,
      defaultValue: 1,
      comment: '状态 0:停用 1:启用'
    }
  }, {
    sequelize,
    tableName: 'activities',
    timestamps: true,
    indexes: [
      {
        name: 'PRIMARY',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'id' }
        ]
      },
      {
        name: 'activity_slug_unique',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'slug' }
        ]
      }
    ]
  });
};
