const Sequelize = require('sequelize');
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('referral_edges', {
    referral_id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      primaryKey: true,
      comment: 'ID',
      references: {
        model: 'referrals',
        key: 'id'
      }
    },
    depth: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      defaultValue: 0,
      comment: '边的数量或关联记录的数量'
    },
    count: {
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      defaultValue: 0,
      comment: '子代节点数'
    }
  }, {
    sequelize,
    tableName: 'referral_edges',
    timestamps: false,
    indexes: [
      {
        name: 'PRIMARY',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'referral_id' }
        ]
      }
    ]
  });
};
