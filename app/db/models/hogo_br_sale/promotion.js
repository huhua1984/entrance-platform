const Sequelize = require('sequelize');
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('promotion', {
    id: {
      autoIncrement: true,
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    channel_id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      comment: '代理ID',
      references: {
        model: 'channels',
        key: 'id'
      }
    },
    slug: {
      type: DataTypes.STRING(255),
      allowNull: false,
      comment: '标签'
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false,
      comment: '标题'
    },
    type: {
      type: DataTypes.TINYINT,
      allowNull: false,
      defaultValue: 1,
      comment: '状态 1:banner 2:promo'
    },
    img: {
      type: DataTypes.STRING(255),
      allowNull: true,
      comment: '图片路径'
    },
    img_mobile: {
      type: DataTypes.STRING(255),
      allowNull: true,
      comment: '手机图片路径'
    },
    actionType: {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: '行为 1:开弹窗 2:跳外部页面 3:导自家path 4:无任何作用纯粹秀图'
    },
    path: {
      type: DataTypes.STRING(255),
      allowNull: true,
      comment: '前端导引路径'
    },
    status: {
      type: DataTypes.TINYINT,
      allowNull: false,
      defaultValue: 1,
      comment: '状态 0:停用 1:启用'
    },
    content: {
      type: DataTypes.TEXT,
      allowNull: true,
      comment: '内页html'
    },
    sort_id: {
      type: DataTypes.INTEGER,
      allowNull: true,
      comment: '排序'
    }
  }, {
    sequelize,
    tableName: 'promotion',
    timestamps: true,
    indexes: [
      {
        name: 'PRIMARY',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'id' }
        ]
      },
      {
        name: 'promotion_channel_id_slug_unique',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'channel_id' },
          { name: 'slug' }
        ]
      }
    ]
  });
};
