const Sequelize = require('sequelize');
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('sys_prolog', {
    logdate: {
      type: DataTypes.DATE,
      allowNull: false
    },
    proname: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: ''
    },
    time: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    mark: {
      type: DataTypes.STRING(255),
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'sys_prolog',
    timestamps: false
  });
};
