const Sequelize = require('sequelize');
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('labels', {
    id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      primaryKey: true,
      comment: 'ID'
    },
    name: {
      type: DataTypes.STRING(255),
      allowNull: false,
      comment: '標籤名称'
    },
    slug: {
      type: DataTypes.STRING(255),
      allowNull: false,
      comment: '標籤标识',
      unique: 'labels_slug_unique'
    }
  }, {
    sequelize,
    tableName: 'labels',
    timestamps: true,
    indexes: [
      {
        name: 'PRIMARY',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'id' }
        ]
      },
      {
        name: 'labels_slug_unique',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'slug' }
        ]
      }
    ]
  });
};
