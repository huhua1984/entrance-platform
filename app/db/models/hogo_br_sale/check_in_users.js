const Sequelize = require('sequelize');
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('check_in_users', {
    user_id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      primaryKey: true,
      comment: '用户ID'
    },
    combo: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 0,
      comment: '连续签到天数'
    }
  }, {
    sequelize,
    tableName: 'check_in_users',
    timestamps: true,
    indexes: [
      {
        name: 'PRIMARY',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'user_id' }
        ]
      }
    ]
  });
};
