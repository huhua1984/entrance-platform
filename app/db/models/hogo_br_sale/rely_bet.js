const Sequelize = require('sequelize');
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('rely_bet', {
    statis_date: {
      type: DataTypes.DATEONLY,
      allowNull: false,
      comment: '統計日期'
    },
    game_id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      comment: '遊戲編號'
    },
    user_id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      comment: '玩家編號'
    },
    wallet_id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      comment: '錢包編號'
    },
    gateway_id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      comment: '廠商編號'
    },
    channel_id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      comment: '代理編號'
    },
    sum_consume: {
      type: DataTypes.BIGINT,
      allowNull: true,
      defaultValue: 0,
      comment: '總打碼'
    },
    sum_reward: {
      type: DataTypes.BIGINT,
      allowNull: true,
      defaultValue: 0,
      comment: '總獲利'
    },
    sum_num: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: 0,
      comment: '總局数'
    },
    create_time: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: Sequelize.Sequelize.literal('CURRENT_TIMESTAMP')
    },
    update_time: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: Sequelize.Sequelize.literal('CURRENT_TIMESTAMP')
    }
  }, {
    sequelize,
    tableName: 'rely_bet',
    timestamps: false,
    indexes: [
      {
        name: 'rely_bet_UN',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'statis_date' },
          { name: 'game_id' },
          { name: 'user_id' },
          { name: 'wallet_id' },
          { name: 'gateway_id' },
          { name: 'channel_id' }
        ]
      },
      {
        name: 'rely_bet_statis_date_index',
        using: 'BTREE',
        fields: [
          { name: 'statis_date' }
        ]
      },
      {
        name: 'rely_bet_statis_date_user_id_index',
        using: 'BTREE',
        fields: [
          { name: 'statis_date' },
          { name: 'user_id' }
        ]
      },
      {
        name: 'rely_bet_statis_date_gateway_id_game_id_index',
        using: 'BTREE',
        fields: [
          { name: 'statis_date' },
          { name: 'gateway_id' },
          { name: 'game_id' }
        ]
      }
    ]
  });
};
