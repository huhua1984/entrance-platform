const Sequelize = require('sequelize');
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('one_touch_users', {
    id: {
      autoIncrement: true,
      type: DataTypes.BIGINT.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    token: {
      type: DataTypes.STRING(255),
      allowNull: false,
      defaultValue: '',
      comment: '用户token'
    },
    wallet_id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      comment: '錢包id'
    },
    type: {
      type: DataTypes.INTEGER,
      allowNull: false,
      comment: '0:oneTouch, 1:Pariplay, 7:PG'
    },
    GameCode: {
      type: DataTypes.STRING(255),
      allowNull: true,
      comment: '游戏代码'
    }
  }, {
    sequelize,
    tableName: 'one_touch_users',
    timestamps: true,
    indexes: [
      {
        name: 'PRIMARY',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'id' }
        ]
      }
    ]
  });
};
