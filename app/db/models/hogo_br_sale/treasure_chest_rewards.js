const Sequelize = require('sequelize');
module.exports = function (sequelize, DataTypes) {
  return sequelize.define('treasure_chest_rewards', {
    id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      primaryKey: true,
      comment: 'ID'
    },
    treasure_chest_id: {
      type: DataTypes.CHAR(36),
      allowNull: false,
      comment: '寶箱ID',
      references: {
        model: 'treasure_chests',
        key: 'id'
      }
    },
    symbol: {
      type: DataTypes.STRING(10),
      allowNull: false,
      comment: '貨幣類型'
    },
    amount: {
      type: DataTypes.BIGINT,
      allowNull: false,
      comment: '獎勵數量'
    }
  }, {
    sequelize,
    tableName: 'treasure_chest_rewards',
    timestamps: true,
    indexes: [
      {
        name: 'PRIMARY',
        unique: true,
        using: 'BTREE',
        fields: [
          { name: 'id' }
        ]
      },
      {
        name: 'treasure_chest_rewards_treasure_chest_id_foreign',
        using: 'BTREE',
        fields: [
          { name: 'treasure_chest_id' }
        ]
      }
    ]
  });
};
