module.exports = {
  env: {
    browser: true,
    es2021: true,
    es6: true,
    jquery: true,
    mocha: true
  },
  extends: [
    'semistandard'
  ],
  ignorePatterns: ['app/node_modules/**/*.js'],
  parserOptions: {
    ecmaVersion: 13
  },
  globals: {
    logger: true
  }
};
