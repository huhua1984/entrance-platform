require('dotenv').config();
const logger = require('../libs/logger');
const clc = require('cli-color');

module.exports = {
  hogo_br_sale: {
    replication: {
      read: {
        username: process.env.DB_SLAVE_USERNAME,
        password: process.env.DB_SLAVE_PASSWORD,
        database: 'hogo_br_sale',
        host: process.env.DB_SLAVE_HOST,
        port: process.env.DB_SLAVE_PORT
      },
      write: {
        username: process.env.DB_MASTER_USERNAME,
        password: process.env.DB_MASTER_PASSWORD,
        database: 'hogo_br_sale',
        host: process.env.DB_MASTER_HOST,
        port: process.env.DB_MASTER_PORT
      }
    },
    logging: (msg) => { logger.sql(clc.cyan(msg)); },
    dialect: 'mysql',
    pool: {
      max: 10, // maximum number of connections in the pool
      min: 1, // minimum number of connections in the pool
      acquire: 30000, // maximum time, in milliseconds, that pool will try to get connection before throwing error
      idle: 10000 // maximum time, in milliseconds, that a connection can be idle before being released
    }
  },
  hogo_br_play: {
    replication: {
      read: {
        username: process.env.DB_SLAVE_USERNAME,
        password: process.env.DB_SLAVE_PASSWORD,
        database: 'hogo_br_play',
        host: process.env.DB_SLAVE_HOST,
        port: process.env.DB_SLAVE_PORT
      },
      write: {
        username: process.env.DB_MASTER_USERNAME,
        password: process.env.DB_MASTER_PASSWORD,
        database: 'hogo_br_play',
        host: process.env.DB_MASTER_HOST,
        port: process.env.DB_MASTER_PORT
      }
    },
    logging: (msg) => { logger.sql(clc.cyan(msg)); },
    dialect: 'mysql',
    pool: {
      max: 10, // maximum number of connections in the pool
      min: 1, // minimum number of connections in the pool
      acquire: 30000, // maximum time, in milliseconds, that pool will try to get connection before throwing error
      idle: 10000 // maximum time, in milliseconds, that a connection can be idle before being released
    }
  }
};
