const Redis = require('ioredis');
require('dotenv').config();

module.exports = new Redis({
  host: process.env.REDIS_DEFAULT_HOST,
  port: process.env.REDIS_DEFAULT_PORT,
  db: process.env.REDIS_DEFAULT_DATABASE,
  password: process.env.REDIS_DEFAULT_PASSWORD,
  connectTimeout: 10000,
  maxRetriesPerRequest: 3
});
