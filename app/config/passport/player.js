const passport = require('passport');
const bcrypt = require('bcrypt');
const LocalStrategy = require('passport-local').Strategy;
const TotpStrategy = require('passport-totp').Strategy;
const PhoneStrategy = require('passport-phone').Strategy;
const db = require('../../db/models');
const verificationRespository = require('../../repositories/verification');
const users = db.users;
const channels = db.channels;
const authPlayer = new passport.Passport();

// setup passport strategy
authPlayer.use(new LocalStrategy(
  // customize user field
  {
    usernameField: 'username',
    passwordField: 'password',
    passReqToCallback: true
  },
  // authenticate user
  async (req, username, password, done) => {
    try {
      const channel = await channels.findOne({ attributes: ['cooperationFunc', 'status', 'authParameter', 'language', 'statusMaintain'], where: { id: req.body.code }, raw: true });
      if (!channel) return done(null, false, { message: req.t('5_errorMsg_8') });
      if (channel.status === 0) return done(null, false, { message: req.t('0_channelDisabled') });
      if (channel.statusMaintain === 1) return done(null, false, { message: req.t('4_UnderMain') });
      const user = await users.findOne({ where: { username, channelId: req.body.code } });
      if (!user) return done(null, false, { message: req.t('1_accountPasswordErrorMsg') });
      if (user.status === 0) return done(null, false, { message: req.t('0_accountSuspended') });
      const hashPass = /^\$2y\$/.test(user.password) ? '$2b$' + user.password.slice(4) : user.password;
      if (!bcrypt.compareSync(password, hashPass)) return done(null, false, { message: req.t('1_accountPasswordErrorMsg') });
      await user.update({ loginAt: new Date() });

      return done(null, user);
    } catch (error) {
      return done(error);
    }
  }
));

authPlayer.use(new PhoneStrategy(
  {
    passReqToCallback: true
  },
  async (req, phoneNumber, verifyCode, done) => {
    try {
      const user = await users.findOne({ where: { phoneNumber, channelId: req.body.code } });
      if (!user) { return done(null, false, { message: req.t('0_phoneNumberIsNotRegiser') }); }
      if (user.status === 0) return done(null, false, { message: req.t('0_accountSuspended') });
      const verifyRes = await verificationRespository.verifyPhoneSMSCode({ verifyCode, phoneNumber, channelId: req.body.code });
      if (!verifyRes) { return done(null, false, { message: req.t('0_verifyFailed') }); }
      await user.update({ loginAt: new Date() });
      return done(null, user);
    } catch (error) {
      logger.error(error);
      done(error);
    }
  }
));

// serialize and deserialize user
authPlayer.serializeUser((user, cb) => {
  cb(null, { serial: user.serial });
});
authPlayer.deserializeUser((user, cb) => {
  users.findByPk(user.serial, { include: [{ association: 'channel' }] }).then(_user => {
    return cb(null, _user);
  });
});

const passportJWT = require('passport-jwt');
const ExtractJwt = passportJWT.ExtractJwt;
const JwtStrategy = passportJWT.Strategy;

const jwtOptions = {};
jwtOptions.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
jwtOptions.secretOrKey = process.env.JWT_SECRET;

const strategy = new JwtStrategy(jwtOptions, function (jwtPayload, next) {
  users.findByPk(jwtPayload.id, {
    include: [{ association: 'channel' }]
  }).then(user => {
    if (!user) return next(null, false);
    return next(null, user);
  });
});
authPlayer.use(strategy);

authPlayer.use(new TotpStrategy(
  async (user, done) => {
    return done(null, user.otp, 30);
  }
));

module.exports = authPlayer;
