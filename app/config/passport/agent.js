const passport = require('passport');
const LocalStrategy = require('passport-local');
const sequelize = require('sequelize');
const bcrypt = require('bcrypt');
const TotpStrategy = require('passport-totp').Strategy;
const db = require('../../db/models');
const adminUsers = db.admin_users;
const authAgent = new passport.Passport();
// setup passport strategy
authAgent.use(new LocalStrategy(
  // customize user field
  {
    usernameField: 'account',
    passwordField: 'password',
    passReqToCallback: true
  },
  // authenticate user
  async (req, username, password, done) => {
    try {
      const user = await adminUsers.findOne({ where: { username }, include: [{ association: 'channel', where: { serial: { [sequelize.Op.not]: 1000 } } }] });
      if (!user) return done(null, false, '帳號或密碼輸入錯誤！');
      const hashPass = /^\$2y\$/.test(user.password) ? '$2b$' + user.password.slice(4) : user.password;
      if (!bcrypt.compareSync(password, hashPass)) return done(null, false, '帳號或密碼輸入錯誤！');
      return done(null, user);
    } catch (error) {
      return done(error);
    }
  }
));

// serialize and deserialize user
authAgent.serializeUser((user, cb) => {
  cb(null, { id: user.id, role: 'agent' });
});
authAgent.deserializeUser((user, cb) => {
  adminUsers.findByPk(user.id, {}).then(_user => {
    return cb(null, _user);
  });
}
);

// JWT
if (process.env.PASSPORT_STRATEGY === 'jwt') {
  const passportJWT = require('passport-jwt');
  const ExtractJwt = passportJWT.ExtractJwt;
  const JwtStrategy = passportJWT.Strategy;

  const jwtOptions = {};
  jwtOptions.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
  jwtOptions.secretOrKey = process.env.JWT_SECRET;

  const strategy = new JwtStrategy(jwtOptions, function (jwtPayload, next) {
    adminUsers.findByPk(jwtPayload.id, {
      include: []
    }).then(user => {
      if (!user) return next(null, false);
      return next(null, user);
    });
  });
  authAgent.use(strategy);
}

if (process.env.PASSPORT_TOTP_ENABLE) {
  authAgent.use(new TotpStrategy(
    async (user, done) => {
      return done(null, user.otp, 30);
    }
  ));
}

module.exports = authAgent;
