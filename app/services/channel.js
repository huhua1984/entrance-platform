const db = require('../db/models');
const channels = db.channels;
const gameIntegrations = db.game_integrations;

const channelService = {};

channelService.getCooperation = async (req, res, next) => {
  try {
    const channel = await channels.findOne({ attributes: ['cooperationFunc', 'status', 'authParameter', 'language', 'statusMaintain', 'promotionUrlType'], where: { id: req.params.id }, raw: true });
    if (!channel) return res.apiRes('notFound', { message: req.t('0_promotionalLinkAlertMsg') });
    if (channel.status === 0) return res.apiRes('unavailable', { message: req.t('0_channelDisabled') });
    if (channel.statusMaintain === 1) return res.apiRes('unavailable', { message: req.t('4_UnderMain') });
    const lang = (channel.language || channel.authParameter?.lang || 'zh-cn').replace('_', '-');
    const { cooperationFunc, promotionUrlType } = channel;
    res.cookie('lang', lang);
    return res.apiRes('ok', { cooperationFunc, promotionUrlType, lang });
  } catch (err) {
    next(err);
  }
};

channelService.getGames = async (req, res, next) => {
  try {
    const channel = await channels.findOne({
      attributes: ['language'],
      where: { id: req.params.id },
      include: [{
        association: 'gateway_integrations',
        where: { status: 1 },
        include: [{
          association: 'gateway',
          attributes: ['slug'],
          where: { status: 1 }
        }]
      }]
    });
    if (!channel) return res.apiRes('ok', { games: [], message: req.t('19_nonactivated') });

    if (channel?.language) {
      req.setLocale(channel.language);
    }

    const gatewayIntegrates = channel.gateway_integrations.map(row => {
      return row.gateway?.slug;
    });
    const gameDatas = await gameIntegrations.findAll({
      attributes: ['gameId', 'gameName', 'gameNickName', 'gameCode', 'sortId', 'isHot', 'hotOrder', 'gameImg', 'gameType', 'flag', 'group', 'maintainStatus'],
      include: [{ association: 'gateway', attributes: ['name', 'slug'], where: { slug: gatewayIntegrates } },
        { association: 'game', attributes: ['maintainStatus', 'gameNickName'], where: { status: 1 } }
      ],
      order: [['sortId', 'ASC']],
      where: {
        channelId: req.params.id,
        status: 1
      }
    });
    const games = gameDatas.map(row => {
      const game = {
        gameId: row.gameId,
        gameName: req.t(`${row.game.gameNickName}`),
        gameNickName: row.game.gameNickName,
        gameCode: row.gameCode,
        sortId: row.sortId,
        isHot: row.isHot,
        hotOrder: row.hotOrder,
        gameImg: row.gameImg,
        maintainStatus: row.game.maintainStatus || row.maintainStatus || 0,
        gameType: row.gameType,
        flag: row.flag,
        group: row.group,
        gateway: row.gateway
      };
      return game;
    });
    return res.apiRes('ok', { games });
  } catch (error) {
    next(error);
  }
};

module.exports = channelService;
