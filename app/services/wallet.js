const db = require('../db/models');
const wallets = db.wallets;

const insert = async (req, res, next) => {
  try {
    const { userId, symbol, position } = req.body;
    await wallets.create({
      userId, symbol, position, createdAt: new Date()
    });
    res.apiRes('ok');
  } catch (error) {
    next(error);
  }
};

module.exports = { insert };
