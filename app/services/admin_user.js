const bcrypt = require('bcrypt');
const db = require('../db/models');
const logger = require('../libs/logger');
const admins = db.admin_users;

const updateChannelPassword = async (req, res, next) => {
  try {
    const adminUser = await admins.findOne({ where: { channel_id: req.params.channel_id } });
    if (!adminUser) {
      res.apiRes('notFound', { message: 'channel id 无此 admin_users' });
      return;
    }
    const password = req.body.password;
    logger.debug(`updateChannelPassword -> password: ${password}, channel_id: ${req.params.channel_id}`);
    await adminUser.update({ password: bcrypt.hashSync(password, bcrypt.genSaltSync(10)) });
    res.apiRes('ok');
  } catch (error) {
    next(error);
  }
};

module.exports = { updateChannelPassword };
