const vpGameRecordPuller = require('../../libs/gameRecordPuller/vp');
const { gameProviderAdapter } = require('../../libs/gameProviders/gameProviderAdaptor');

const vpService = {};
vpService.upsertVpLogsByChannel = async (req, res, next) => {
  try {
    const channelId = req.params.id;
    const startTime = new Date(req.body.startTime).getTime();
    const endTime = new Date(req.body.endTime).getTime();

    const rows = await vpGameRecordPuller.pullByChannel(startTime, endTime, channelId);
    res.apiRes('ok', { rows });
  } catch (error) {
    next(error);
  }
};

vpService.upsertVpLogs = async (req, res, next) => {
  try {
    const startTime = new Date(req.body.startTime).getTime();
    const endTime = new Date(req.body.endTime).getTime();
    await vpGameRecordPuller.pull(startTime, endTime);
    res.apiRes('ok');
  } catch (error) {
    next(error);
  }
};

vpService.startJob = async (req, res, next) => {
  try {
    vpGameRecordPuller.start();
  } catch (error) {
    next(error);
  }
};

vpService.stopJob = async (req, res, next) => {
  try {
    vpGameRecordPuller.stop();
  } catch (error) {
    next(error);
  }
};

vpService.seamlessWallet = async (req, res, next) => {
  try {
    // 已移植至TS
    const { agent, timestamp, param, key } = req.query;
    const data = await gameProviderAdapter.seamlessWallet('vp', { agent, timestamp, param, key });

    res.writeHeader(200, { 'Content-Type': 'text/plain;charset=utf8' });
    res.end(JSON.stringify(data));
  } catch (error) {
    next(error);
  }
};

module.exports = vpService;
