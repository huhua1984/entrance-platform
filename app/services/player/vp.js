const { Op } = require('sequelize');
const moment = require('moment');
const _ = require('lodash');
const db = require('../../db/models');
const utils = require('../../libs/utils');
const vpLog = db.vpLog;
const games = db.games;
const wallets = db.wallets;
const gameIntegrations = db.game_integrations;
const BigNumber = require('bignumber.js');
const { gameProviderAdapter } = require('../../libs/gameProviders/gameProviderAdaptor');

const getLogs = async (req, res, next) => {
  try {
    const user = req.user.toJSON();
    const startDate = req.query.startDate && req.query.startDate !== '' ? req.query.startDate : moment().subtract(24, 'h').format();
    const endDate = req.query.endDate && req.query.endDate !== '' ? req.query.endDate : moment().format();
    const pageSize = req.query.pageSize ? parseInt(req.query.pageSize) : 20; // 每页记录数
    const currentPage = req.query.currentPage ? parseInt(req.query.currentPage) : 1; // 当前页码
    const offSet = (currentPage - 1) * pageSize;
    const options = {
      username: user.username,
      gameEndTime: { [Op.between]: [startDate, endDate] }
    };
    if (moment(startDate).isAfter(endDate)) {
      res.apiRes('invalidArgument', { message: 'startDate is after endDate' });
    }
    if (req.query.gameId && req.query.gameId !== '') {
      options.gameId = req.query.gameId;
    }
    if (req.query.roomCode && req.query.roomCode !== '') {
      options.roomCode = req.query.roomCode;
    }

    const vpLogs = await vpLog.findAll({
      where: options,
      offset: offSet,
      limit: pageSize
    });

    const gameInfo = await games.findAll({ include: [{ association: 'gateway', where: { slug: ['vp', 'nc'] } }], raw: true });
    const gameInfoByMap = _.keyBy(gameInfo, 'id');
    const rows = vpLogs.map(row => {
      const gameName = gameInfoByMap[row.gameId]?.gameNickName || '';
      return {
        gameEndTime: row.gameEndTime,
        gameUserNo: row.gameUserNo,
        gameName: req.t(gameName),
        roomName: req.t('vp_room_' + row.roomCode) || req.t(gameName),
        currency: row.currency,
        allBet: row.allBet,
        profit: row.profit
      };
    });
    res.apiRes('ok', { rows });
  } catch (error) {
    next(error);
  }
};

const getBalance = async (req, res, next) => {
  const user = req.user.toJSON();
  try {
    const balance = await gameProviderAdapter.getBalance('vp', { username: user.username, channelId: user.channel_id });
    res.apiRes('ok', { balance, username: user.username, channelId: user.channel_id });
  } catch (error) {
    try {
      const wallet = await wallets.findOne({
        attributes: ['balance'],
        where: {
          userId: user.id,
          symbol: user.channel.symbol,
          position: 'TOKEN'
        }
      });
      if (!wallet) {
        await wallets.create({
          userId: user.id,
          symbol: user.channel.symbol,
          position: 'TOKEN'
        });
        res.apiRes('ok', { balance: 0, username: user.username, channelId: user.channel_id });
        return;
      }
      const balance = new BigNumber(wallet.balance).dividedBy(1000000).toNumber();
      res.apiRes('ok', { balance, username: user.username, channelId: user.channel_id });
    } catch (error) {
      next(error);
    }
  }
};

const launchGame = async (req, res, next) => {
  try {
    const user = req.user.toJSON();
    const gameData = await gameIntegrations.findOne({
      attributes: ['gameId', 'gameName', 'gameNickName', 'gameCode', 'maintainStatus'],
      include: [
        { association: 'game', attributes: ['maintainStatus'] }
      ],
      where: {
        channelId: user.channelId,
        gameCode: req.query.gameCode
      }
    });
    if (!gameData || gameData.maintainStatus === 1 || gameData.game.maintainStatus === 1) {
      res.apiRes('unavailable', { message: req.t('4_errorMsg_11') });
      return;
    }
    const ip = await utils.getIp(req).replace(/::ffff:/, '');
    const redirect = await gameProviderAdapter.getLoginUrl('vp', {
      channelId: user.channelId,
      username: user.username,
      kindId: req.query.gameCode,
      ip
    });
    res.apiRes('ok', { redirect, ...(res.locals.message ? { message: res.locals.message } : {}) });
  } catch (error) {
    next(error);
  }
};

module.exports = {
  getLogs,
  getBalance,
  launchGame
};
