const db = require('../../db/models');
const utils = require('../../libs/utils');
const { authenticator } = require('otplib');
const base32 = require('thirty-two');
const QRCode = require('qrcode');

const signInPage = (req, res, next) => {
  if (!req.query.code) {
    next(new Error('請輸入promo code!'));
    return;
  }
  res.render('player/signin', { code: req.query.code });
};

const indexPage = (req, res, next) => {
  res.render('index', { title: process.env.SERVICE_NAME, user: req.user.account });
};

const setupPage = async (req, res, next) => {
  try {
    const Users = req.user.isAgent ? db.agents : db.players;
    const user = await Users.findOne({ where: { id: req.user.id } });
    let key;
    if (user?.otp) {
      key = user.otp;
    } else {
      key = utils.randomKey(10);
      await user.update({ otp: key });
    }
    const encodedKey = base32.encode(key);
    const qrImage = await QRCode.toDataURL(authenticator.keyuri(user.account, process.env.SERVICE_NAME, encodedKey));

    res.render('setup', { user: req.user, key, qrImage, otpUrl: req.user.isAgent ? '/api/agent/otpSignin' : '/api/player/otpSignin' });
  } catch (error) {
    next(error);
  }
};

const signUpPage = (req, res, next) => {
  res.render('player/signup', { code: req.query.code });
};

module.exports = { signInPage, indexPage, setupPage, signUpPage };
