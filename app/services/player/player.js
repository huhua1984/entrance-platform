const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const { Op } = require('sequelize');
const moment = require('moment');
const db = require('../../db/models');
const users = db.users;
const channels = db.channels;
const wallets = db.wallets;
const walletLedgers = db.wallet_ledgers;

const passport = require('../../config/passport/player');
const utils = require('../../libs/utils');
const passwordGenerator = require('../../libs/passwordGenerator');
const verificationRespository = require('../../repositories/verification');
const { gameProviderAdapter } = require('../../libs/gameProviders/gameProviderAdaptor');
const { default: BigNumber } = require('bignumber.js');

const signIn = (req, res, next) => {
  passport.authenticate('local', function (err, user, info) {
    if (err) { return next(err); }
    if (!user) {
      return res.apiRes('permissionDenied', info);
    }
    req.logIn(user, function (err) {
      if (err) { return next(err); }
      next();
    });
  })(req, res, next);
};

const jwtSignIn = async (req, res, next) => {
  try {
    const { username, password } = req.body;
    const user = await users.findOne({ where: { username, channel_id: req.body.code } });
    if (!user) return res.apiRes({ message: req.t('1_accountPasswordErrorMsg') });
    if (user.status === 0) return res.apiRes({ message: req.t('0_accountSuspended') });
    const hashPass = /^\$2y\$/.test(user.password) ? '$2b$' + user.password.slice(4) : user.password;
    if (!bcrypt.compareSync(password, hashPass)) return res.apiRes({ message: req.t('1_accountPasswordErrorMsg') });
    await user.update({ login_at: new Date() });
    const payload = { id: user.serial };
    const token = jwt.sign(payload, process.env.JWT_SECRET);
    return res.apiRes('ok', { token });
  } catch (error) {
    next(error);
  }
};

const phoneSignIn = (req, res, next) => {
  passport.authenticate('phone', function (err, user, info) {
    if (err) { return next(err); }
    if (!user) {
      return res.apiRes('notFound', info);
    }
    req.logIn(user, function (err) {
      if (err) { return next(err); }
      next();
    });
  })(req, res, next);
};

const sendVerification = async (req, res, next) => {
  try {
    const { message, code } = await verificationRespository.sendVerification(req.body.phoneNumber, req.body.code, req.t);
    if (message) {
      res.apiRes('invalidArgument', { message });
      return;
    }
    res.apiRes('ok', { message: req.t('0_verificationSentMsg'), ...(utils.isProd() ? {} : { code }) });
  } catch (error) {
    next(error);
  }
};

const forgetPassword = async (req, res, next) => {
  try {
    const { phoneNumber } = req.body;
    const user = await users.findOne({ where: { phoneNumber, channel_id: req.body.code } });
    if (!user) return res.apiRes('notFound', { message: `${req.t('0_phoneNumber')}${req.t('0_doesNotExist')}` });
    if (user.status === 0) return res.apiRes('unauthenticated', { message: req.t('0_accountSuspended') });
    const newPassowrd = passwordGenerator.generatePassword(10);
    const { message } = await verificationRespository.sendForgetPassowrd(user.username, phoneNumber, newPassowrd, req.t);
    if (message) {
      res.apiRes('invalidArgument', { message });
      return;
    }
    const bcryptPwd = bcrypt.hashSync(newPassowrd, bcrypt.genSaltSync(10), null);

    await user.update({ password: bcryptPwd });
    res.apiRes('ok', { message: req.t('0_verificationSentMsg'), ...(utils.isProd() ? {} : { newPassowrd }) });
  } catch (error) {
    next(error);
  }
};

const verification = async (req, res, next) => {
  try {
    const message = await verificationRespository.sendVerification(req.body.phoneNumber, req.body.code, req.t);
    if (message) {
      res.apiRes('invalidArgument', { message });
      return;
    }
    res.apiRes('ok', { message: req.t('0_verificationSentMsg') });
  } catch (error) {
    res.apiRes('unknown', error);
  }
};

const signOut = (req, res, next) => {
  req.logout(function (err) {
    if (err) { return next(err); }
    res.apiRes('ok');
  });
};

const session = (req, res, next) => {
  const body = req.user.dataValues;
  const { symbol, language } = body.channel.dataValues;
  const { username, phoneNum, email } = body;
  return res.apiRes('ok', { username, phoneNumber: phoneNum, email, symbol, language });
};

const otpLogin = async (req, res, next) => {
  passport.authenticate('totp', function (err, user, info) {
    if (err) { return next(err); }
    if (!user) {
      return res.apiRes('unauthenticated', info);
    }
    req.session.isOtpAuthenticated = true;
    next();
  })(req, res, next);
};

const redirectIndex = async (req, res, next) => {
  return res.redirect('/index');
};

const register = async (req, res, next) => {
  try {
    const { username, phoneNumber, verifyCode } = req.body;
    const channel = await channels.findOne({ where: { id: req.body.code } });
    if (!channel) return req.apiRes('invalidArgument', { message: req.t('5_errorMsg_1') });
    const checkUser = await users.findOne({ where: { username, channel_id: req.body.code } });
    if (checkUser) return res.apiRes('alreadyExists', { message: req.t('0_accountAlready') });
    const checkPhone = await users.findOne({ where: { phoneNum: phoneNumber, channel_id: req.body.code } });
    if (checkPhone) return res.apiRes('alreadyExists', { message: req.t('0_phoneNumberIsAlreadyExists') });
    const verifyRes = await verificationRespository.verifyPhoneSMSCode({ verifyCode, phoneNumber, channelId: req.body.code });
    if (!verifyRes) return res.apiRes('unauthenticated', { message: req.t('0_verifyFailed') });
    const resUser = await users.create({
      name: req.body.username,
      username: req.body.username,
      phoneNum: req.body.phoneNumber,
      password: bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(10), null),
      channelId: req.body.code,
      verifiedAt: new Date()
    });
    await wallets.create({
      userId: resUser.id,
      symbol: channel.symbol,
      position: 'TOKEN'
    });
    logger.info(`帐号注册成功, username: ${resUser.dataValues.username}`);
    res.locals.message = req.t('0_registerSuccessMsg');
    next();
  } catch (error) {
    next(error);
  }
};

const launchGame = async (req, res, next) => {
  try {
    const user = req.user.toJSON();
    const ip = await utils.getIp(req).replace(/::ffff:/, '');
    const redirect = await gameProviderAdapter.getLoginUrl('vp', {
      channelId: user.channel_id,
      username: user.username,
      ip
    });
    res.apiRes('ok', { redirect, ...(res.locals.message ? { message: res.locals.message } : {}) });
  } catch (error) {
    next(error);
  }
};

const updatePassword = async (req, res) => {
  const user = req.user;
  const hashPass = /^\$2y\$/.test(user.password) ? '$2b$' + user.password.slice(4) : user.password;
  if (!bcrypt.compareSync(req.body.password, hashPass)) return res.apiRes('badRequset', { message: req.t('1_accountPasswordErrorMsg') });
  await user.update({ password: bcrypt.hashSync(req.body.newPassword, bcrypt.genSaltSync(10), null) });
  res.apiRes('ok');
};

const getWalletLedgers = async (req, res, next) => {
  try {
    const user = req.user.toJSON();
    const startDate = req.query.startDate && req.query.startDate !== '' ? req.query.startDate : moment().subtract(24, 'h').format();
    const endDate = req.query.endDate && req.query.endDate !== '' ? req.query.endDate : moment().format('YYYY-MM-DD HH:mm:ss');
    const pageSize = req.query.pageSize ? parseInt(req.query.pageSize) : 20; // 每页记录数
    const currentPage = req.query.currentPage ? parseInt(req.query.currentPage) : 1; // 当前页码
    const offSet = (currentPage - 1) * pageSize;
    const options = {
      createdAt: { [Op.between]: [startDate, endDate] },
      ...(req.query.typeId ? { typeId: req.query.typeId } : {})
    };
    if (moment(startDate).isAfter(endDate)) {
      res.apiRes('invalidArgument', { message: 'startDate is after endDate' });
      return;
    }

    const _rows = await walletLedgers.findAll({
      where: options,
      include: {
        attribues: ['id'],
        association: 'wallet',
        where: {
          userId: user.id,
          symbol: user.channel.symbol,
          position: 'TOKEN'
        }
      },
      offset: offSet,
      limit: pageSize
    });

    const rows = _rows.map(row => {
      return {
        id: row.id,
        createdAt: row.createdAt,
        gain: new BigNumber(row.gain).dividedBy(1000000).toNumber(),
        loss: new BigNumber(row.loss).dividedBy(1000000).toNumber(),
        balanceBefore: new BigNumber(row.balanceBefore).dividedBy(1000000).toNumber(),
        balanceRemain: new BigNumber(row.balanceRemain).dividedBy(1000000).toNumber(),
        currency: row.wallet.symbol,
        type: req.t(row.type)
      };
    });
    res.apiRes('ok', { rows });
  } catch (error) {
    next(error);
  }
};

module.exports = {
  signIn,
  jwtSignIn,
  phoneSignIn,
  sendVerification,
  verification,
  forgetPassword,
  signOut,
  session,
  otpLogin,
  redirectIndex,
  register,
  launchGame,
  updatePassword,
  getWalletLedgers
};
