const health = async (req, res, next) => {
  const data = {
    uptime: process.uptime(),
    date: new Date()
  };

  res.apiRes('ok', data);
};

const redirectIndex = async (req, res, next) => {
  let baseUrl = '';
  if (req.originalUrl?.includes('agent')) {
    baseUrl = '/agent';
  } else if (req.originalUrl?.includes('admin')) {
    baseUrl = '/admin';
  }
  return res.redirect(`${baseUrl}/index`);
};

const lang = function (req, res) {
  const data = req.getCatalog(req.query.lang ?? '');
  res.apiRes('ok', data);
};

const langList = function (req, res) {
  res.apiRes('ok', req.getLocales().filter(el => el !== 'desensitization'));
};

const git = require('git-rev-sync');
const version = (req, res) => {
  let version;
  try {
    version = process.env.VERSION || git.tag();
  } catch (error) {
    logger.error(error);
  }
  let sha;
  try {
    sha = process.env.COMMIT_SHA || git.short();
  } catch (error) {
    logger.error(error);
  }
  res.apiRes('ok', { version, sha });
};

module.exports = { health, redirectIndex, lang, langList, version };
