/**
 * Module dependencies.
 */
require('dotenv').config();

const logger = require('./libs/logger');
global.logger = logger;

const path = require('path');
const directory = path.join(path.resolve(), 'server');
const fs = require('fs');
const serverTypes = [];
const files = fs.readdirSync(directory);

const serverFiles = files.filter(file => path.extname(file) === '.js');

if (serverFiles.length === 0) {
  logger.error(`disable to find server.js files, please check server dir: ${directory}`);
  process.exit();
}

// Check if serverType parameters are provided
const serverTypesInput = process.argv.slice(2);

if (serverTypesInput.length === 0) {
  serverTypes.push(...serverFiles.map(file => path.parse(file).name));
} else {
  serverTypes.push(...serverTypesInput);
}

// Loop through each provided serverType
serverTypes.forEach((serverType, index) => {
  try {
    // Dynamically require the specified serverType.js file
    const app = require(`./server/${serverType.toLowerCase()}`);
    const http = require('http');

    /**
     * Get port from environment and store in Express.
     */
    const port = normalizePort(process.env[`${serverType.toUpperCase()}_PORT`] || `952${7 + index}`);
    app.set('port', port);

    /**
     * disable etag
     */
    app.set('etag', false);

    /**
     * Create HTTP server.
     */
    const server = http.createServer(app);

    /**
    * Event listener for HTTP server "error" event.
    */
    const onError = (error) => {
      if (error.syscall !== 'listen') {
        throw error;
      }

      const bind = typeof port === 'string'
        ? 'Pipe ' + port
        : 'Port ' + port;

      // handle specific listen errors with friendly messages
      switch (error.code) {
        case 'EACCES':
          logger.error(bind + ' requires elevated privileges');
          process.exit(1);
          break;
        case 'EADDRINUSE':
          logger.error(bind + ' is already in use');
          process.exit(1);
          break;
        default:
          throw error;
      }
    };

    /**
     * Event listener for HTTP server "listening" event.
     */
    const onListening = () => {
      const addr = server.address();
      const bind = typeof addr === 'string'
        ? 'pipe ' + addr
        : 'port ' + addr.port;
      logger.info(serverType + ' listening on ' + bind);
    };

    /**
     * Listen on provided port, on all network interfaces.
     */
    server.listen(port);
    server.on('error', onError);
    server.on('listening', onListening);
  } catch (error) {
    logger.error(`Error loading serverType: ${serverType}`);
    logger.error(error);
    process.exit(1);
  }
});

/**
 * Normalize a port into a number, string, or false.
 */
function normalizePort (val) {
  const port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}
