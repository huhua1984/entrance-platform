const express = require('express');
const { vpErrorHandler } = require('../../libs/gameProviders/errorHandler');
const router = express.Router();
const vpService = require('../../services/games/vp');

router.post('/channel/:id/records', vpService.upsertVpLogsByChannel, vpErrorHandler);
router.post('/records', vpService.upsertVpLogs, vpErrorHandler);
router.get('/record/job/start', vpService.startJob);
router.get('/record/job/stop', vpService.stopJob);

module.exports = router;
