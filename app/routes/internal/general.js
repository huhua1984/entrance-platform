const express = require('express');
const router = express.Router();
const { health, version } = require('../../services/general');

/* General */
router.get('/health', health);
router.get('/version', version);

module.exports = router;
