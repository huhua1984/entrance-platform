const express = require('express');
const router = express.Router();
const wallet = require('../../services/wallet');
const walletValidator = require('../../libs/middlewares/validator/wallet');
const validatorHandler = require('../../libs/middlewares/validatorHandler');

/* wallets */
router.post('/', walletValidator.insert, validatorHandler, wallet.insert);

module.exports = router;
