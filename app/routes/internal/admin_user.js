const express = require('express');
const router = express.Router();
const adminUserService = require('../../services/admin_user');
const adminUserValidator = require('../../libs/middlewares/validator/admin_user');
const validatorHandler = require('../../libs/middlewares/validatorHandler');

/* 更改密碼 by channel_id */
router.post('/password/updateChannel/:channel_id', adminUserValidator.updateChannelPassword, validatorHandler, adminUserService.updateChannelPassword);

module.exports = router;
