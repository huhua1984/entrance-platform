const express = require('express');
const router = express.Router();
const channel = require('../../services/channel');
const channelValidator = require('../../libs/middlewares/validator/channel');
const validatorHandler = require('../../libs/middlewares/validatorHandler');

/**
 * @openapi
 * /api/channel/{channelId}/coop:
 *   get:
 *     summary: 獲取代理合作方式
 *     tags:
 *       - channel
 *     parameters:
 *       - in: path
 *         name: channelId
 *         schema:
 *           type: string
 *         require: true
 *         description: 代理UUID
 *         example: 718007a2d2da44e0a10933d7c807fc4d
 *     responses:
 *       '200':
 *         description: 操作成功
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ApiResponse'
 *             example:
 *               codeNO: 0
 *               code: 'OK'
 *               message: '操作成功'
 *               body:
 *                 cooperation_func: 2
 *                 promotionUrlType: 0
 *                 lang: 'ja-jp'
 *               description: cooperation_func 1:网咖OTP 2:网咖免验证, promotionUrlType 0:Auth 1:Landing
 *       '404':
 *         description: 查無代理帳號 表示推廣連結錯誤
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ApiResponse'
 *             example:
 *               codeNO: 5
 *               code: 'NOT_FOUND'
 *               message: '推广连结不正确'
 *       '503':
 *         description: 代理未啟用
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ApiResponse'
 *             example:
 *               codeNO: 14
 *               code: 'UNAVAILABLE'
 *               message: '代理未启用'
 */
router.get('/:id/coop', channelValidator.checkId, validatorHandler, channel.getCooperation);

/**
 * @openapi
 * /api/channel/{channelId}/games:
 *   get:
 *     summary: 獲取代理遊戲列表
 *     tags:
 *       - channel
 *     parameters:
 *       - in: path
 *         name: channelId
 *         schema:
 *           type: string
 *         require: true
 *         description: 代理UUID
 *         example: 718007a2d2da44e0a10933d7c807fc4d
 *     responses:
 *       '200':
 *         description: 操作成功
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ApiResponse'
 *             example:
 *               codeNO: 0
 *               code: 'OK'
 *               message: '操作成功'
 *               body:
 *                 games: [
 *                   {"gameId":"14b47223eef349aba003683875f1ea58","gameName":"NC_Buffalo Hunter","gameCode":"16057","sortId":0,"isHot":0,"hotOrder":0,"gameImg":"","gameType":0,"gateway": {"name":"Nolimit City","slug":"nc"}},
 *                   {"gameId":"00f504bc10aa4e9eaf9bfc4fd1eb21e7","gameName":"抢庄牌九","gameCode":"730","sortId":0,"isHot":0,"hotOrder":0,"gameImg":"","gameType":3,"gateway": {"name":"Viva Poker","slug":"vp"}}]
 *               description:
 *                 gameType: 类型 0:无 1:Slots 2:Live 3:Table 4:Fish
 *                 sortId: 排序
 *                 flag: 标签分类(大厅、直播) 0:无 1:Lobby
 *                 isHot: 热门 0:无 1:热 2:新
 *                 gateway:
 *                   name: 通道名稱(廠商)
 *                   slug: 通道代號(廠商)
 *                 group: 群组分类(最佳、特色) 0:无 1:牌技遊戲 2:賭桌遊戲 3:押分機遊戲 4:購買免費遊戲
 */
router.get('/:id/games', channelValidator.checkId, validatorHandler, channel.getGames);

module.exports = router;
