const express = require('express');
const router = express.Router();
const playerService = require('../../services/player/player');
const playerVpService = require('../../services/player/vp');
const validatorHandler = require('../../libs/middlewares/validatorHandler');
const { dayLimiter, minuteLimiter } = require('../../libs/middlewares/limters');
const playerValidator = require('../../libs/middlewares/validator/player');
const { isProd } = require('../../libs/utils');
const { vpErrorHandler } = require('../../libs/gameProviders/errorHandler');
const authenticated = require('../../libs/middlewares/authenticate');

/**
 * @openapi
 * /api/player/signin:
 *   post:
 *     summary: 帳密登入
 *     tags:
 *       - player
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               code:
 *                 type: string
 *                 description: 推廣碼 - 代理UUID
 *                 example: 718007a2d2da44e0a10933d7c807fc4d
 *               username:
 *                 type: string
 *                 description: 帳號
 *                 example: demo_user1
 *               password:
 *                 type: string
 *                 description: 密碼
 *                 example: zxc12345
 *     responses:
 *       '200':
 *         description: 操作成功
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ApiResponse'
 *             example:
 *               codeNO: 0
 *               code: 'OK'
 *               message: '操作成功'
 *               body:
 *                 redirect: 'https://example.lobby'
 *       '401':
 *         description: 用户名或密码有误!
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ApiResponse'
 *             example:
 *               codeNO: 16
 *               code: 'UNAUTHENTICATED'
 *               message: 用户名或密码有误!
 */
router.post('/signin', playerValidator.signIn, validatorHandler, playerService.signIn, authenticated, playerService.launchGame, vpErrorHandler);

/**
 * @openapi
 * /api/player/jwtSignin:
 *   post:
 *     summary: 帳密登入
 *     tags:
 *       - player
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               code:
 *                 type: string
 *                 description: 推廣碼 - 代理UUID
 *                 example: 718007a2d2da44e0a10933d7c807fc4d
 *               username:
 *                 type: string
 *                 description: 帳號
 *                 example: demo_user1
 *               password:
 *                 type: string
 *                 description: 密碼
 *                 example: zxc12345
 *     responses:
 *       '200':
 *         description: 操作成功 (將token待入Authorization HEADER)
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ApiResponse'
 *             example:
 *               codeNO: 0
 *               code: 'OK'
 *               message: '操作成功'
 *               body:
 *                 token: ''
 *       '401':
 *         description: 用户名或密码有误!
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ApiResponse'
 *             example:
 *               codeNO: 16
 *               code: 'UNAUTHENTICATED'
 *               message: 用户名或密码有误!
 */
router.post('/jwtSignin', playerValidator.signIn, validatorHandler, playerService.jwtSignIn);

/**
 * @openapi
 * /api/player/phoneSignin:
 *   post:
 *     summary: 手機登入
 *     tags:
 *       - player
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               code:
 *                 type: string
 *                 description: 推廣碼 - 代理UUID
 *                 example: 718007a2d2da44e0a10933d7c807fc4d
 *               phoneNumber:
 *                 type: string
 *                 description: 手機號碼
 *                 example: 8016804019
 *               verifyCode:
 *                 type: string
 *                 description: 驗證碼
 *                 example: 123456
 *     responses:
 *       '200':
 *         description: 操作成功
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ApiResponse'
 *             example:
 *               codeNO: 0
 *               code: 'OK'
 *               message: '操作成功'
 *               body:
 *                 redirect: 'http://example.lobby'
 *       '401':
 *         description: 驗證失敗
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ApiResponse'
 *             example:
 *               codeNO: 16
 *               code: 'UNAUTHENTICATED'
 *               message: 验证失败，请重新输入!
 */
router.post('/phoneSignin', playerValidator.phoneSignIn, validatorHandler, playerService.phoneSignIn, playerService.launchGame, vpErrorHandler);

/**
 * @openapi
 * /api/player/register:
 *   post:
 *     summary: 註冊
 *     tags:
 *       - player
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               code:
 *                 type: string
 *                 description: 推廣碼 - 代理UUID
 *                 example: 718007a2d2da44e0a10933d7c807fc4d
 *               username:
 *                 type: string
 *                 description: 帳號
 *                 example: demo_user1
 *               password:
 *                 type: string
 *                 description: 密碼
 *                 example: zxc12345
 *               confirmPassword:
 *                 type: string
 *                 description: 確認密碼
 *                 example: zxc12345
 *               phoneNumber:
 *                 type: string
 *                 description: 手機號碼
 *                 example: 8016804019
 *               verifyCode:
 *                 type: string
 *                 description: 驗證碼
 *                 example: 123456
 *     responses:
 *       '200':
 *         description: 註冊成功 前端秀出文字並導到遊戲大廳
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ApiResponse'
 *             example:
 *               codeNO: 0
 *               code: 'OK'
 *               message: '帐户创建成功，请使用新帐号登录游戏大厅'
 *               body:
 *                 message: '帐户创建成功，请使用新帐号登录游戏大厅'
 *                 redirect: 'http://example.lobby'
 *         headers:
 *           Set-Cookie:
 *             schema:
 *               type: string
 *               example: player=s%3AbzVCEJ--SDHcojkU8RK8TGbMheWRxNep.goAZ%2BYZN0D3A6cu8h35qUIEMKPLZzQx%2B5Vpf1VRZff0; Path=/; Expires=Thu, 07 Dec 2023 09:44:54 GMT; HttpOnly
 *       '401':
 *         description: 驗證失敗
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ApiResponse'
 *             example:
 *               codeNO: 16
 *               code: 'UNAUTHENTICATED'
 *               message: 验证失败，请重新输入!
 */
router.post('/register', playerValidator.register, validatorHandler, playerService.register, playerService.signIn, playerService.launchGame, vpErrorHandler);

/**
 * @openapi
 * /api/player/signout:
 *   post:
 *     summary: 登出
 *     tags:
 *       - player
 *     parameters:
 *       - in: cookie
 *         name: player
 *         schema:
 *           type: string
 *         example: s%3AbzVCEJ--SDHcojkU8RK8TGbMheWRxNep.goAZ%2BYZN0D3A6cu8h35qUIEMKPLZzQx%2B5Vpf1VRZff0
 *         description: 用户身份验证的 Cookie
 *     responses:
 *       '200':
 *         description: 登出成功
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ApiResponse'
 *             example:
 *               codeNO: 0
 *               code: 'OK'
 *               message: '操作成功'
 */
router.post('/signout', playerService.signOut);

/**
 * @openapi
 * /api/player/session:
 *   get:
 *     summary: 獲取session資訊
 *     tags:
 *       - player
 *     parameters:
 *       - in: cookie
 *         name: player
 *         schema:
 *           type: string
 *         example: s%3AbzVCEJ--SDHcojkU8RK8TGbMheWRxNep.goAZ%2BYZN0D3A6cu8h35qUIEMKPLZzQx%2B5Vpf1VRZff0
 *         description: 用户身份验证的 Cookie
 *     responses:
 *       '200':
 *         description: 操作成功
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ApiResponse'
 *             example:
 *               codeNO: 0
 *               code: 'OK'
 *               message: '操作成功'
 *               body:
 *                 username: 'demo_user1'
 *                 phoneNumber: '12345678'
 *                 email: 'test@g.com'
 *                 symbol: 'JPY'
 *                 language: 'ja-jp'
 *       '401':
 *         description: 驗證失敗
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ApiResponse'
 *             example:
 *               codeNO: 16
 *               code: 'UNAUTHENTICATED'
 *               message: '验证未授权'
 */
router.get('/session', authenticated, playerService.session);

/**
 * @openapi
 * /api/player/walletLedgers:
 *   get:
 *     summary: 獲取帳變紀錄
 *     tags:
 *       - player
 *     parameters:
 *       - in: cookie
 *         name: player
 *         schema:
 *           type: string
 *         example: s%3AbzVCEJ--SDHcojkU8RK8TGbMheWRxNep.goAZ%2BYZN0D3A6cu8h35qUIEMKPLZzQx%2B5Vpf1VRZff0
 *         description: 用户身份验证的 Cookie
 *       - in: query
 *         name: starDate
 *         schema:
 *           type: datetime ISO 8601
 *         example: '2024-01-07T16:00:00+00:00'
 *         description: 開始日期 預設當前時間24小時前
 *       - in: query
 *         name: endDate
 *         schema:
 *           type: datetime ISO 8601
 *         example: '2024-01-07T16:00:00+00:0'
 *         description: 結束日期 預設伺服器當前時間
 *       - in: query
 *         name: page
 *         schema:
 *           type: date
 *         example: '2023-12-07'
 *         description: 當前頁碼 預設:1
  *       - in: query
 *         name: pageSize
 *         schema:
 *           type: date
 *         example: '2023-12-07'
 *         description: 每頁紀錄數量 預設:20
 *       - in: query
 *         name: typeId
 *         schema:
 *           type: numberstring
 *         example: '1'
 *         description: type 預設:1 系統上下分, 1002 游戏请求下注, 1003 游戏返还余额, 1005 游戏取消下注
 *     responses:
 *       '200':
 *         description: 操作成功
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ApiResponse'
 *             example:
 *               codeNO: 0
 *               code: 'OK'
 *               message: '操作成功'
 *               body:
 *                 rows: ['$walletLedgers' ]
 *               description:
 *                 $walletLedgers:
 *                   id: 注單id
 *                   createdAt: 時間
 *                   type: 帳變類型
 *                   currency: 幣別
 *                   gain: 收入
 *                   loss: 支出
 *                   balanceBefore: 交易前餘額
 *                   balanceAfter: 交易後餘額
 *       '401':
 *         description: 驗證失敗
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ApiResponse'
 *             example:
 *               codeNO: 16
 *               code: 'UNAUTHENTICATED'
 *               message: '验证未授权'
 */
router.get('/walletLedgers', authenticated, playerService.getWalletLedgers);

/**
 * @openapi
 * /api/player/sendVerification:
 *   post:
 *     summary: 發送驗證碼
 *     tags:
 *       - player
 *     description: NODE_ENV為production將會對同一支手機號碼限制一天10次及每分鐘1次
 *     requestBody:
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               code:
 *                 required: true
 *                 type: string
 *                 description: 推廣碼 - 代理UUID
 *                 example: 718007a2d2da44e0a10933d7c807fc4d
 *               phoneNumber:
 *                 type: string
 *                 description: 手機號碼
 *                 example: 8016804019
 *     responses:
 *       '200':
 *         description: 發送成功
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ApiResponse'
 *             example:
 *               codeNO: 0
 *               code: 'OK'
 *               message: '验证码已发送，请查阅简讯。'
 *               body:
 *                 message: '验证码已发送，请查阅简讯。'
 *       '400':
 *         description: 手機號碼格式錯誤
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ApiResponse'
 *             example:
 *               codeNO: 3
 *               code: 'INVALID_ARGUMENT'
 *               message: 手机号码格式不正确，请重新输入!
 */
router.post('/sendVerification', playerValidator.sendVerification, validatorHandler,
  ...(isProd() ? [dayLimiter, minuteLimiter] : []), playerService.sendVerification);

/**
 * @openapi
 * /api/player/forgetPassword:
 *   post:
 *     summary: 忘記密碼
 *     tags:
 *       - player
 *     description: NODE_ENV為production將會對同一支手機號碼限制一天10次及每分鐘1次
 *     requestBody:
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               code:
 *                 required: true
 *                 type: string
 *                 description: 推廣碼 - 代理UUID
 *                 example: 718007a2d2da44e0a10933d7c807fc4d
 *               phoneNumber:
 *                 type: string
 *                 description: 手機號碼
 *                 example: 8016804019
 *     responses:
 *       '200':
 *         description: 發送成功
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ApiResponse'
 *             example:
 *               codeNO: 0
 *               code: 'OK'
 *               message: '帐号及密码讯息已发送，请查阅简讯并请勿将帐号及密码资讯告知其他人'
 *               body:
 *                 message: '帐号及密码讯息已发送，请查阅简讯并请勿将帐号及密码资讯告知其他人'
 *       '400':
 *         description: 手機號碼格式錯誤
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ApiResponse'
 *             example:
 *               codeNO: 3
 *               code: 'INVALID_ARGUMENT'
 *               message: 手机号码格式不正确，请重新输入!
 */
router.post('/forgetPassword', playerValidator.sendVerification, validatorHandler,
  ...(isProd() ? [dayLimiter, minuteLimiter] : []), playerService.forgetPassword);

/**
 * @openapi
 * /api/player/updatePassword:
 *   post:
 *     summary: 修改密碼
 *     tags:
 *       - player
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               password:
 *                 type: string
 *                 description: 舊密碼(AES加密)
 *                 example: U2FsdGVkX1/JBB15fdMqY64qnLPLhH3fk+vVHWilbfY=
 *               newPassword:
 *                 type: string
 *                 description: 新密碼(AES加密)
 *                 example: U2FsdGVkX1/JBB15fdMqY64qnLPLhH3fk+vVHWilbfY=
 *               confirmNewPassword:
 *                 type: string
 *                 description: 確認新密碼(AES加密)
 *                 example: U2FsdGVkX1/JBB15fdMqY64qnLPLhH3fk+vVHWilbfY=
 *     responses:
 *       '200':
 *         description: 操作成功
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ApiResponse'
 *             example:
 *               codeNO: 0
 *               code: 'OK'
 *               message: '操作成功'
 *       '401':
 *         description: 用户名或密码有误!
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ApiResponse'
 *             example:
 *               codeNO: 16
 *               code: 'UNAUTHENTICATED'
 *               message: 用户名或密码有误!
 */
router.post('/updatePassword', authenticated, playerValidator.updatePassword, validatorHandler, playerService.updatePassword);

/**
 * @openapi
 * /api/player/vp/logs:
 *   get:
 *     summary: 獲取vp遊戲紀錄
 *     tags:
 *       - player
 *     parameters:
 *       - in: cookie
 *         name: player
 *         schema:
 *           type: string
 *         example: s%3AbzVCEJ--SDHcojkU8RK8TGbMheWRxNep.goAZ%2BYZN0D3A6cu8h35qUIEMKPLZzQx%2B5Vpf1VRZff0
 *         description: 用户身份验证的 Cookie
 *       - in: query
 *         name: starDate
 *         schema:
 *           type: date
 *         example: '2023-12-01'
 *         description: 開始日期 不帶入預設 當前時間一天前
 *       - in: query
 *         name: endDate
 *         schema:
 *           type: date
 *         example: '2023-12-07'
 *         description: 結束日期 不帶入預設 為當前時間
 *       - in: query
 *         name: page
 *         schema:
 *           type: date
 *         example: '2023-12-07'
 *         description: 當前頁碼 預設:1
 *       - in: query
 *         name: pageSize
 *         schema:
 *           type: date
 *         example: '2023-12-07'
 *         description: 每頁紀錄數量 預設:20
 *     responses:
 *       '200':
 *         description: 操作成功
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ApiResponse'
 *             example:
 *               codeNO: 0
 *               code: 'OK'
 *               message: '操作成功'
 *               body:
 *                 rows: ['$gameLogSchema' ]
 *               description:
 *                 $gameLogSchema:
 *                   gameEndTime: 遊戲結束時間
 *                   gameUserNo: 局號
 *                   gameName: 遊戲名稱
 *                   roomName: 房間名稱
 *                   currency: 幣別
 *                   allBet: 總投注
 *                   profit: 輸贏金額
 *       '401':
 *         description: 驗證失敗
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ApiResponse'
 *             example:
 *               codeNO: 16
 *               code: 'UNAUTHENTICATED'
 *               message: '验证未授权'
 */
router.get('/vp/logs', authenticated, playerVpService.getLogs, vpErrorHandler);

/**
 * @openapi
 * /api/player/vp/balance:
 *   get:
 *     summary: 獲取vp遊戲餘額
 *     tags:
 *       - player
 *     parameters:
 *       - in: cookie
 *         name: player
 *         schema:
 *           type: string
 *         example: s%3AbzVCEJ--SDHcojkU8RK8TGbMheWRxNep.goAZ%2BYZN0D3A6cu8h35qUIEMKPLZzQx%2B5Vpf1VRZff0
 *         description: 用户身份验证的 Cookie
 *     responses:
 *       '200':
 *         description: 操作成功
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ApiResponse'
 *             example:
 *               codeNO: 0
 *               code: 'OK'
 *               message: '操作成功'
 *               body:
 *                 balance: 567
 *       '401':
 *         description: 驗證失敗
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ApiResponse'
 *             example:
 *               codeNO: 16
 *               code: 'UNAUTHENTICATED'
 *               message: '验证未授权'
 */
router.get('/vp/balance', authenticated, playerVpService.getBalance, vpErrorHandler);

/**
 * @openapi
 * /api/player/vp/launchGame:
 *   post:
 *     summary: vp遊戲登入
 *     tags:
 *       - player
 *     parameters:
 *       - in: cookie
 *         name: player
 *         schema:
 *           type: string
 *         example: s%3AbzVCEJ--SDHcojkU8RK8TGbMheWRxNep.goAZ%2BYZN0D3A6cu8h35qUIEMKPLZzQx%2B5Vpf1VRZff0
 *         description: 用户身份验证的 Cookie
 *       - in: query
 *         name: gameCode
 *         schema:
 *           type: string
 *         example: 900
 *         description: 遊戲的game code
 *     responses:
 *       '200':
 *         description: 操作成功
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ApiResponse'
 *             example:
 *               codeNO: 0
 *               code: 'OK'
 *               message: '操作成功'
 *               body:
 *                 redirect: 'https://example.lobby'
 *       '401':
 *         description: 驗證失敗
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ApiResponse'
 *             example:
 *               codeNO: 16
 *               code: 'UNAUTHENTICATED'
 *               message: '验证未授权'
 */
router.post('/vp/launchGame', authenticated, playerVpService.launchGame, vpErrorHandler);

module.exports = router;
