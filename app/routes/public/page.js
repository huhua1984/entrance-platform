const express = require('express');
const router = express.Router();
const playerPageService = require('../../services/player/page');
const otpAuth = require('../../libs/middlewares/otpAuth');
const authenticated = require('../../libs/middlewares/authenticate');

router.get('/signin', playerPageService.signInPage);
router.get('/index', authenticated, otpAuth, playerPageService.indexPage);
router.get('/setup', authenticated, playerPageService.setupPage);
router.get('/signup', playerPageService.signUpPage);

module.exports = router;
