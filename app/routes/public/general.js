const express = require('express');
const router = express.Router();
const { health, lang, langList, version } = require('../../services/general');

/**
 * @openapi
 * /api/health:
 *   get:
 *     summary: 服務健康狀況
 *     tags:
 *       - general
 *     responses:
 *       '200':
 *         description: 操作成功
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ApiResponse'
 *             example:
 *               codeNO: 0
 *               code: 'OK'
 *               message: '操作成功'
 *               body:
 *                 uptime: 459.3774961
 *                 date: '2023-12-07T02:48:54.435Z'
 */
router.get('/health', health);

/**
 * @openapi
 * /api/lang:
 *   get:
 *     summary: 獲取語言包
 *     tags:
 *       - general
 *     parameters:
 *       - in: query
 *         name: lang
 *         type: string
 *         description: 篩選語系
 *     responses:
 *       '200':
 *         description: 操作成功
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ApiResponse'
 *             example:
 *               codeNO: 0
 *               code: 'OK'
 *               message: '操作成功'
 *               body:
 *                 zh-cn:
 *                   0_example: 例子
 *                 ja-jp:
 *                   0_example: たとえ
 *                 en-us:
 *                   0_example: example
 */
router.get('/lang', lang);

// 语系列表
router.get('/lang/list', langList);

/**
 * @openapi
 * /api/version:
 *   get:
 *     summary: 服務版本
 *     tags:
 *       - general
 *     responses:
 *       '200':
 *         description: 操作成功
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ApiResponse'
 *             example:
 *               codeNO: 0
 *               code: 'OK'
 *               message: '操作成功'
 *               body:
 *                 version: 'v0.0.1'
 *                 sha: '9a6541a'
 */
router.get('/version', version);

module.exports = router;
